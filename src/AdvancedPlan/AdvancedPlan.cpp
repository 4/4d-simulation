// M.I.K.E. 3D Soccer Simulation team
// File: AdvancedPlan.cpp

#include <AdvancedPlan.h>
#include <HeadCommand.h>
#include <SayCommand.h>
#include <ConfigSkill.h>
#include <SkillManager.h>

using namespace std;
using namespace MIKE;

AdvancedPlan::AdvancedPlan(){
	mHeadCommand = new HeadCommand();
	mSayCommand = new SayCommand();
}

AdvancedPlan::~AdvancedPlan(){
	delete mHeadCommand;
	delete mSayCommand;
}

HeadCommand* AdvancedPlan::getHeadCommand() const {
	return mHeadCommand;
}

SayCommand* AdvancedPlan::getSayCommand() const {
	return mSayCommand;
}

void AdvancedPlan::decideHead() {
	// TODO: Put head decision code here
}

void AdvancedPlan::decideSay() {
	// TODO: Put say decision code here
}

void AdvancedPlan::decide() {
	SkillManager::Instance()->manage<ConfigSkill>("Style");
}

