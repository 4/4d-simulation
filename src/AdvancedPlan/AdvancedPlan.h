// M.I.K.E. 3D Soccer Simulation team
// File: AdvancedPlan.h

#ifndef _MIKE_ADVANCED_PLAN_H
#define _MIKE_ADVANCED_PLAN_H

namespace MIKE{

	class HeadCommand;
	class SayCommand;

	class AdvancedPlan {
		
		HeadCommand *mHeadCommand;
		SayCommand *mSayCommand;

	public:
		
		AdvancedPlan();				// Constractor
		virtual ~AdvancedPlan();			// Distractor

		HeadCommand* getHeadCommand() const;
		SayCommand* getSayCommand() const;

		virtual void decide();		// Decides what to add to skill manager
		
		virtual void decideHead();
		virtual void decideSay();

	}; // class AdvancedPlan

} // namespace MIKE

#endif // _MIKE_ADVANCED_PLAN_H
