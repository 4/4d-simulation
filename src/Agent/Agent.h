// M.I.K.E. 3D Soccer Simulation team
// File: Agent.h

#ifndef _MIKE_AGENT_H
#define _MIKE_AGENT_H

namespace MIKE {

	class Connection;
	class AdvancedPlan;

	// Agent model class. This class is the top layer class.
	class Agent {
		
		AdvancedPlan *plan;	// What Plan to decide with

		Connection *connection; // Connection to the server

	public:

		Agent(int argc, char *argv[]); // Constructor
		~Agent(); // Destructor

		void run(); // Program's main loop

	private:

		void init(); // Initialize agent

	}; // class Agent

} // namespace MIKE

#endif // _MIKE_AGENT_H

