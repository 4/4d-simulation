// M.I.K.E. 3D Soccer Simulation team
// File: Agent.cpp

#include <Agent.h>
#include <AdvancedPlan.h>
#include <ConfigManager.h>
#include <Connection.h>
#include <SkillManager.h>
#include <WorldModel.h>
#include <BodyCommand.h>
#include <HeadCommand.h>
#include <SayCommand.h>

using namespace std;
using namespace MIKE;

Agent::Agent(int argc, char *argv[]) {
	cout << "M.I.K.E. 3D Soccer Simulation Team" << endl;
	cout << "Iran University of Science and Technology" << endl;

	ConfigManager::Instance()->initialize(argc, argv); // Initialize config files and config values

	connection = new Connection(ConfigManager::Instance()->get<string>("Server.Connection.Host"),
			ConfigManager::Instance()->get<string>("Server.Connection.Port"));

	plan = new AdvancedPlan(); // new plan by one of AdvancedPlan inherited classes
}

Agent::~Agent() {
	delete plan;

	delete connection;
}

void Agent::run() {
	init();

	while(true) {
		WorldModel::Instance()->update(connection->receive());

		plan->decide();
		plan->decideHead();
		plan->decideSay();
		
		SkillManager::Instance()->update();

		connection->send(plan->getHeadCommand()->toString() + " " + SkillManager::Instance()->getBodyCommand()->toString()
		  + " " + plan->getSayCommand()->toString());
	}
}

void Agent::init() {
	connection->connect();

	connection->send("(scene rsg/agent/nao/nao.rsg)");

	WorldModel::Instance()->update(connection->receive());
	WorldModel::Instance()->update(connection->receive());

	string number = ConfigManager::Instance()->get<string>("Agents.Global.Number");
	string unum = ConfigManager::Instance()->get<string>("Agents.Agent" + number + "." + "UniNum");
	string teamname = ConfigManager::Instance()->get<string>("Agents.Global.Teamname");
	string beam = ConfigManager::Instance()->get<string>("Agents.Agent" + number + "." + "Beam");

	connection->send("(init (unum " + unum + ")(teamname " + teamname + "))");

	WorldModel::Instance()->update(connection->receive());

	connection->send("(beam " + beam + ")");
}
   
