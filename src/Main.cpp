// M.I.K.E. 3D Soccer Simulation team
// File: Main.cpp

#include <iostream>
#include <exception>
#include <Agent.h>

using namespace std;
using namespace MIKE;

int main(int argc, char *argv[]) {

	try {

		Agent agent(argc, argv);
		agent.run(); // Main loop

		return 0;
	} catch(const exception &e) {
		cout << "\033[1;31m" << e.what() << "\033[0m" << endl;
		return 0;
	}

}

