// M.I.K.E. 3D Soccer Simulation team
// File: BodyCommand.h

#ifndef _MIKE_BODY_COMMAND_H
#define _MIKE_BODY_COMMAND_H

#include <Types.h>
#include <string>

namespace MIKE {

	// BodyCommand class holds joint velocity values for each cycle except head joints
	class BodyCommand {

		float mVelocity[JOINT_COUNT];

	public:

		BodyCommand(); // Default constructor
		BodyCommand(const BodyCommand &command); // Copy constructor
		void operator= (const BodyCommand &command);
		~BodyCommand(); // Destructor

		float operator[] (const JointID id) const;
		float& operator[] (const JointID id);

		std::string toString() const;

	}; // class Command

} // namespace MIKE

#endif // _MIKE_BODY_COMMAND_H

