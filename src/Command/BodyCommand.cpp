// M.I.K.E. 3D Soccer Simulation team
// File: BodyCommand.cpp

#include <BodyCommand.h>
#include <stdexcept>
#include <sstream>

using namespace MIKE;
using namespace std;

BodyCommand::BodyCommand() {
	for (int i = 0; i < JOINT_COUNT; i ++)
		mVelocity[i] = 0.0f;
}

BodyCommand::BodyCommand(const BodyCommand &command) {
	for (int i = 3; i < JOINT_COUNT; i ++) // Skipping head joints
		mVelocity[i] = command.mVelocity[i];
}

void BodyCommand::operator= (const BodyCommand &command) {
	for (int i = 3; i < JOINT_COUNT; i ++) // Skipping head joints
		mVelocity[i] = command.mVelocity[i];
}

BodyCommand::~BodyCommand() {

}

float BodyCommand::operator[] (const JointID id) const {
	if (id < 3 || id >= JOINT_COUNT)
		throw runtime_error("In 'const float BodyCommand::operator[] (const JointID id) const': Undefined joint id");
	return mVelocity[id];
}

float& BodyCommand::operator[] (const JointID id) {
	if (id < 1 || id >= JOINT_COUNT)
		throw runtime_error("In 'float& BodyCommand::operator[] (const JointID id)': Undefined joint id");
	return mVelocity[id];
}

string BodyCommand::toString() const {
	stringstream ss;
	for (int i = 3; i < JOINT_COUNT; i ++)
		ss << (i == 3 ? "" : " ") << "(" << JointString[i] << " " << mVelocity[i] << ")";
	return ss.str();
}

