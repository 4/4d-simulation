// M.I.K.E. 3D Soccer Simulation team
// File: HeadCommand.h

#ifndef _MIKE_HEADCOMMAND_H
#define _MIKE_HEADCOMMAND_H

#include <Types.h>

namespace MIKE {

	// Head command contains neck-yaw and neck-pitch commands
	class HeadCommand {

		float mVelocity[3];

	public:

		HeadCommand(); // Default constructor

		HeadCommand(const HeadCommand &command); // Copy constructor
		void operator= (const HeadCommand &command);

		~HeadCommand(); // Destructor

		float operator[] (const JointID id) const;
		float& operator[] (const JointID id);

		std::string toString() const;

	}; // class HeadCommand

} // namespace MIKE

#endif // _MIKE_HEADCOMMAND_H

