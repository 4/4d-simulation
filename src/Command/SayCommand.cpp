// M.I.K.E. 3D Soccer Simulation team
// File: SayCommand.cpp

#include <SayCommand.h>

using namespace MIKE;
using namespace std;

SayCommand::SayCommand() {

}

SayCommand::SayCommand(const SayCommand &command) {
	mMessage = command.mMessage;
}

void SayCommand::operator= (const SayCommand &command) {
	mMessage = command.mMessage;
}

SayCommand::~SayCommand() {

}

string SayCommand::getMessage() const {
	return mMessage;
}

void SayCommand::setMessage(const string &message) {
	mMessage = message;
}

string SayCommand::toString() const {
	if(mMessage.length() == 0)
		return string("");

	return "(say " + mMessage + ")";
}

