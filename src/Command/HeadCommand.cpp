// M.I.K.E. 3D Soccer Simulation team
// File: HeadCommand.cpp

#include <HeadCommand.h>
#include <stdexcept>
#include <sstream>

using namespace MIKE;
using namespace std;

HeadCommand::HeadCommand() {
	for (int i = 0; i < 3; i ++)
		mVelocity[i] = 0.0f;
}

HeadCommand::HeadCommand(const HeadCommand &command) {
	for (int i = 1; i < 3; i ++)
		mVelocity[i] = command.mVelocity[i];
}

void HeadCommand::operator= (const HeadCommand &command) {
	for (int i = 1; i < 3; i ++)
		mVelocity[i] = command.mVelocity[i];
}

HeadCommand::~HeadCommand() {

}

float HeadCommand::operator[] (const JointID id) const {
	if (id != 1 && id != 2)
		throw runtime_error("In 'const float HeadCommand::operator[] (const JointID id) const': Undefined joint id");
	return mVelocity[id];
}

float& HeadCommand::operator[] (const JointID id) {
	if (id != 1 && id != 2)
		throw runtime_error("In 'float& HeadCommand::operator[] (const JointID id)': Undefined joint id");
	return mVelocity[id];
}

string HeadCommand::toString() const {
	stringstream ss;
	for (int i = 1; i < 3; i ++)
		ss << (i == 1 ? "" : " ") << "(" << JointString[i] << " " << mVelocity[i] << ")";
	return ss.str();
}

