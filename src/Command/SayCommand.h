// M.I.K.E. 3D Soccer Simulation team
// File: SayCommand.h

#ifndef _MIKE_SAYCOMMAND_H
#define _MIKE_SAYCOMMAND_H

#include <string>

namespace MIKE {

	// Say command holds a message to send to other agents
	class SayCommand {

		std::string mMessage; // Message string

	public:

		SayCommand(); // Default constructor

		SayCommand(const SayCommand &command); // Copy constructor
		void operator= (const SayCommand &command);

		~SayCommand(); // Destructor

		// Getter functions
		std::string getMessage() const;

		// Setter functions
		void setMessage(const std::string &message);

		std::string toString() const;

	}; // class SayCommand

} // namespace MIKE

#endif // _MIKE_SAYCOMMAND_H

