// M.I.K.E. 3D Soccer Simulation team
// File: Connection.h
// Based on Asfaar base connection

#ifndef _MIKE_CONNECTION_H
#define _MIKE_CONNECTION_H

#include <TCPSocket.h>
#include <string>

namespace MIKE {

	// Connection class, holds the connection to the server
	class Connection {
	
		std::string mHost; // Host ip address
		unsigned mPort; // Host port number

		TCPSocket mSocket; // Connected socket to the server

	public:
	
		Connection(const std::string &host, const std::string &port); // Constructor
		~Connection(); // Destructor
	
		void connect(); // Connect to server
		void disconnect(); // Disconnect from server

		void send(const std::string& msg); // Send a message to server
		std::string receive(); // Receive a message from server

	private:

		bool selectInput();

	};

} // namespace MIKE

#endif //_MIKE_CONNECTION_H

