// M.I.K.E. 3D Soccer Simulation team
// File: Connection.cpp
// Based on Asfaar base connection

#include <Connection.h>
#include <stdexcept>
#include <iostream>
#include <netinet/in.h>
#include <string.h>
#include <sstream>

using namespace std;
using namespace MIKE;

Connection::Connection(const string &host, const string &port): mHost(host) {
	stringstream ss;
	ss << port;
	ss >> mPort;
}

Connection::~Connection() {
	disconnect();
}

void Connection::connect() {
	cout << "Connecting to TCP " << mHost << ":" << mPort << "\n";

	try
	{
		Addr local(INADDR_ANY,INADDR_ANY);
		mSocket.bind(local);
	}

	catch (const exception &error) {
		throw runtime_error("Failed to bind socket with '" + string(error.what()) + "'");
	}

	try
	{
		Addr server(mPort,mHost);
		mSocket.connect(server);
	}

	catch (const exception &error)
	{
		throw runtime_error("Connection failed with: '" + string(error.what()) + "'");
	}
}

void Connection::disconnect() {
	mSocket.close();
	cout << "Closing Connection to " << mHost << ":" << mPort << "\n";
}

void Connection::send(const string& msg) {
	if (msg.empty())
	{
		return;
	}

	unsigned int len = htonl(msg.size());
	string prefix((const char*)&len,sizeof(unsigned int));
	string str = prefix + msg;
	ssize_t si = write(mSocket.getFD(), str.data(), str.size());

	if (si == 0)
	{
	        throw runtime_error("send failed");
		return;
	}
}

string Connection::receive()
{
	if (!selectInput())
	{
		throw runtime_error("selectInput failed");
	}

	static char buffer[16 * 1024];
	memset(buffer, 0, sizeof(buffer));
	unsigned int msgLen = 0;
	unsigned int bytesRead = read(mSocket.getFD(), &msgLen, sizeof(unsigned int));
	msgLen = ntohl(msgLen);

	if (bytesRead < sizeof(unsigned int))
	{
		throw runtime_error("Receive: Disconnected");
	}

	unsigned msgRead = 0;
	char *offset = buffer;

	while (msgRead < msgLen)
	{
		msgRead += read(mSocket.getFD(), offset, msgLen - msgRead);
		offset += msgRead;
	}

	return string(buffer);
}

bool Connection::selectInput()
{
	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(mSocket.getFD(), &readfds);

	return select(mSocket.getFD()+1,&readfds, 0, 0, 0) > 0;
}

