// M.I.K.E. 3D Soccer Simulation team
// File: BasicPlan.h

#ifndef _MIKE_BASIC_PLAN_H
#define _MIKE_BASIC_PLAN_H

#include <string>

namespace MIKE{

	class WorldModel;

	class BasicPlan{
		
	public:
		BasicPlan();			// Constractor
		~BasicPlan();			// Distractor

		virtual bool isPlanDone() = 0;
		virtual bool decide() = 0;	// decides what to add to skill manager and return's a bool wheter did or not
	}; // class BasicPlan

} // namespace MIKE

#endif // _MIKE_BASIC_PLAN_H
