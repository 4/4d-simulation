// M.I.K.E. 3D Soccer Simulation team
// File: Ball.cpp

#include <Ball.h>
#include <ConfigManager.h>

using namespace MIKE;

Ball::Ball() : WMObject() {
	mRadius = ConfigManager::Instance()->get<float>("WorldModel.Ball.Radius");
	mMass = ConfigManager::Instance()->get<float>("WorldModel.Ball.Mass");
}

Ball::Ball(const Ball &ball) : WMObject(ball) {
	mRadius = ball.mRadius;
	mMass = ball.mMass;
}

void Ball::operator= (const Ball &ball) {
	mPosition = ball.mPosition;
	mRelativePosition = ball.mRelativePosition;
	mInVision = ball.mInVision;
	mPolarVision = ball.mPolarVision;

	mRadius = ball.mRadius;
	mMass = ball.mMass;
}

Ball::~Ball() {

}

float Ball::getRadius() const {
	return mRadius;
}

float Ball::getMass() const {
	return mMass;
}

void Ball::setRadius(const float radius) {
	mRadius = radius;
}

void Ball::setMass(const float mass) {
	mMass = mass;
}

