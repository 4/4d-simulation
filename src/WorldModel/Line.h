// M.I.K.E. 3D Soccer Simulation team
// File: Line.h
#ifndef _MIKE_LINE_H
#define _MIKE_LINE_H

#include <WMObject.h>
#include <SExpression.h>

namespace MIKE{
	class Line{
		int mType; 		// Which line is this
		WMObject *start;	// Start position in field of view
		WMObject *end;		// end position in field of view
	public:
		Line();
		~Line();

		void update(const SExpression&);

		void setType(int);

		int getType() const;
		const WMObject* getStart() const;
		const WMObject* getEnd() const;
	};
}

#endif //_MIKE_LINE_H
