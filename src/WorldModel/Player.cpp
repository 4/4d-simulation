#include <Player.h>

using namespace std;
using namespace MIKE;

Player::Player(){
	mTeam = "";
	mID = 0;
	head = new WMObject;
	rightLowerArm = new WMObject;
	leftLowerArm = new WMObject;
	rightFoot = new WMObject;
	leftFoot = new WMObject;
}

Player::~Player(){
	delete head;
	delete rightLowerArm;
	delete leftLowerArm;
	delete rightFoot;
	delete leftFoot;
}

void Player::update(const SExpression& se){
	mTeam = se.get("team")[0].value<string>();
	mID = se.get("id")[0].value<int>();
	if(se.get("head").size() != 0)
		head->setPolarVision(se.get("head")[0].get("pol")[0].value<Vector3f>());
	if(se.get("rlowerarm").size() != 0)
		rightLowerArm->setPolarVision(se.get("rlowerarm")[0].get("pol")[0].value<Vector3f>());
	if(se.get("llowerarm").size() != 0)
		leftLowerArm->setPolarVision(se.get("llowerarm")[0].get("pol")[0].value<Vector3f>());
	if(se.get("rfoot").size() != 0)
		rightFoot->setPolarVision(se.get("rfoot")[0].get("pol")[0].value<Vector3f>());
	if(se.get("lfoot").size() != 0)
		leftFoot->setPolarVision(se.get("lfoot")[0].get("pol")[0].value<Vector3f>());

	//TODO: set player position by this args
}

string Player::getTeam() const{
	return mTeam;
}

int Player::getID() const{
	return mID;
}

const WMObject* Player::getHead() const{
	return head;
}

const WMObject* Player::getRightLowerArm() const{
	return rightLowerArm;
}

const WMObject* Player::getLeftLowerArm() const{
	return leftLowerArm;
}

const WMObject* Player::getRightFoot() const{
	return rightFoot;
}

const WMObject* Player::getLeftFoor() const{
	return leftFoot;
}
