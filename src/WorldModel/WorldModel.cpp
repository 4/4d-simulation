// M.I.K.E. 3D Soccer Simulation Team
// File: WorldModel.cpp

#include <WorldModel.h>
#include <SExpression.h>
#include <Body.h>

using namespace std;
using namespace MIKE;

WorldModel::WorldModel() {
	flags = new Flag[FLAG_COUNT];
	for (int i = 0; i < FLAG_COUNT; i ++)
		flags[i].setFlagType(static_cast<FlagType>(i));
	
	body = new Body();
	ball = new Ball();
	gameState = new GameState;

	worldTime = 0;
}

WorldModel::~WorldModel() {
	delete body;
	delete gameState;
	delete flags;
}

void WorldModel::update(const string &message) {
	// TODO: Put update code here!
	string str = "(" + message + ")";
	SExpression se(str);

	worldTime = se.get("time")[0].get("now")[0].value<float>();

	body->update(se);
	gameState->update(se.get("GS")[0]);

	if(se.get("See").size() != 0){
		for(int i = 0 ; i < FLAG_COUNT ; i++){
			if(se.get("See")[0].get(FlagName[i]).size() != 0)
			{
				flags[i].setPolarVision(se.get("See")[0].get(FlagName[i])[0].get("pol")[0].value<Vector3f>());
				flags[i].setTimeLastSeen(worldTime);
			}
		}

		if(se.get("See")[0].get("B").size() != 0)
			ball->setPolarVision(se.get("See")[0].get("B")[0].get("pol")[0].value<Vector3f>());

		int linesCount = se.get("See")[0].get("L").size();
		for(unsigned int i = 0 ; i < mLines.size() ; i++)
		{
			delete (mLines[i]);
		}
		mLines.clear();
		//mLines.resize(linesCount , new Line());
		for(int i = 0 ; i < linesCount ; i++)
		{
			mLines.push_back(new Line());
			mLines[i]->update(se.get("See")[0].get("L")[i]);
		}
		
		//TODO: localize team can set lines types here

		int playersCount = se.get("See")[0].get("P").size();
		for(unsigned int i = 0 ; i < mPlayers.size() ; i++)
		{
			delete (mPlayers[i]);
		}
		mPlayers.clear();
		//mPlayers.resize(playersCount , new Player());
		for(int i = 0 ; i < playersCount ; i++)
		{
			mPlayers.push_back(new Player());
			mPlayers[i]->update(se.get("See")[0].get("P")[i]);
		}
	}
}

const Body* WorldModel::getBody() const {
	return body;
}

const GameState* WorldModel::getGameState() const{
	return gameState;
}

float WorldModel::getWorldTime() const{
	return worldTime;
}

const Ball* WorldModel::getBall() const{
	return ball;
}

const vector<Player*> WorldModel::getPlayers() const{
	return mPlayers;
}
