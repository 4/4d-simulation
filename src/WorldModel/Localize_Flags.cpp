#include "WorldModel.h"

#include <iostream>

#include <BasicCMath.h>
#include <Vector2.h>
#include <ConfigManager.h>

using namespace std;
using namespace MIKE;

Vector3f WorldModel::Realpos()
{
	/*for (int i=F1L  ; i<=G2R ; ++i)
		//cerr << flags[FlagType(i)].getPolarVision() << endl;
	//cerr << "-------------------------------------------------" << endl;*/

	//ETeamSide side=getTeamSide();

	//   std::cout<<"team side ="<<side<<std::endl;
	//int i,j;
	int array[8]={0};
	for (int i=F1L  ; i<=G2R ; ++i)
	{
		FlagType tmpvision = FlagType(i);
		if( 10.0f <=  10.0f - (worldTime - flags[tmpvision].getTimeLastSeen()))
		{
			array[i]=1;
			//std::cout<<"flag "<<i<<std::endl;
		}
	}
	seTxy(array);

	float sign = 1;
	if (!gameState->getTeam())
		sign = -1;
	Vector3f mMyPos = Vector3f(sign * x,sign * y, gRadToDeg(  normalizeAngle(  gDegToRad(z) )     )      );

	return mMyPos;
}

/**  set global X , Y  **/
void WorldModel::seTxy(int array[])
{
	/// will be deleted
	const double FieldLength = ConfigManager::Instance()->get<double>("WorldModel.Field.FieldLength");
	const double FieldWidth  = ConfigManager::Instance()->get<double>("WorldModel.Field.FieldWidth");
	const double GoalWidth   = ConfigManager::Instance()->get<double>("WorldModel.Field.GoalWidth");

 
	int i=-1 ,j=-1 ;
	Vector3f vector; 
	double tmpx, tmpy/*,tmpx1,tmpy1*/ ,tmpz,tmptheta=0,c_length;
	std::vector<double> all(13);
	if(array[0]==1 && array[1]==1)
	{
		//cerr << "1" << endl;
		i=0; j=1;
		tmptheta=seTij(i,j);


		c_length=length(flags[F1R].getPosition(),flags[F1L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldLength,'c');


		tmpy=seTh(FieldLength,tmptheta);
		tmpx=seTx('i',tmpy);
		//tmpx1=seTx('j',tmpy);
		if(flags[tmpvisionj].getPolarVision().x()-flags[tmpvisioni].getPolarVision().x()>(0))
		{

			//             x=(FieldLength / 2-//tmpx1);
			//             y=FieldWidth/2-seTy('j',//tmpx1);
			tmpz=all[5]*(-1);
			z=tmpz-all[11]+90;
			x=FieldLength / 2-all[7] ;
			y=FieldWidth/2-all[3];
		}
		else
		{
			//             x=tmpx-FieldLength / 2;
			//             y=FieldWidth/2-seTy('i',tmpx);
			z=all[4]-all[9]+90;
			x=all[6]-FieldLength / 2;
			y=FieldWidth/2-all[3];
		}
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"0,1###"<<"x= "<<x<<std::endl<<"y=" <<y<< std::endl;
		//std::cout<<"0,1###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl; 
	}


	else if(array[2]==1 && array[3]==1)
	{
		//cerr << "2" << endl;
		i=2; j=3; 
		tmptheta=seTij(i,j);

		c_length=length(flags[F2R].getPosition(),flags[F2L].getPosition());
		std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldLength,'c');

		tmpy=seTh(FieldLength,tmptheta);
		//          //tmpy1=seTy(
		tmpx=seTx('i',tmpy);
		//tmpx1=seTx('j',tmpy);
		if(flags[tmpvisionj].getPolarVision().x()-flags[tmpvisioni].getPolarVision().x()>(0))
		{

			//             x=(FieldLength / 2-//tmpx1);
			//             y=FieldWidth/2-seTy('j',//tmpx1);
			tmpz=all[5];
			z=tmpz-all[11]+270;
			x=FieldLength/2-all[7];
			y=all[3]-FieldWidth / 2;
		}
		else
		{
			//             x=tmpx-FieldLength / 2;
			//             y=FieldWidth/2-seTy('i',tmpx);
			tmpz=all[4]*(-1);
			z=tmpz-all[9]+270;
			x=all[6]-FieldLength/ 2;
			y=all[3]-FieldWidth/ 2;
		}

		// x=((FieldLength / 2-//tmpx1)+(tmpx-FieldLength / 2))/2;
		//y=tmpy-FieldWidth/2;
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"2,3###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl<<"H#####"<<tmpy<<std::endl;
		//tmpz=atan2Deg(//tmpx1,tmpy);
		//z=tmpz-flags[tmpvisionj].getPolarVision().y()+270;
		//std::cout<<"2,3###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisionj].getPolarVision().y()<< std::endl; 
	}

	else if(array[0]==1 && array[2]==1)
	{
		//cerr << "3" << endl;
		i=0 ; j=2;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1R].getPosition(),flags[F2R].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth,'c');

		tmpx=seTh(FieldWidth,tmptheta);
		tmpy=seTx('j',tmpx);
		//tmpy1=seTx('i',tmpx);
		if (flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(0))
		{
			y=all[7]-FieldWidth/2;
			z=all[5]-all[11];
			x=all[3]-FieldLength / 2;
		}
		else
		{
			y=all[6]*(-1)+FieldWidth/2;
			tmpz=all[4]*(-1);
			z=tmpz-all[9];
			x=all[3]-FieldLength / 2;

		}

		z=180+z;
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"0,2###"<<"x= "<<x<<std::endl<<"y=" <<y<< std::endl;
		//std::cout<<"0,2###"<<"z= "<<z<< std::endl;
	}

	else if(array[1]==1 && array[3]==1)
	{
		//cerr << "4" << endl;
		i=1 ; j=3;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1R].getPosition(),flags[F2R].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth,'c');

		tmpx=seTh(FieldWidth,tmptheta);
		tmpy=seTx('j',tmpx);
		//tmpy1=seTx('i',tmpx);
		x=FieldLength / 2-all[3]; 
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(0))
		{
			y=all[7]-FieldWidth/2;
			tmpz=all[5]*(-1);
			z= tmpz-all[11];
			x=FieldLength / 2-all[3];
			///             cout<<"ZZZ"<<z<<std::endl;
		}
		else
		{
			y=all[6]*(-1)+FieldWidth/2;
			x=FieldLength / 2-all[3];
			z=all[4]-all[9];
			///              cout<<"ZZ"<<z<<std::endl;
		}
		//          tmpz=atan2Deg(//tmpy1,tmpx);
		//               z=tmpz-flags[tmpvisioni].getPolarVision().y();
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		if(z<0)
			z+=360;
		//         for (int i=0; i<=12;i++)
		//          std::cout<<std::endl<<i<<"####"<<all[i]<<std::endl;
		//std::cout<<"1,3###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl;
		//std::cout<<"1,3###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl;
	}

	else if(array[0]==1 && array[4]==1)
	{
		//cerr << "5" << endl;
		i=0 ; j=4 ;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1L].getPosition(),flags[G1L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth/2-GoalWidth/2,'i');

		tmpx=seTh((FieldWidth/2.0f-GoalWidth/2.0f),tmptheta);
		tmpy=seTx('i',tmpx);
		//tmpy1=seTx('j',tmpx);
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(-.5))
		{
			x=all[3]-FieldLength/2;
			y=FieldWidth/2-all[6];
			tmpz=all[4]*(-1);
			z=tmpz-all[9];
			z=180+z;
		}
		else
		{
			x=all[3]-FieldLength/2;
			y=all[7]+(GoalWidth/2);
			tmpz=all[5]-all[11];
			z=180+tmpz;
		}
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"0,4###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl;
		//std::cout<<"0,4###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl;
	}
	else if(array[2]==1 && array[6]==1)
	{
		//cerr << "6" << endl;
		i=2 ; j=6 ;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1L].getPosition(),flags[G1L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth/2-GoalWidth/2,'i');

		tmpx=seTh((FieldWidth/2.0f-GoalWidth/2.0f),tmptheta);
		tmpy=seTx('i',tmpx);
		//tmpy1=seTx('j',tmpx);
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(-.5))
		{
			x=all[3]-FieldLength / 2;
			y=all[6]-FieldWidth/2;
			z=180+all[4]-all[9];
		}
		else
		{

			x=all[3]-FieldLength / 2;
			y=all[7]*(-1)+(-GoalWidth/2);
			tmpz=all[5]*(-1);
			z=180+tmpz-all[11];
		}
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"2,6###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl;
		//std::cout<<"2,6###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl;

	}

	else if(array[1]==1 && array[5]==1)
	{
		//cerr << "7" << endl;
		i=1 ; j=5;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1L].getPosition(),flags[G1L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth/2-GoalWidth/2,'i');

		tmpx=seTh((FieldWidth/2.0f-GoalWidth/2.0f),tmptheta);
		tmpy=seTx('i',tmpx);
		//tmpy1=seTx('j',tmpx);
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(-.5))
		{
			x=FieldLength / 2-all[3];
			y=FieldWidth/2-all[6];
			z=all[4]-all[9];
		}
		else
		{
			x=FieldLength / 2-all[3];
			y=all[7]+(GoalWidth/2);
			tmpz=all[5]*(-1);
			z=tmpz-all[11];
		}
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		if( z<0 )
			z+=360;
		//std::cout<<"1,5###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl;
		//std::cout<<"1,5###"<<"z= "<<z<<std::endl<<"tmpz###"<<tmpz<<std::endl<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl;
	}

	else if(array[3]==1 && array[7]==1)
	{
		//cerr << "8" << endl;
		i=3 ; j=7;
		tmptheta=seTij(i,j);

		c_length=length(flags[F1L].getPosition(),flags[G1L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(FieldWidth/2-GoalWidth/2,'i');

		tmpx=seTh((FieldWidth/2.0f-GoalWidth/2.0f),tmptheta);
		tmpy=seTx('i',tmpx);
		//tmpy1=seTx('j',tmpx);
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(-.5))
		{
			x=FieldLength / 2-all[3];
			y=all[6]-FieldWidth/2;
			tmpz=(-1)*all[4];
			z=tmpz-all[9];
		}
		else
		{
			x=FieldLength / 2-all[7];
			y=all[7]*(-1)+(-GoalWidth/2);
			z=all[5]-all[11];
		}
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		if( z<0 )
			z+=360;
		//std::cout<<"3,7###"<<"x= "<<x<<std::endl<<"y=" <<y<<sinDeg(tmptheta)<< std::endl;
		//std::cout<<"3,7###"<<"z= "<<z<<"tmpz###"<<tmpz<<"theta###"<<flags[tmpvisioni].getPolarVision().y()<< std::endl;
	}

	else if (array[4]==1 && array[6]==1)
	{
		//cerr << "9" << endl;
		i=4 ; j=6;
		tmptheta=seTij(i,j);

		c_length=length(flags[G1L].getPosition(),flags[G2L].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(GoalWidth,'i');

		tmpx=seTh(GoalWidth,tmptheta);
		tmpy=seTx('j',tmpx);
		//tmpy1=seTx('i',tmpx);
		if(flags[tmpvisionj].getPolarVision().x()-flags[tmpvisioni].getPolarVision().x()>(0))
		{
			y=all[7]+(-GoalWidth/2);
			tmpz=all[5];
			z=180+(tmpz-all[11]);
		}
		else
		{
			y=all[6]*(-1.0)+(GoalWidth/2);
			tmpz=all[4]*(-1);
			z=180+(tmpz-all[9]);
		}
		x=all[3]-FieldLength/2;
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		//std::cout<<"4,6###"<<"x= "<<x<<std::endl<<"y=" <<y<< std::endl;
		//std::cout<<"4,6###"<<"z= "<<z<<"tmpz###"<<tmpz<< std::endl;
	}

	else if (array[5]==1 && array[7]==1)
	{
		//cerr << "10" << endl;
		i=5 ; j=7;
		tmptheta=seTij(i,j);

		c_length=length(flags[G1R].getPosition(),flags[G2R].getPosition());
		///          std::cout <<"c_lentgh####"<<c_length<<std::endl;
		all=seTreal(GoalWidth,'i');

		tmpx=seTh(GoalWidth,tmptheta);
		tmpy=seTx('j',tmpx);
		//tmpy1=seTx('i',tmpx);
		///          std::cout<<"tmpy && //tmpy1= "<<tmpy<<" "<<//tmpy1<<endl;
		if(flags[tmpvisioni].getPolarVision().x()-flags[tmpvisionj].getPolarVision().x()>(0))
		{
			y=all[6]*(-1)+(GoalWidth/2);
			tmpz=all[4];
			z=tmpz-all[9];
		}
		else
		{
			y=all[7]+(-GoalWidth/2);
			tmpz=all[5]*(-1);
			z=tmpz-all[11];
		}
		x=FieldLength/2-all[3];
		z=z-body->findJoint(JID_HEAD_1)->getJointValue();
		if( z<0 )
			z+=360;
		//std::cout<<"5,7###"<<"z= "<<z<<"tmpz###"<<tmpz<<std::endl;
		//std::cout<<"5,7###"<<"x= "<<x<<std::endl<<"y=" <<y<< std::endl<<"sin####"<<sinDeg(tmptheta)<<std::endl<<"tmpx###"<<tmpx;
	}

	//cerr << x << "   " << y << "   " << z << endl;
}

/**set flag i & flag j **/
double WorldModel::seTij(int i, int j)
{
	double tmptheta=0;
	tmpvisioni = FlagType(i);
	tmpvisionj = FlagType(j);
	// tmpvision1i=flags[tmpvisioni].getPolarVision().x()
	//  flags[tmpvisioni].getPolarVision().x()=sqrt(pow(flags[tmpvisioni].getPolarVision().x(),2)-pow((flags[tmpvisioni].getPolarVision().x()*sinDeg(flags[tmpvisioni].getPolarVision().z())),2));
	//  flags[tmpvisionj].getPolarVision().x()=sqrt(pow(flags[tmpvisionj].getPolarVision().x(),2)-pow((flags[tmpvisionj].getPolarVision().x()*sinDeg(flags[tmpvisionj].getPolarVision().z())),2));
	tmptheta=fabs(flags[tmpvisioni].getPolarVision().y()-flags[tmpvisionj].getPolarVision().y());
	tmptheta=180-tmptheta;
	return tmptheta;

}

/**set hight**/
double WorldModel::seTh(double length,double tmptheta)
{
	return (sinDeg(tmptheta)*flags[tmpvisioni].getPolarVision().x()*flags[tmpvisionj].getPolarVision().x()/length);
}

/**set tmp Y **/
double WorldModel::seTx(char a , double tmpy)
{
	double tmp;
	if (a=='i')
	{ 
		tmp=(flags[tmpvisioni].getPolarVision().x()*flags[tmpvisioni].getPolarVision().x())-(tmpy*tmpy);
		//std::cout<<"tmpx^2###"<<tmp<<std::cout;
		if(tmp<0)
			tmp=tmp*(-1);
		return sqrt(tmp);
	}
	if (a=='j')
	{ 
		tmp=(flags[tmpvisionj].getPolarVision().x()*flags[tmpvisionj].getPolarVision().x())-(tmpy*tmpy);
		//std::cout<<"tmpx^2###"<<tmp<<std::cout;
		if(tmp<0)
			tmp=tmp*(-1);
		return sqrt(tmp);
	}

	return 0;
}

double WorldModel::length (Vector3f a, Vector3f b)
{
	Vector3f tmp;
	tmp.x()=a.x()-b.x();
	tmp.y()=a.y()-b.y();
	tmp.z()=a.z()-b.z();
	if( tmp.x()<tmp.y() )
		return fabs(tmp.y()); //sqrt(pow(tmp.y(),2)+pow(tmp.z(),2));
	else
		return fabs(tmp.x()); //sqrt(pow(tmp.x(),2)+pow(tmp.z(),2)); 
}

vector<double> WorldModel::seTreal (double c_length, char c)
{
	double costheta, tmpz=0.35 ,tmpz1=0.35;
	if(c=='g')
	{
		tmpz=0.45;
		tmpz1=0.45;
	}
	if(c=='i')
		tmpz1=0.45;
	std::vector <double> vector1(13);
	vector1[0]=sqrt(pow(flags[tmpvisioni].getPolarVision().x(),2)-pow(0.8-tmpz,2));//A
	vector1[1]=sqrt(pow(flags[tmpvisionj].getPolarVision().x(),2)-pow(0.8-tmpz1,2));//B
	costheta=(pow(vector1[0],2)+pow(vector1[1],2)-pow(c_length,2))/(2*vector1[0]*vector1[1]);//cos theta
	///  std::cout<<"cost####"<<costheta<<std::endl;
	vector1[2]=acosDeg(costheta);//theta
	vector1[3]=(vector1[0]*vector1[1]*sinDeg(180-vector1[2]))/c_length;//h=x=tmpx
	vector1[4]=acosDeg(vector1[3]/vector1[0]);//theta A
	vector1[5]=acosDeg(vector1[3]/vector1[1]);//theta B
	vector1[6]=sinDeg(vector1[4])*vector1[0];//y=tmpy A
	vector1[7]=sinDeg(vector1[5])*vector1[1];//y=tmpy B
	//vector1[6]=sqrt(pow(vector1[0],2)-pow(vector1[3],2));
	//vector1[7]=sqrt(pow(vector1[1],2)-pow(vector1[3],2));
	vector1[8]=sinDeg(flags[tmpvisioni].getPolarVision().y())*sinDeg(90-flags[tmpvisioni].getPolarVision().z());
	vector1[9]=asinDeg(vector1[8]);//tmpz A
	vector1[10]=sinDeg(flags[tmpvisionj].getPolarVision().y())*sinDeg(90-flags[tmpvisionj].getPolarVision().z());
	vector1[11]=asinDeg(vector1[10]);//tmpz B
	vector1[12]=vector1[5]*(-1);
	///  for (int i=0; i<=12;i++)
	///          std::cout<<std::endl<<i<<"####"<<vector1[i]<<std::endl;

	//for (unsigned int i = 0; i < vector1.size(); i++)
		//cerr << vector1[i] << "       ";
	//cerr << endl;
	return vector1;
}  
