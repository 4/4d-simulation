// M.I.K.E. 3D Soccer Simulation Team
// File: GameState.h

#ifndef _MIKE_GAMESTATE_H
#define _MIKE_GAMESTATE_H

#include <SExpression.h>
#include <string>

namespace MIKE{
	class GameState{
		int unum;		// Our unum
		bool team;		// Our team
		std::string playMode;	// Current play mode
		int scoreLeft;		// Score left
		int scoreRight;		// Score right
		float gameTime;		// Game time
	public:
		GameState();

		void update(const SExpression&);

		int getUnum() const;
		bool getTeam() const;
		std::string getPlayMode() const;
		int getScoreLeft() const;
		int getScoreRight() const;
		float getGameTime() const;
	}; // class GameState
} // namespace MIKE

#endif //_MIKE_GAMESTATE_H
