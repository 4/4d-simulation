#include <GameState.h>

using namespace std;
using namespace MIKE;

GameState::GameState(){
	unum = 0;
	team = 0;
	playMode = "NONE";
	scoreLeft = 0;
	scoreRight = 0;
	gameTime = 0;
}

void GameState::update(const SExpression& se){
	if(se.get("unum").size() != 0)
		unum = se.get("unum")[0].value<int>();
	if(se.get("team").size() != 0)
		team = (se.get("team")[0].value<string>() == "left");
	playMode = se.get("pm")[0].value<string>();
	scoreLeft = se.get("sl")[0].value<int>();
	scoreRight = se.get("sr")[0].value<int>();
	gameTime = se.get("t")[0].value<float>();
}

int GameState::getUnum() const{
	return unum;
}

bool GameState::getTeam() const{
	return team;
}

string GameState::getPlayMode() const{
	return playMode;
}

int GameState::getScoreLeft() const{
	return scoreLeft;
}

int GameState::getScoreRight() const{
	return scoreRight;
}

float GameState::getGameTime() const{
	return gameTime;
}
