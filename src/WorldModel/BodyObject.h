// M.I.K.E. 3D Soccer Simulation team
// File: BodyObject.h

#ifndef _MIKE_BODY_OBJECT_H
#define _MIKE_BODY_OBJECT_H

#include <WMObject.h>
#include <Vector3.h>
#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>

namespace MIKE {
	// this class Holds Body Part informations
	class BodyObject : public WMObject{
		std::string mName;			// Part's name
		BodyObject *par; 			// Part's parent Object
		std::vector<BodyObject*> mChilds;
		float mMass; 				// Part's Mass
		
		std::string mJointName;			// Joint's name
	    float mJointValue; 			// Joint's current angle
		Vector3f mJointAxis; 			// Joint's rotation axis;
	    float mMinJointValue; 			// Joint's min angle covered
	    float mMaxJointValue; 			// Joint's max angle covered
	public:
		BodyObject(); 				// Constractor
		~BodyObject();				// Distractor
		
		void setByPTree(const boost::property_tree::ptree& , BodyObject* = NULL);		// Set It's argumments by ptree
		BodyObject* findJoint(const std::string&);	// Finds joint by its JointName
		

		const std::string& getName() const;
		const BodyObject* getPar() const;
		const std::vector<BodyObject*> getChilds() const;
		float getMass() const;
		const std::string& getJointName() const;
	    float getJointValue() const;
		const Vector3f& getJointAxis() const;
	    float getMinJointValue() const;
	    float getMaxJointValue() const;
		
		void setJointValue(float);

	}; // class BodyObject
} // namespace MIKE
#endif // _MIKE_BODY_OBJECT_H
