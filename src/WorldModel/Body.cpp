// M.I.K.E. 3D Soccer Simulation team
// File: Body.cpp

#include <Body.h>
#include <ConfigManager.h>
#include <boost/property_tree/ptree.hpp>
#include "Vector3.h"
#include <Types.h>

using namespace std;
using namespace MIKE;
using namespace boost::property_tree;

Body::Body() : WMObject(),
			   mTorsoToLeftHip(Vector3f(-0.01, 0.055, -0.115)),
			   mTorsoToRightHip(Vector3f(-0.01, -0.055, -0.115)),
			   mHipToShank(Vector3f(0.005, 0.0, -0.120)),
			   mShankToAnkle(Vector3f(0.0, 0.0, -0.100)),
			   mTorsoToLeftToe(Vector3f(0.105, 0.055, -0.375)),
			   mTorsoToRightToe(Vector3f(0.105, -0.055, -0.375)),
			   mAnkleToToe(Vector3f(0.110, 0.0, -0.04)),

			   mTorsoToLeftShoulder(Vector3f(0.0, 0.098, 0.075)),
			   mTorsoToRightShoulder(Vector3f(0.0, -0.098, 0.075)),
			   mShoulderToElbow(Vector3f(0.09, 0.0, 0.009)),
			   mElbowToFinger(Vector3f(0.105, 0.0, 0.0)),
			   mTorsoToLeftFinger(Vector3f(0.195, 0.108, 0.084)),
			   mTorsoToRightFinger(Vector3f(0.195, -0.108, 0.084))
{
	hear = new Hear();
	torso = new BodyObject();
	ptree tmp = ConfigManager::Instance()->getConfigTree();
	tmp = tmp.get_child("Body.joint");
	torso->setByPTree(tmp);
	mTorsoRotationMatrix = Matrix4();
	mTorsoRotationMatrix.identity();
	mTorsoRotations = Vector3f();

	mGyro = Vector3f();
	mAccelerometer = Vector3f();

	bodyJoints.push_back(torso);
	for (int i = 1; i < JOINT_COUNT; i++)
		bodyJoints.push_back(torso->findJoint(FindJointName[i]));
}

Body::~Body(){
	delete hear;
	delete torso;
	for (unsigned int i = 0; i < bodyJoints.size(); i++)
		bodyJoints.pop_back();
}

void Body::update(const SExpression& se){
	vector<SExpression> ses = se.get("HJ");
	for(int i = 0 ; i < (int)ses.size() ; i++){
		BodyObject* change = findJoint(ses[i].get("n")[0].value<string>());
		change->setJointValue(ses[i].get("ax")[0].value<float>());
	}

	mGyro = se.get("GYR")[0].get("rt")[0].value<Vector3f>();
	mAccelerometer = se.get("ACC")[0].get("a")[0].value<Vector3f>();

	for(int i = 0 ; i < (int)se.get("FRP").size() ; i++){
		if(se.get("FRP")[i].get("n")[0].value<string>() == "lf"){
			mLeftFRP.first = se.get("FRP")[i].get("c")[0].value<Vector3f>();
			mLeftFRP.second = se.get("FRP")[i].get("f")[0].value<Vector3f>();
		}
		else{
			mRightFRP.first = se.get("FRP")[i].get("c")[0].value<Vector3f>();
			mRightFRP.second = se.get("FRP")[i].get("f")[0].value<Vector3f>();
		}
	}

	if(se.get("hear").size() != 0)
		hear->update(se.get("hear")[0].value<string>());
	
	forwardKinematics();
}

void Body::forwardKinematics() {
    LFK(false); // Right Leg FK
	LFK(true);  // Left Leg FK
	AFK(false);	// Right Arm FK
	AFK(true);	// Left Arm FK
}

void Body::LFK(bool left) {
	// Set Relative Vectors
	Vector3f torso2Hip = (left ? mTorsoToLeftHip : mTorsoToRightHip);
	Vector3f hip2Shank = mHipToShank;
	Vector3f shank2Ankle = mShankToAnkle;
	Vector3f ankle2Toe = mAnkleToToe; // Toe = Noke PA :D

	//Rotate Relative Vectors About Axis by Degrees of torso Rotation Matrix
	/*Vector3f degrees = mTorsoRotationMatrix;
	  degrees.z() = 0.0f;
	  torso2Hip = Vector3f::rotateAboutOAxis(torso2Hip, degrees);
	  hip2RShank = Vector3f::rotateAboutOAxis(hip2Shank, degrees);
	  shank2RAnkle = Vector3f::rotateAboutOAxis(shank2Ankle, degrees);*/

	// Prepare joints' Vectors
	Vector3f lj1V(0, 1, (left ? -1 : 1)); 	// lj1 Vector
	Vector3f lj2V(1, 0, 0); 				// lj2 Vector
	Vector3f lj3V(0, 1, 0); 				// lj3 Vector
	Vector3f lj4V(0, 1, 0); 				// lj4 Vector
	Vector3f lj5V(0, 1, 0); 				// lj5 Vector
	Vector3f lj6V(1, 0, 0); 				// lj6 Vector

	// Set joints' Angles
	float lj1A = findJoint(left ? "llj1" : "rlj1")->getJointValue() * DTR; // lj1 Angle
	float lj2A = findJoint(left ? "llj2" : "rlj2")->getJointValue() * DTR; // lj2 Angle
	float lj3A = -findJoint(left ? "llj3" : "rlj3")->getJointValue() * DTR; // lj3 Angle
	float lj4A = -findJoint(left ? "llj4" : "rlj4")->getJointValue() * DTR; // lj4 Angle
	float lj5A = -findJoint(left ? "llj5" : "rlj5")->getJointValue() * DTR; // lj5 Angle
	float lj6A = findJoint(left ? "llj6" : "rlj6")->getJointValue() * DTR; // lj6 Angle

	// Set lj2V
	Vector3f lj2P1(-1, 0, 0);
	Vector3f lj2P2(1, 0, 0);
	lj2V = Vector3f::rotateLine(lj1V, lj2P1, lj2P2, lj1A);

	// Set lj3V
	Vector3f lj3P1(0, -1, 0);
	Vector3f lj3P2(0, 1, 0);
	lj3V = Vector3f::rotateLine(lj1V, lj3P1, lj3P2, lj1A);
	lj3V = Vector3f::rotateLine(lj2V, lj3P1, lj3P2, lj2A);

	// Set lj4V
	Vector3f lj4P1 = hip2Shank + Vector3f(0, -1, 0);
	Vector3f lj4P2 = hip2Shank + Vector3f(0, 1, 0);
	lj4V = Vector3f::rotateLine(lj1V, lj4P1, lj4P2, lj1A);
	lj4V = Vector3f::rotateLine(lj2V, lj4P1, lj4P2, lj2A);
	lj4V = Vector3f::rotateLine(lj3V, lj4P1, lj4P2, lj3A);

	// Rotate hip2Shank about joint 1, 2, 3
	hip2Shank = Vector3f::rotateAboutAAxis(lj1V, hip2Shank, lj1A);
	hip2Shank = Vector3f::rotateAboutAAxis(lj2V, hip2Shank, lj2A);
	hip2Shank = Vector3f::rotateAboutAAxis(lj3V, hip2Shank, lj3A);

	// Rotate shank2Ankle about joint 1, 2, 3
	shank2Ankle = Vector3f::rotateAboutAAxis(lj1V, shank2Ankle, lj1A);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj2V, shank2Ankle, lj2A);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj3V, shank2Ankle, lj3A);

	// Set lj5V
	Vector3f lj5P1 = shank2Ankle + lj4P1 - hip2Shank;
	Vector3f lj5P2 = shank2Ankle + lj4P2 - hip2Shank;
	lj5V = Vector3f::rotateLine(lj4V, lj5P1, lj5P2, lj4A);

	// Rotate shank2Ankle about joint 4
	shank2Ankle = Vector3f::rotateAboutAAxis(lj4V, shank2Ankle, lj4A);

	// Set lj6V
	lj6V = shank2Ankle * lj5V;
	lj6V = lj6V.normalize();
	// Reset ankle2Toe
	Vector3f xAxis(1.0, 0.0, 0.0);
	float xDegree = Vector3f::angleBetween(xAxis, ankle2Toe);
	ankle2Toe = lj6V.normalize() * ankle2Toe.length();
	ankle2Toe = Vector3f::rotateAboutAAxis(lj5V, ankle2Toe, xDegree);


	// Set final lj6V
	lj6V = Vector3f::rotateAboutAAxis(lj5V, lj6V, lj5A);

	// Set Pos of joint 1, 2, 3
    findJoint(left ? "llj1" : "rlj1")->setRelativePosition(torso2Hip);
    findJoint(left ? "llj2" : "rlj2")->setRelativePosition(torso2Hip);
    findJoint(left ? "llj3" : "rlj3")->setRelativePosition(torso2Hip);

	// Set Pos of Joint 4
	Vector3f hipPos = findJoint(left ? "llj3" : "rlj3")->getRelativePosition();
    findJoint(left ? "llj4" : "rlj4")->setRelativePosition(hipPos + hip2Shank);

	// Set Pos of Joint 5
	Vector3f shankPos = findJoint(left ? "llj4" : "rlj4")->getRelativePosition();
    findJoint(left ? "llj5" : "rlj5")->setRelativePosition(shankPos + shank2Ankle);

	// Rotate ankle2Toe about joint 5, 6
	ankle2Toe = Vector3f::rotateAboutAAxis(lj5V, ankle2Toe, lj5A);
	ankle2Toe = Vector3f::rotateAboutAAxis(lj6V, ankle2Toe, lj6A);

	/*Vector3f zAxis(0.0, 0.0, 1.0);
	  cout << (left ? "Left" : "Right")  << " Z degree = " << Vector3f::angleBetweenAbout(ankle2Toe, xAxis, zAxis) * RTD << endl;*/

	// Set Pos of Toe to Joint6
	Vector3f anklePos = findJoint(left ? "llj5" : "rlj5")->getRelativePosition();
    findJoint(left ? "llj6" : "rlj6")->setRelativePosition(anklePos + ankle2Toe);

	// Set gloabal position of joints
	unsigned int minID = (left ? FindJointID.at("llj1") : FindJointID.at("rlj1"));
	unsigned int maxID = (left ? FindJointID.at("llj6") : FindJointID.at("rlj6"));
	for (unsigned int i = minID; i <= maxID; i++)
	    findJoint(i)->setPosition(torso->getPosition() + 
								  findJoint(i)->getRelativePosition());
}

void Body::AFK(bool left) {
	// Set Relative Pos
	Vector3f torso2Shoulder = (left ? mTorsoToLeftShoulder : mTorsoToRightShoulder);
	Vector3f shoulder2Elbow = mShoulderToElbow;
	Vector3f torso2Finger = (left ? mTorsoToLeftFinger : mTorsoToRightFinger);

	//Rotate Relative Vectors About Axis by Degrees of torso Rotation Matrix
	/*Vector3f degrees = mTorsoRotationMatrix;
	degrees.z() = 0.0f;
	torso2Shoulder = Vector3f::rotateAboutOAxis(torso2Shoulder, degrees);
	shoulder2Elbow = Vector3f::rotateAboutOAxis(shoulder2Elbow, degrees);
    torso2Finger = Vector3f::rotateAboutOAxis(torso2Finger, degrees);*/

	// Prepare joints' vectors
	Vector3f aj1V(0, 1, 0);
	Vector3f aj2V(0, 0, 1);
	Vector3f aj3V(1, 0, 0);
	Vector3f aj4V(0, 0, 1);

	// Set joints' angles
	float aj1A = -findJoint(left ? "laj1" : "raj1")->getJointValue() * DTR;
	float aj2A = findJoint(left ? "laj2" : "raj2")->getJointValue() * DTR;
	float aj3A = findJoint(left ? "laj3" : "raj3")->getJointValue() * DTR;
	float aj4A = findJoint(left ? "laj4" : "raj4")->getJointValue() * DTR;

	// Set aj2V
	Vector3f aj2P1(0, 0, -1);
	Vector3f aj2P2(0, 0, +1);
	aj2V = Vector3f::rotateLine(aj1V, aj2P1, aj2P2, aj1A);

	// Set aj3V
	Vector3f aj3P1(-1, 0, 0);
	Vector3f aj3P2(+1, 0, 0);
	aj3V = Vector3f::rotateLine(aj1V, aj3P1, aj3P2, aj1A);
	aj3V = Vector3f::rotateLine(aj2V, aj3P1, aj3P2, aj2A);

	// Set aj4V
	Vector3f aj4P1 = shoulder2Elbow + Vector3f(0, 0, -1);
	Vector3f aj4P2 = shoulder2Elbow + Vector3f(0, 0, +1);
	aj4V = Vector3f::rotateLine(aj1V, aj4P1, aj4P2, aj1A);
	aj4V = Vector3f::rotateLine(aj2V, aj4P1, aj4P2, aj2A);
	aj4V = Vector3f::rotateLine(aj3V, aj4P1, aj4P2, aj3A);

	// Rotate shoulder2Elbow about joint 1, 2, 3
	shoulder2Elbow = Vector3f::rotateAboutAAxis(aj1V, shoulder2Elbow, aj1A);
	shoulder2Elbow = Vector3f::rotateAboutAAxis(aj2V, shoulder2Elbow, aj2A);
	shoulder2Elbow = Vector3f::rotateAboutAAxis(aj3V, shoulder2Elbow, aj3A);

	// Set Pos of Joint 1, 2 => Shoulder Pos
    findJoint(left ? "laj1" : "raj1")->setRelativePosition(torso2Shoulder);
    findJoint(left ? "laj2" : "raj2")->setRelativePosition(torso2Shoulder);

	// Set Pos of Joint 3 => Elbow Pos
	Vector3f shoulderPos = findJoint(left ? "laj2" : "raj2")->getRelativePosition();
    findJoint(left ? "laj3" : "raj3")->setRelativePosition(shoulderPos + shoulder2Elbow);

	// Rotate torso2Finger about joint 1, 2, 3
	torso2Finger -= shoulderPos;
	torso2Finger = Vector3f::rotateAboutAAxis(aj1V, torso2Finger, aj1A);
	torso2Finger = Vector3f::rotateAboutAAxis(aj2V, torso2Finger, aj2A);
	torso2Finger = Vector3f::rotateAboutAAxis(aj3V, torso2Finger, aj3A);
	torso2Finger += shoulderPos;

	// Rotate torso2Finger about joint 4
	Vector3f elbowPos =  findJoint(left ? "laj3" : "raj3")->getRelativePosition();
	torso2Finger -= elbowPos;
	torso2Finger = Vector3f::rotateAboutAAxis(aj4V, torso2Finger, aj4A);
	torso2Finger += elbowPos;

	// Set Pos of Joint 4 => Finger Pos
    findJoint(left ? "laj4" : "raj4")->setRelativePosition(torso2Finger);

   	// Set gloabal position of joints
	unsigned int minID = (left ? FindJointID.at("laj1") : FindJointID.at("raj1"));
	unsigned int maxID = (left ? FindJointID.at("laj4") : FindJointID.at("raj4"));
	for (unsigned int i = minID; i <= maxID; i++)
	    findJoint(i)->setPosition(torso->getPosition() + 
								  findJoint(i)->getRelativePosition());
}

BodyObject* Body::findJoint(const std::string& name) {
	return bodyJoints[FindJointID.at(name)];
}

const BodyObject* Body::findJoint(int id) const{
	return bodyJoints[id];
}

BodyObject* Body::findJoint(int id){
	return bodyJoints[id];
}

BodyObject* Body::getTorso() const {
	return torso;
}

const Vector3f Body::getTorsoRotationsVector() const {
	return mTorsoRotations;
}

const Vector3f Body::getTorsoToLeftHip() const {
	return mTorsoToLeftHip;
}

const Vector3f Body::getTorsoToRightHip() const {
	return mTorsoToRightHip;
}

const Vector3f Body::getHipToShank() const {
	return mHipToShank;
}

const Vector3f Body::getShankToAnkle() const {
	return mShankToAnkle;
}

const Vector3f Body::getTorsoToLeftToe() const {
	return mTorsoToLeftToe;
}

const Vector3f Body::getTorsoToRightToe() const {
	return mTorsoToRightToe;
}

const Vector3f Body::getAnkleToToe() const {
	return mAnkleToToe;
}

const Vector3f Body::getTorsoToLeftShoulder() const {
	return mTorsoToLeftShoulder;
}

const Vector3f Body::getTorsoToRightShoulder() const {
	return mTorsoToRightShoulder;
}

const Vector3f Body::getShoulderToElbow() const {
	return mShoulderToElbow;
}

const Vector3f Body::getElbowToFinger() const {
	return mElbowToFinger;
}

const Vector3f Body::getTorsoToLeftFinger() const {
	return mTorsoToLeftFinger;
}

const Vector3f Body::getTorsoToRightFinger() const {
	return mTorsoToRightFinger;
}

Vector3f Body::getGyro() const{
	return mGyro;
}

Vector3f Body::getAccelerometer() const{
	return mAccelerometer;
}

pair<Vector3f , Vector3f> Body::getRightFRP() const{
	return mRightFRP;
}

pair<Vector3f , Vector3f> Body::getLeftFRP() const{
	return mLeftFRP;
}

const Hear* Body::getHear() const{
	return hear;
}
