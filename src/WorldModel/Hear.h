// M.I.K.E. 3D Soccer Simulation team
// File: Hear.h

#ifndef _MIKE_HEAR_H
#define _MIKE_HEAR_H

#include <string>

namespace MIKE{
	class Hear{
		std::string mTeam;	// Team name of voice source 
		float mTime;		// Time of hearing
		float mDirection;	// Direction of sound Source = -1 if the source is self
		std::string mMessage;	// Message we hear
	public:
		Hear();
		~Hear();

		void update(const std::string&);

		float getTime() const;
		float getDirection() const;
		std::string getMessage() const;
		std::string getTeam() const;
	};
}

#endif //_MIKE_HEAR_H
