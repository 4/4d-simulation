#include <Hear.h>
#include <sstream>

using namespace std;
using namespace MIKE;

Hear::Hear(){
	mTeam = "";
	mTime = 0;
	mDirection = -1;
	mMessage = "";
}

Hear::~Hear(){

}

void Hear::update(const string& s){
	stringstream ss(s);
	ss >> mTeam;
	ss >> mTime;
	string tmp;
	ss >> tmp;
	if(tmp == "self")
		mDirection = -1;
	else{
		stringstream tmpss(tmp);
		tmpss >> mDirection;
	}
	ss >> mMessage;
}

float Hear::getTime() const{
	return mTime;
}

float Hear::getDirection() const{
	return mDirection;
}

string Hear::getMessage() const{
	return mMessage;
}

string Hear::getTeam() const{
	return mTeam;
}
