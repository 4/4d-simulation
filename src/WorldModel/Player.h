// M.I.K.E. 3D Soccer Simulation team
// File: Player.h

#ifndef _MIKE_PLAYER_H
#define _MIKE_PLAYER_H

#include <SExpression.h>
#include <WMObject.h>

namespace MIKE{
	class Player : public WMObject{
		std::string mTeam;		// Team name
		int mID;			// Player ID
		WMObject* head;			// Head
		WMObject* rightLowerArm;	// Right lower arm
		WMObject* leftLowerArm;		// Left lower arm
		WMObject* rightFoot;		// Right foot
		WMObject* leftFoot;		// Left foot

	public:
		Player();
		~Player();

		void update(const SExpression&);

		std::string getTeam() const;
		int getID() const;
		const WMObject* getHead() const;
		const WMObject* getRightLowerArm() const;
		const WMObject* getLeftLowerArm() const;
		const WMObject* getRightFoot() const;
		const WMObject* getLeftFoor() const;
	};
}

#endif //_MIKE_PLAYER_H
