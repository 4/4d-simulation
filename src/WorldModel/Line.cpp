#include <Line.h>

using namespace std;
using namespace MIKE;

Line::Line(){
	mType = -1;
	start = new WMObject();
	end = new WMObject();
}

Line::~Line(){
	delete start;
	delete end;
}
void Line::update(const SExpression& se){
	start->setPolarVision(se.get("pol")[0].value<Vector3f>());
	end->setPolarVision(se.get("pol")[1].value<Vector3f>());
}

void Line::setType(int type){
	mType = type;
}

int Line::getType() const{
	return mType;
}

const WMObject* Line::getStart() const{
	return start;
}

const WMObject* Line::getEnd() const{
	return end;
}
