// M.I.K.E. 3D Soccer Simulation team
// File: WMObject.h

#ifndef _MIKE_WMOBJECT_H
#define _MIKE_WMOBJECT_H

#include <Vector3.h>

namespace MIKE {

	// Every object in the world model is a WorldModelObject
	class WMObject {

	protected:

		Vector3f mPosition; // Holds the position of object in 3d space
		Vector3f mRelativePosition; // Relative position to the torso

		bool mInVision; // Is the object in current vision?
		Vector3f mPolarVision; // Vision's polar position
		
	public:

		WMObject(); // Default constructor
		WMObject(const WMObject &object); // Copy constructor
		void operator= (const WMObject &object);
		virtual ~WMObject(); // Destructor

		// Getter functions
		Vector3f getPosition() const;
		Vector3f getRelativePosition() const;
		bool isInVision() const;
		Vector3f getPolarVision() const;

		// Setter functions
		void setPosition(const Vector3f &position);
		void setRelativePosition(const Vector3f &position);
		void setInVision(const bool inVision);
		void setPolarVision(const Vector3f &polarVision);

	}; // class WMObject

} // namespace MIKE

#endif // _MIKE_WMOBJECT_H

