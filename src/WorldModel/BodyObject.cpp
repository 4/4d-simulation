// M.I.K.E. 3D Soccer Simulation team
// File: BodyObject.cpp

#include <BodyObject.h>
#include <ConfigManager.h>

using namespace MIKE;
using namespace std;
using namespace boost::property_tree;

BodyObject::BodyObject() : WMObject(){
	mJointValue = 0;
	par = NULL;
}

BodyObject::~BodyObject(){
	delete par;
}

void BodyObject::setByPTree(const ptree& pt , BodyObject* parent){
	par = parent;
	mName = pt.get<string>("<xmlattr>.name");
	mMass = pt.get<float>("<xmlattr>.mass");
	mJointName = pt.get<string>("<xmlattr>.jname");
	mJointAxis = pt.get<Vector3f>("<xmlattr>.axis");
	mMinJointValue = pt.get<float>("<xmlattr>.min");
	mMaxJointValue = pt.get<float>("<xmlattr>.max");

	for(ptree::const_iterator it = pt.begin() ; it != pt.end() ; it++){
		if(it->first != "<xmlattr>"){
			BodyObject* tmp = new BodyObject();
			tmp->setByPTree(it->second ,  this);
			mChilds.push_back(tmp);
		}
	}
}

BodyObject* BodyObject::findJoint(const string& jname){
	if(jname == mJointName)
		return this;
	for(int i = 0 ; i < (int)mChilds.size() ; i++)
		if(mChilds[i]->findJoint(jname))
			return mChilds[i]->findJoint(jname);
	return NULL;
}

const string& BodyObject::getName() const{
	return mName;
}

const BodyObject* BodyObject::getPar() const{
	return par;
}

const string& BodyObject::getJointName() const{
	return mJointName;
}

const vector<BodyObject*> BodyObject::getChilds() const{
	return mChilds;
}

float BodyObject::getJointValue() const{
	return mJointValue;
}

const Vector3f& BodyObject::getJointAxis() const{
	return mJointAxis;
}

float BodyObject::getMinJointValue() const{
	return mMinJointValue;
}

float BodyObject::getMaxJointValue() const{
	return mMaxJointValue;
}

void BodyObject::setJointValue(float jointValue){
	mJointValue = jointValue;
}

