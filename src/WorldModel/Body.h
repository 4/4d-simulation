// M.I.K.E. 3D Soccer Simulation team
// File: Body.h

#ifndef _MIKE_BODY_H
#define _MIKE_BODY_H

#include <WMObject.h>
#include <BodyObject.h>
#include <ConfigManager.h>
#include <SExpression.h>
#include <Matrix4.h>
#include <Vector3.h>
#include <Hear.h>

#include <string>
#include <vector>

namespace MIKE{
	//holds all body informations
	class Body : public WMObject{
		Matrix4 mTorsoRotationMatrix;	// Rotation matrix of torso
		Vector3f mTorsoRotations;	// Rotation's of torso on each axis x y z	
		BodyObject* torso; 		// A BodyObject as a root of body tree
		std::vector<BodyObject*> bodyJoints;

		const Vector3f mTorsoToLeftHip;
		const Vector3f mTorsoToRightHip;
		const Vector3f mHipToShank;
		const Vector3f mShankToAnkle;
		const Vector3f mTorsoToLeftToe;
		const Vector3f mTorsoToRightToe;
		const Vector3f mAnkleToToe;

		const Vector3f mTorsoToLeftShoulder;
		const Vector3f mTorsoToRightShoulder;
		const Vector3f mShoulderToElbow;
		const Vector3f mElbowToFinger;
		const Vector3f mTorsoToLeftFinger;
		const Vector3f mTorsoToRightFinger;

		Vector3f mGyro;			// GyroRate, Current angular velocities along three axes ... deg / sec
		Vector3f mAccelerometer;	// Accelerometer, Current acceleration along three axes ... meter / (sec ^ 2)

		std::pair<Vector3f , Vector3f> mRightFRP;	// Right foot force resistance, first = local coordinates of origin of the aplied force meter
									//second = the components of the force vector. length of the vector represents force in newton
		std::pair<Vector3f , Vector3f> mLeftFRP;	// Left foot force resistance, just as above.

		Hear* hear;					// What we hear now
	public:
		Body();				// Constractor
		~Body();			// Distractor

		void update(const SExpression&);	// update's body informations

	    void forwardKinematics();			// FK that fills BodyObjects positions by joint angles
		void LFK(bool left); // Leg forwardKinematics
		void AFK(bool left); // Arm forwardKinematics

		BodyObject* findJoint(const std::string& name);
		BodyObject* findJoint(int id);
		const BodyObject* findJoint(int id) const;

	    BodyObject* getTorso() const;
		const Vector3f getTorsoRotationsVector() const;

		const Vector3f getTorsoToLeftHip() const;
		const Vector3f getTorsoToRightHip() const;
		const Vector3f getHipToShank() const;
		const Vector3f getShankToAnkle() const;
		const Vector3f getTorsoToLeftToe() const;
		const Vector3f getTorsoToRightToe() const;
		const Vector3f getAnkleToToe() const;

		const Vector3f getTorsoToLeftShoulder() const;
		const Vector3f getTorsoToRightShoulder() const;
		const Vector3f getShoulderToElbow() const;
		const Vector3f getElbowToFinger() const;
		const Vector3f getTorsoToLeftFinger() const;
		const Vector3f getTorsoToRightFinger() const;

		Vector3f getGyro() const;
		Vector3f getAccelerometer() const;
		std::pair<Vector3f , Vector3f> getRightFRP() const;
		std::pair<Vector3f , Vector3f> getLeftFRP() const;

		const Hear* getHear() const;
	}; // class Body
} // namespace MIKE

#endif // _MIKE_BODY_H
