// M.I.K.E. 3D Soccer Simulation Team
// File: WorldModel.h

#ifndef _MIKE_WORLDMODEL_H
#define _MIKE_WORLDMODEL_H

#include <string>
#include <vector>

#include <Flag.h>
#include <Types.h>
#include <Ball.h>
#include <GameState.h>
#include <Line.h>
#include <Player.h>
#include "Body.h"

namespace MIKE {
    
	class WorldModel : public Singleton<WorldModel> {
	private:

		// Store world model objects
		Flag* flags;			// Field flags
		GameState *gameState;		// Current game state
		std::vector<Line*> mLines;	// Lines we see
		// TODO: store all players and just update their positions by what we see
		std::vector<Player*> mPlayers;	// players we see

		float worldTime;		// World Time
		Body* body;			// Our Body
		Ball* ball;			// Ball

	private: /// localize
		float x, y, z;
		FlagType tmpvisioni;
		FlagType tmpvisionj;

		Vector3f Realpos();
		void seTxy(int array[]);
		double seTij(int i, int j);
		double seTh(double length,double tmptheta);
		double seTx(char a, double tmpy);
		double length (Vector3f a, Vector3f b);
		std::vector<double> seTreal (double c_length, char c);

	public:

		WorldModel(); // Default constructor
		
		~WorldModel(); // Destructor

		void update(const std::string &message); // Update world model by server message

		float getWorldTime() const;
		const Body* getBody() const;
		const GameState* getGameState() const;
		const Ball* getBall() const;
		const std::vector<Player*> getPlayers() const;

	}; // class WorldModel

} // namespace MIKE

#endif // _MIKE_WORLDMODEL_H

