// M.I.K.E. 3D Soccer Simulation team
// File: Ball.h

#ifndef _MIKE_BALL_H
#define _MIKE_BALL_H

#include <WMObject.h>

namespace MIKE {

	// This class holds the ball information in world model
	class Ball : public WMObject {

		float mRadius; // Ball radius
		float mMass; // Ball mass

	public:

		Ball(); // Default constructor
		Ball(const Ball &ball); // Copy constructor
		void operator= (const Ball &ball);
		~Ball(); // Destructor

		// Getter functions
		float getRadius() const;
		float getMass() const;

		// Setter functions
		void setRadius(const float radius);
		void setMass(const float mass);

	}; // class Ball

} // namespace MIKE

#endif // _MIKE_BALL_H

