// M.I.K.E. 3D Soccer Simulation team
// File: WMObject.cpp

#include <WMObject.h>
using namespace MIKE;

WMObject::WMObject() : mInVision(false) {

}

WMObject::WMObject(const WMObject &object) {
	mPosition = object.mPosition;
	mRelativePosition = object.mRelativePosition;
	mInVision = object.mInVision;
	mPolarVision = object.mPolarVision;
}

void WMObject::operator= (const WMObject &object) {
	mPosition = object.mPosition;
	mRelativePosition = object.mRelativePosition;
	mInVision = object.mInVision;
	mPolarVision = object.mPolarVision;
}

WMObject::~WMObject() {

}

Vector3f WMObject::getPosition() const {
	return mPosition;
}

Vector3f WMObject::getRelativePosition() const {
	return mRelativePosition;
}

bool WMObject::isInVision() const {
	return mInVision;
}

Vector3f WMObject::getPolarVision() const {
	return mPolarVision;
}

void WMObject::setPosition(const Vector3f &position) {
	mPosition = position;
}

void WMObject::setRelativePosition(const Vector3f &position) {
	mRelativePosition = position;
}

void WMObject::setInVision(const bool inVision) {
	mInVision = inVision;
}

void WMObject::setPolarVision(const Vector3f &polarVision) {
	mPolarVision = polarVision;
}

