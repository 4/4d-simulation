// M.I.K.E. 3D Soccer Simulation team
// File: Flag.h

#ifndef _MIKE_FLAG_H
#define _MIKE_FLAG_H

#include <WMObject.h>
#include <Types.h>

namespace MIKE {

	// Flag class
	class Flag : public WMObject {

		FlagType mFlagType; // Flag type
		float timeLastSeen;

	public:

		Flag(); // Default constructor
		Flag(const Flag &flag); // Copy constructor
		void operator= (const Flag &flag);
		~Flag(); // Destructor
		
		// Getter functions
		FlagType getFlagType() const;
		float getTimeLastSeen() const;

		// Setter functions
		void setFlagType(const FlagType flagType);
		void setTimeLastSeen(const float t);

	}; // class Flag

} // namespace MIKE

#endif // _MIKE_FLAG_H

