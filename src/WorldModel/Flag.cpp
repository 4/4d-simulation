// M.I.K.E. 3D Soccer Simulation team
// File: Flag.cpp

#include <Flag.h>
#include <ConfigManager.h>
using namespace MIKE;

Flag::Flag() : WMObject(), mFlagType(FT_UNKNOWN) {

}

Flag::Flag(const Flag &flag) : WMObject(flag) {
	mFlagType = flag.mFlagType;
}

void Flag::operator= (const Flag &flag) {
	mPosition = flag.mPosition;
	mRelativePosition = flag.mRelativePosition;
	mInVision = flag.mInVision;
	mPolarVision = flag.mPolarVision;

	mFlagType = flag.mFlagType;
}

Flag::~Flag() {

}

FlagType Flag::getFlagType() const {
	return mFlagType;
}

float Flag::getTimeLastSeen() const
{
	return timeLastSeen;
}

void Flag::setFlagType(const FlagType flagType) {
	mFlagType = flagType;

	// Set position
	if(mFlagType == FT_F_1L)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_F_1L");
	else if(mFlagType == FT_F_2L)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_F_2L");
	else if(mFlagType == FT_F_1R)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_F_1R");
	else if(mFlagType == FT_F_2R)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_F_2R");
	else if(mFlagType == FT_G_1L)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_G_1L");
	else if(mFlagType == FT_G_2L)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_G_2L");
	else if(mFlagType == FT_G_1R)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_G_1R");
	else if(mFlagType == FT_G_2R)
		mPosition = ConfigManager::Instance()->get<Vector3f>("WorldModel.Flags.FT_G_2R");
}

void Flag::setTimeLastSeen(const float t)
{
	timeLastSeen = t;
}
