// M.I.K.E. 3D Soccer Simulation team
// File: Types.cpp

#include <Types.h>
#include <Vector3.h>

using namespace std;
using namespace MIKE;

const string MIKE::JointString[JOINT_COUNT] = {

	"",			// JID_TORSO				= 0,

	"he1",		// JID_NECK_YAW				= 1,
	"he2",		// JID_NECK_PITCH			= 2,
	"lae1",		// JID_LSHOULDER_PITCH		= 3,
	"lae2",		// JID_LSHOULDER_YAW		= 4,
	"lae3",		// JID_LSHOULDER_ROLL		= 5,
	"lae4",		// JID_LARM_YAW				= 6,
	"lle1",		// JID_LHIP_YAWPITCH		= 7,
	"lle2",		// JID_LHIP_ROLL			= 8,
	"lle3",		// JID_LHIP_PITCH			= 9,
	"lle4",		// JID_LKNEE_PITCH			= 10,
	"lle5",		// JID_LFOOT_PITCH			= 11,
	"lle6",		// JID_LFOOT_ROLL			= 12,
	"rae1",		// JID_RSHOULDER_PITCH		= 13,
	"rae2",		// JID_RSHOULDER_YAW		= 14,
	"rae3",		// JID_RSHOULDER_ROLL		= 15,
	"rae4",		// JID_RARM_YAW				= 16,
	"rle1",		// JID_RHIP_YAWPITCH		= 17,
	"rle2", 	// JID_RHIP_ROLL			= 18,
	"rle3",		// JID_RHIP_PITCH			= 19,
	"rle4",		// JID_RKNEE_PITCH			= 20,
	"rle5",		// JID_RFOOT_PITCH			= 21,
	"rle6",		// JID_RFOOT_ROLL			= 22,

};

const string MIKE::FindJointName[JOINT_COUNT] = {

	"",			// JID_TORSO				= 0,

	"hj1",		// JID_NECK_YAW				= 1,
	"hj2",		// JID_NECK_PITCH			= 2,
	"laj1",		// JID_LSHOULDER_PITCH		= 3,
	"laj2",		// JID_LSHOULDER_YAW		= 4,
	"laj3",		// JID_LSHOULDER_ROLL		= 5,
	"laj4",		// JID_LARM_YAW				= 6,
	"llj1",		// JID_LHIP_YAWPITCH		= 7,
	"llj2",		// JID_LHIP_ROLL			= 8,
	"llj3",		// JID_LHIP_PITCH			= 9,
	"llj4",		// JID_LKNEE_PITCH			= 10,
	"llj5",		// JID_LFOOT_PITCH			= 11,
	"llj6",		// JID_LFOOT_ROLL			= 12,
	"raj1",		// JID_RSHOULDER_PITCH		= 13,
	"raj2",		// JID_RSHOULDER_YAW		= 14,
	"raj3",		// JID_RSHOULDER_ROLL		= 15,
	"raj4",		// JID_RARM_YAW				= 16,
	"rlj1",		// JID_RHIP_YAWPITCH		= 17,
	"rlj2", 	// JID_RHIP_ROLL			= 18,
	"rlj3",		// JID_RHIP_PITCH			= 19,
	"rlj4",		// JID_RKNEE_PITCH			= 20,
	"rlj5",		// JID_RFOOT_PITCH			= 21,
	"rlj6",		// JID_RFOOT_ROLL			= 22,
};

const std::map<std::string, int> MIKE::FindJointID =  std::map<std::string, int>{
	std::pair<std::string, int>("",		0),				// JID_TORSO

	std::pair<std::string, int>("hj1",	1),			// JID_NECK_YAW
	std::pair<std::string, int>("hj2",	2),			// JID_NECK_PITCH
	std::pair<std::string, int>("laj1",	3),			// JID_LSHOULDER_PITCH
	std::pair<std::string, int>("laj2",	4),			// JID_LSHOULDER_YAW
	std::pair<std::string, int>("laj3",	5),			// JID_LSHOULDER_ROLL
	std::pair<std::string, int>("laj4",	6),			// JID_LARM_YAW
	std::pair<std::string, int>("llj1",	7),			// JID_LHIP_YAWPITCH
	std::pair<std::string, int>("llj2",	8),			// JID_LHIP_ROLL
	std::pair<std::string, int>("llj3",	9),			// JID_LHIP_PITCH
	std::pair<std::string, int>("llj4",	10),		// JID_LKNEE_PITCH
	std::pair<std::string, int>("llj5",	11),		// JID_LFOOT_PITCH
	std::pair<std::string, int>("llj6",	12),		// JID_LFOOT_ROLL
	std::pair<std::string, int>("raj1",	13),		// JID_RSHOULDER_PITCH
	std::pair<std::string, int>("raj2",	14),		// JID_RSHOULDER_YAW
	std::pair<std::string, int>("raj3",	15),		// JID_RSHOULDER_ROLL
	std::pair<std::string, int>("raj4",	16),		// JID_RARM_YAW
	std::pair<std::string, int>("rlj1",	17),		// JID_RHIP_YAWPITCH
	std::pair<std::string, int>("rlj2",	18),		// JID_RHIP_ROLL
	std::pair<std::string, int>("rlj3",	19),		// JID_RHIP_PITCH
	std::pair<std::string, int>("rlj4",	20),		// JID_RKNEE_PITCH
	std::pair<std::string, int>("rlj5",	21),		// JID_RFOOT_PITCH
	std::pair<std::string, int>("rlj6",	22),		// JID_RFOOT_ROLL
};

const std::string MIKE::FlagName[FLAG_COUNT] = {
		//FT_UNKNOWN = -1,
		
	"F1L",	//FT_F_1L = 0,
	"F1R",	//FT_F_1R = 1,
	"F2L",	//FT_F_2L = 2,
	"F2R",	//FT_F_2R = 3,
	"G1L",	//FT_G_1L = 4,
	"G1R",	//FT_G_1R = 5,
	"G2L",	//FT_G_2L = 6,
	"G2R",	//FT_G_2R = 7,

		//FLAG_COUNT = 8
};

