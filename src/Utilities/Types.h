// M.I.K.E. 3D Soccer Simulation team
// File: Types.h

#ifndef _MIKE_TYPES_H
#define _MIKE_TYPES_H

#include <string>
#include <map>

namespace MIKE {

	struct VisionSense;

	// Basic types
	typedef signed char Sint8;
	typedef unsigned char Uint8;
	typedef signed int Sint16;
	typedef unsigned int Uint16;
	typedef signed long int Sint32;
	typedef unsigned long int Uint32;
	typedef signed long long int Sint64;
	typedef unsigned long long int Uint64;

	// Enum for different coordinate systems
	enum CoordSystem {
		CARTESIAN = 1,
		POLAR = 2,
		DPOLAR = 3
	};

	enum FlagType {
		FT_UNKNOWN = -1,
		
		FT_F_1L = 0,
		FT_F_1R = 1,
		FT_F_2L = 2,
		FT_F_2R = 3,
		FT_G_1L = 4,
		FT_G_1R = 5,
		FT_G_2L = 6,
		FT_G_2R = 7,

		FLAG_COUNT = 8,

		F1L = 0,
		F1R = 1,
		F2L = 2,
		F2R = 3,
		G1L = 4,
		G1R = 5,
		G2L = 6,
		G2R = 7
	};

	// This is an enumeration of all the joints currently avalable in simulator
	enum JointID {
		JID_ROOT		= 0,
		JID_TORSO		= 0,

		JID_HEAD_1		= 1,
		JID_HEAD_2		= 2,
		JID_LARM_1		= 3,
		JID_LARM_2		= 4,
		JID_LARM_3		= 5,
		JID_LARM_4		= 6,
		JID_LLEG_1		= 7,
		JID_LLEG_2		= 8,
		JID_LLEG_3		= 9,
		JID_LLEG_4		= 10,
		JID_LLEG_5		= 11,
		JID_LLEG_6		= 12,
		JID_RARM_1		= 13,
		JID_RARM_2		= 14,
		JID_RARM_3		= 15,
		JID_RARM_4		= 16,
		JID_RLEG_1		= 17,
		JID_RLEG_2		= 18,
		JID_RLEG_3		= 19,
		JID_RLEG_4		= 20,
		JID_RLEG_5		= 21,
		JID_RLEG_6		= 22,

		JID_NECK_YAW			= 1,
		JID_NECK_PITCH			= 2,
		JID_LSHOULDER_PITCH		= 3,
		JID_LSHOULDER_YAW		= 4,
		JID_LSHOULDER_ROLL		= 5,
		JID_LARM_YAW			= 6,
		JID_LHIP_YAWPITCH		= 7,
		JID_LHIP_ROLL			= 8,
		JID_LHIP_PITCH			= 9,
		JID_LKNEE_PITCH			= 10,
		JID_LFOOT_PITCH			= 11,
		JID_LFOOT_ROLL			= 12,
		JID_RSHOULDER_PITCH		= 13,
		JID_RSHOULDER_YAW		= 14,
		JID_RSHOULDER_ROLL		= 15,
		JID_RARM_YAW			= 16,
		JID_RHIP_YAWPITCH		= 17,
		JID_RHIP_ROLL			= 18,
		JID_RHIP_PITCH			= 19,
		JID_RKNEE_PITCH			= 20,
		JID_RFOOT_PITCH			= 21,
		JID_RFOOT_ROLL			= 22,


		JID_HEAD_NECK			= 1,
		JID_HEAD_HEAD			= 2,
		JID_LARM_SHOULDER		= 3,
		JID_LARM_UPPERARM		= 4,
		JID_LARM_ELBOW			= 5,
		JID_LARM_LOWERARM		= 6,
		JID_LLEG_HIP1			= 7,
		JID_LLEG_HIP2			= 8,
		JID_LLEG_THIGH			= 9,
		JID_LLEG_SHANK			= 10,
		JID_LLEG_ANKLE			= 11,
		JID_LLEG_FOOT			= 12,
		JID_RARM_SHOULDER		= 13,
		JID_RARM_UPPERARM		= 14,
		JID_RARM_ELBOW			= 15,
		JID_RARM_LOWERARM		= 16,
		JID_RLEG_HIP1			= 17,
		JID_RLEG_HIP2			= 18,
		JID_RLEG_THIGH			= 19,
		JID_RLEG_SHANK			= 20,
		JID_RLEG_ANKLE			= 21,
		JID_RLEG_FOOT			= 22,

		JOINT_COUNT		= 23
	};

	// This array holds all the joint names used in command
	extern const std::string JointString[JOINT_COUNT];

	extern const std::string FindJointName[JOINT_COUNT];

	extern const std::map<std::string, int> FindJointID;


	extern const std::string FlagName[FLAG_COUNT];

	// enum for IK
	enum IKMod {
		IK_Ankle	= 1,
		IK_Toe		= 2
	};

} // namespace MIKE

#endif // _MIKE_TYPES_H

