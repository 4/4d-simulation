// M.I.K.E. 3D Soccer Simulation Team
// File: SExpression.cpp

#include <SExpression.h>
#include <sstream>
#include <algorithm>
#include <functional>

using namespace std;
using namespace MIKE;

SExpression::SExpression(const string &str, bool good) {
	setString(str, good);
}

SExpression::~SExpression() {

}

vector<SExpression> SExpression::get(const string &goal) const {
	vector<SExpression> res;
	stringstream input(str), output;
	int depth = 0;
	bool receive = false;

	while (input.good()) {
		string s;
		input >> s;

		if (s == "(" && ++ depth == 2) {
			string name;
			input >> name;

			if (name == goal) {
				receive = true;
				output << "( " << goal << " ";
			}
		} else if (s == ")" && depth -- == 2 && receive) {
			receive = false;
			output << ")";
			res.push_back(SExpression(output.str(), true));
			output.str("");
			output.clear();
		} else if (receive)
			output << s << " ";
	}

	return res;
}

void SExpression::setString(const string &s, bool good) {
	if (good)
		str = s;
	else
		set(s);
}

string SExpression::getString() const {
	return str;
}

void SExpression::set(const string &s) {
	str = "";
	for (int i = 0; i < (int) s.size(); i ++)
		if (s[i] == '(' || s[i] == ')')
			str += " " + string(1, s[i]) + " ";
		else
			str += s[i];

	auto iter = unique(str.begin(), str.end(), [](char a, char b) -> bool { return (a == b) && (a == ' '); });
	str.erase(iter, str.end());
}

