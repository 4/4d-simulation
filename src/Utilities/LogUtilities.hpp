//M.I.K.E 3D Soccer Simulation Team
//LogUtilities.h

#ifndef _MIKE_LOGUTILITIES_H
#define _MIKE_LOGUTILITIES_H


#include <iostream>

using namespace std;

namespace MIKE 
{

	//This function prints out the input and returns it again.
	template<class Type>
	Type printOut(const Type mOut)
	{
		cout << mOut << endl;
		return mOut;
	}


}//namspace MIKE


#endif
