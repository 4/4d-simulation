// M.I.K.E. 3D Soccer Simulation Team
// File: SExpression.h

#ifndef _MIKE_SEXPRESSION_H
#define _MIKE_SEXPRESSION_H

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>

namespace MIKE {

	class SExpression {

		std::string str;

	public:

		SExpression(const std::string &str, bool good = false);
		~SExpression();

		std::vector<SExpression> get(const std::string &goal) const; // Return the child SExpression with the given name

		void setString(const std::string &s, bool good = false);
		std::string getString() const;

		template<class Type> Type value() const;

	private:

		void set(const std::string &s);

	}; // class SExpression

	template<class Type> Type SExpression::value() const {
		std::string res, tmp;
		std::stringstream ss(str);

		for (int n = 1; ss >> tmp && tmp != ")"; n ++)
			if (n > 2)
				res += (n == 3 ? "" : " ") + tmp;

		return boost::lexical_cast<Type>(res);
	}

} // namespace MIKE

#endif // _MIKE_SEXPRESSION_H

