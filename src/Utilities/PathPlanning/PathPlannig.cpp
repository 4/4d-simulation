#include "PathPlanning.h"

#include "PPMath.h"
#include "PPGraph.h"

using namespace std;
using namespace MIKE;
using namespace pp_math;
using namespace pp_graph;

PathPlanning::PathPlanning()
{
}

PathPlanning::PathPlanning(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle, 
		const deque<Obstacle> obstacles)
{
	mPath = getPathPlanning_basic(startPos, startAngle, endPos, endAngle, obstacles);
}

PathPlanning::PathPlanning(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle)
{
	deque<Obstacle> obstacles;
	mPath = getPathPlanning_basic(startPos, startAngle, endPos, endAngle, obstacles);
}

PathPlanning::PathPlanning(const Vector2f startPos, const Vector2f endPos, const deque<Obstacle> obstacles)
{
	mPath = getPathPlanning(startPos, endPos, obstacles);
}

PathPlanning::PathPlanning(const Vector2f startPos, const Vector2f endPos)
{
	deque<Obstacle> obstacles;
	mPath = getPathPlanning(startPos, endPos, obstacles);
}

const Path_Layer3 &PathPlanning::getPath() const
{
	return (Path_Layer3&) mPath;
}

const deque<Vector2f> &PathPlanning::getPathPoints(const float dis)
{
	mPathPoints = drawPath(mPath, 0.001f, dis);
	mPathPointsRes.clear();

	if (mPathPoints.size() >= 2)
	{
		//const float epsilon = 0.009f;
		Vector2f currentPoint = mPathPoints.front();
		mPathPointsRes.push_back(mPathPoints.front());

		for (unsigned int i = 0; i < mPathPoints.size(); i++)
			if (i > 1)
			{
				//if (abs(sqrt(pow(currentPoint.x() - mPathPoints[i].x(), 2.0f) + pow(currentPoint.y() - mPathPoints[i].y(), 2.0f)) - dis) < epsilon)
				if (v2f_distance(currentPoint, mPathPoints[i]) > dis)
				{
					currentPoint = mPathPoints[i - 1];
					mPathPointsRes.push_back(mPathPoints[i - 1]);
				}
			}
		mPathPointsRes.push_back(mPathPoints.back());
	}

	return (deque<Vector2f> &) mPathPointsRes;
}

const Path_Layer1 PathPlanning::getPathPlanning_layer1(const Vector2f startPos, const float startAngle, Vector2f endPos, const float endAngle)
{
	LinearFunc f1(startPos, startAngle);

	LinearFunc f2(endPos, endAngle);

	//cerr << startPos << "  " << gRadToDeg(startAngle) << "  " << endPos << "  " << gRadToDeg(endAngle) << endl;
	//cerr << gRadToDeg(get_angle_vector(endPos - startPos)) << endl;

	if (f1.isOnLine(endPos) || f2.isOnLine(startPos))
	{
		float ss = startPos.x();
		float ee = endPos.x();
		if (abs(startPos.x() - endPos.x()) < 0.0001f)
		{
			ss = startPos.y();
			ee = endPos.y();
		}

		float domain_d1 = ss;
		float domain_d2 = ee;

		Path_Layer1 p;
		LinearFunc lf = LinearFunc(startPos, get_angle_vector(endPos - startPos));
		lf.d = LinearDomain(domain_d1, domain_d2, 0);
		lf.dDraw = LinearDomain(domain_d1, domain_d2, 1);
		p.setLinearFunc(lf);
		p.isLinearFirst = 1;
		return p;
	}

	/*if ((!f1.isOnX() && abs(f1.getYGivenX(endPos.x()) - endPos.y()) < 0.01f) ||
			(f1.isOnX() && abs(f1.getXGivenY(endPos.y()) - endPos.x()) < 0.01f))
	{
		float ss = startPos.x();
		float ee = endPos.x();
		if (abs(startPos.x() - endPos.x()) < 0.0001f)
		{
			ss = startPos.y();
			ee = endPos.y();
		}

		float domain_d1 = ss;
		float domain_d2 = ee;

		Path_Layer1 p;
		LinearFunc lf = f1;
		lf.d = LinearDomain(domain_d1, domain_d2, 0);
		lf.dDraw = LinearDomain(domain_d1, domain_d2, 1);
		p.setLinearFunc(lf);
		p.isLinearFirst = 1;
		return p;
	}*/

	Vector2f h = getIntersectionPointTwoLines(f1, f2);
	//cerr << "x3 = " << x3 << "  y3 = " << y3 << endl;

	float dis1 = v2f_distance(startPos, h);
	float dis2 = v2f_distance(endPos, h);
	float dis_min;
	float dis_max;
	Vector2f pos_p;
	Vector2f pos_q;
	float angle_p;
	//float angle_q;

	if (dis1 < dis2)
	{
		dis_min = dis1;
		dis_max = dis2;
		pos_p = endPos;
		pos_q = startPos;
		angle_p = endAngle;
		//angle_q = startAngle;
	}
	else
	{
		dis_min = dis2;
		dis_max = dis1;
		pos_p = startPos;
		pos_q = endPos;
		angle_p = startAngle;
		//angle_q = endAngle;
	}
	//cerr << "p = " << pos_p.x() << "  " << pos_p.y() << endl;
	//cerr << "q = " << pos_q.x() << "  " << pos_q.y() << endl;
	//cerr << "angle_p = " << gRadToDeg(angle_p) << endl;
	//cerr << "angle_q = " << gRadToDeg(angle_q) << endl;

	Vector2f k1;
	k1.x() = pos_p.x() + (dis_max - dis_min) * cos(angle_p);
	k1.y() = pos_p.y() + (dis_max - dis_min) * sin(angle_p);
	Vector2f k2;
	k2.x() = pos_p.x() - (dis_max - dis_min) * cos(angle_p);
	k2.y() = pos_p.y() - (dis_max - dis_min) * sin(angle_p);
	Vector2f k;
	k = (v2f_distance(h, k1) <= v2f_distance(h, k2)) ? k1 : k2;
	//cerr << "k = " << k.x() << "  " << k.y() << endl;
	//cerr << "dis(k, h) = " << v2f_distance(k, h) << endl;
	//cerr << "dis(q, h) = " << v2f_distance(pos_q, h) << endl;

	LinearFunc f3 = LinearFunc(k, h, 1);

	LinearFunc f4 = LinearFunc(pos_q, h, 1);

	Vector2f alphabeta = getIntersectionPointTwoLines(f3, f4);
	float alpha = alphabeta.x();
	float beta = alphabeta.y();
	//cerr << "alpha = " << alpha << "  beta = " << beta << endl;

	// set flag_linear
	bool flag_linear = 1;
	if (v2f_distance(startPos, h) == v2f_distance(endPos, h))
		flag_linear = 0;

	// set flag_circular
	bool flag_circular = 1;
	if (abs(f1.a / f2.a - f1.b / f2.b) < 0.000001f && abs(f1.a / f2.a - f1.c / f2.c) < 0.000001f)
		flag_circular = 0;

	Path_Layer1 path = Path_Layer1();

	//cerr << "start = " << startPos << endl;
	//cerr << "end = " << endPos << endl;
	//cerr << "k = " << k << endl;
	//cerr << "h = " << h << endl;
	//cerr << "pos_q = " << pos_q << endl;
	//cerr << "pos_p = " << pos_p << endl;

	// LinearFunc
	if (flag_linear)
	{
		bool flag_xy = 0;
		float ss = k.x();
		float ee = pos_p.x();
		if (abs(k.x() - pos_p.x()) < 0.0001f)
		{
			ss = k.y();
			ee = pos_p.y();
			flag_xy = 1;
		}

		float domain_d1 = ss;
		float domain_d2 = ee;
		if (pos_p.x() == startPos.x() && pos_p.y() == startPos.y())
			swap(domain_d1, domain_d2);
		if (pos_p.x() != k.x() || pos_p.y() != k.y())
		{
			LinearFunc lfunc = LinearFunc(pos_p, k, 0);
			lfunc.d = LinearDomain(domain_d1, domain_d2, 0);
			lfunc.dDraw = LinearDomain(domain_d1, domain_d2, 1);
			path.setLinearFunc(lfunc);

			//cerr << "LinerFunc.a, b, c = " << lfunc.a << "  " << lfunc.b << "  " << lfunc.c << endl;
			//cerr << "LinerFunc.Domain = " << lfunc.d.startD << "  " << lfunc.d.endD << endl;

			if (!flag_xy)
			{
				if (domain_d1 == startPos.x() || domain_d2 == startPos.x())
					path.isLinearFirst = 1;
			}
			else
				if (domain_d1 == startPos.y() || domain_d2 == startPos.y())
					path.isLinearFirst = 1;
		}
	}

	// CircularFunc
	if (flag_circular)
	{
		Vector2f o = Vector2f(alpha, beta);
		float r = v2f_distance(o, pos_q);
		r = v2f_distance(o, k);
		Vector2f oq = Vector2f(pos_q.x() - o.x(), pos_q.y() - o.y());
		Vector2f ok = Vector2f(k.x() - o.x(), k.y() - o.y());
		float domain_start_angle = get_angle_vector(oq);
		float domain_end_angle   = get_angle_vector(ok);
		if (abs(pos_q.x() - endPos.x()) < 0.001f && abs(pos_q.y() - endPos.y()) < 0.001f)
			swap(domain_start_angle, domain_end_angle);
		//float domain_angle1 = atan((k.y() - o.y()) / (k.x() - o.x()));
		//float domain_angle2 = atan((pos_q.y() - o.y()) / (pos_q.x() - o.x()));

		if (abs(pos_q.x() - k.x()) >= 0.001f || abs(pos_q.y() - k.y()) >= 0.001f)
			path.setCircularFunc(CircularFunc(alpha, beta, r, CircularDomain(domain_start_angle, domain_end_angle)));
	}

	return path;
}

const Path_Layer2 PathPlanning::getPathPlanning_layer2(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle)
{
	if (isExistLayer2(startPos, startAngle, endPos, endAngle))
		return existStateLayer2(startPos, startAngle, endPos, endAngle);


	Path_Layer2 fp_res;

	//cerr << startPos << "   " << gRadToDeg(startAngle) << "   " << endPos << "   " << endAngle << endl;
	//cerr << abs(startAngle - (M_PI / 2.0f)) << endl;
	//cerr << abs(startAngle + (M_PI / 2.0f)) << endl;
	//cerr << (abs(startAngle - (M_PI / 2.0f)) < 0.01f || abs(startAngle + (M_PI / 2.0f)) < 0.01f) << endl;

	float sAngle = startAngle;
	float eAngle = endAngle;

	/*if (abs(sAngle - get_angle_vector(endPos - startPos) - M_PI) < 0.001f ||
			abs(sAngle - get_angle_vector(endPos - startPos) + M_PI) < 0.001f)
		sAngle += gDegToRad(1.0f);*/

	//if (abs(get_angle_vector(v) - sAngle - M_PI) <= 0.001 || abs(get_angle_vector(v) - sAngle + M_PI) <= 0.001f)
	//	sAngle += gDegToRad(1.0f);

	//cerr << "v_tmp = " << v_tmp.x() << "  " << v_tmp.y() << endl;
	//cerr << "shift = " << shiftVector.x() << "  " << shiftVector.y() << endl;
	//cerr << "vs_tmp = " << vs_tmp.x() << "  " << vs_tmp.y() << endl;
	//cerr << "ve_tmp = " << ve_tmp.x() << "  " << ve_tmp.y() << endl;

	LinearFunc f1(startPos, startAngle);
	LinearFunc f2(endPos, endAngle);
	//bool isOnX = 0;
	//if (abs(startPos.x() - endPos.x()) < 0.000001f)
	//	isOnX = 1;

	Vector2f v  = endPos - startPos;
	Vector2f vs = Vector2f(cos(sAngle), sin(sAngle));
	if (get_angle_between_vectors(v, vs) > M_PI_2)
	{
		Vector2f vs_tmp = Vector2f(cos(sAngle + M_PI_2), sin(sAngle + M_PI_2));
		if (get_angle_between_vectors(v, vs_tmp) <= M_PI_2)
			sAngle += M_PI_2;
		else
			sAngle -= M_PI_2;
	}
	vs = Vector2f(cos(sAngle), sin(sAngle));
	if (abs(get_angle_between_vectors(v, vs) - M_PI_2) <= 0.01)
	{
		Vector2f vs_tmp = Vector2f(cos(sAngle + gDegToRad(1.0f)), sin(sAngle + gDegToRad(1.0f)));
		if (get_angle_between_vectors(v, vs_tmp) < M_PI_2)
			sAngle += gDegToRad(1.0f);
		else
			sAngle -= gDegToRad(1.0f);
	}
	vs = Vector2f(cos(sAngle), sin(sAngle));


	Vector2f v2  = v;
	Vector2f ve = Vector2f(cos(eAngle), sin(eAngle));
	if (get_angle_between_vectors(v2, ve) > M_PI_2)
	{
		Vector2f ve_tmp = Vector2f(cos(eAngle + M_PI_2), sin(eAngle + M_PI_2));
		if (get_angle_between_vectors(v2, ve_tmp) <= M_PI_2)
			eAngle += M_PI_2;
		else
			eAngle -= M_PI_2;
	}
	ve = Vector2f(cos(eAngle), sin(eAngle));
	if (abs(get_angle_between_vectors(v2, ve) - M_PI_2) <= 0.01)
	{
		Vector2f ve_tmp = Vector2f(cos(eAngle + gDegToRad(1.0f)), sin(eAngle + gDegToRad(1.0f)));
		if (get_angle_between_vectors(v2, ve_tmp) < M_PI_2)
			eAngle += gDegToRad(1.0f);
		else
			eAngle -= gDegToRad(1.0f);
	}
	ve = Vector2f(cos(eAngle), sin(eAngle));

	//cerr << "startAngle = " << gRadToDeg(sAngle) << "  endAngle = " << gRadToDeg(eAngle) << endl;
	//cerr << "v = " << v.x() << "  " << v.y() << endl;
	//cerr << "vs = " << vs.x() << "  " << vs.y() << endl;
	//cerr << "ve = " << ve.x() << "  " << ve.y() << endl;

	/*Vector2f v_tmp  = Vector2f(cos(get_angle_vector(v)), sin(get_angle_vector(v)));
	Vector2f shiftVector = Vector2f(0.0f, 0.0f - v_tmp.y());
	Vector2f vs_tmp = Vector2f(cos(get_angle_vector(vs)), sin(get_angle_vector(vs)));
	vs_tmp += shiftVector;
	Vector2f ve_tmp = Vector2f(cos(get_angle_vector(ve)), sin(get_angle_vector(ve)));
	ve_tmp += shiftVector;*/

	/*float angle_start_positive = get_angle_positive(get_angle_vector(vs_tmp));
	float angle_end_positive   = get_angle_positive(get_angle_vector(ve_tmp));
	float angle_es_positive    = get_angle_positive(get_angle_vector(v_tmp));
	if ((((angle_start_positive <= angle_es_positive && angle_end_positive >= angle_es_positive) ||
			(angle_start_positive >= angle_es_positive && angle_end_positive <= angle_es_positive))) ||
			((vs_tmp.y() >= v_tmp.y() && ve_tmp.y() <= v_tmp.y()) || (vs_tmp.y() <= v_tmp.y() && ve_tmp.y() >= v_tmp.y())))*/

	//if ((vs_tmp.y() >= 0.0f && ve_tmp.y() <= 0.0f) || (vs_tmp.y() <= 0.0f && ve_tmp.y() >= 0.0f))
	if ((isUpperVector(vs, v) && isLowerVector(ve, v)) || (isLowerVector(vs, v) && isUpperVector(ve, v)))
	{
		//cerr << __FUNCTION__ << "  " << __LINE__ << endl;
		fp_res.append(getPathPlanning_layer1(startPos, sAngle, endPos, eAngle));
	}
	else
	{
		//cerr << __FUNCTION__ << "  " << __LINE__ << endl;
		Vector2f p = Vector2f((startPos.x() + endPos.x()) / 2, (startPos.y() + endPos.y()) / 2);

		/*float angle_p = (get_angle_vector(vs) + get_angle_vector(ve)) / 2;
		Vector2f vp_tmp = Vector2f(cos(angle_p + (M_PI / 2.0f)), sin(angle_p + (M_PI / 2.0f)));
		if (get_angle_between_vectors(v, vp_tmp) <= M_PI / 2.0f)
			angle_p += M_PI / 2.0f;
		else
			angle_p -= M_PI / 2.0f;*/

		float angle_p = get_angle_positive(get_angle_vector(v));
		//if (vs_tmp.y() >= 0.0f && ve_tmp.y() >= 0.0f)
		if (isUpperVector(vs, v) && isUpperVector(ve, v))
		{
			//cerr << "upper !!" << endl;
			angle_p += gDegToRad(10.0f);
			Vector2f finalV_tmp = Vector2f(cos(angle_p), sin(angle_p));
			//finalV_tmp += shiftVector;
			//if (finalV_tmp.y() >= 0.0f)
			if (isUpperVector(finalV_tmp, v))
				angle_p -= gDegToRad(20.0f);
		}
		//if (vs_tmp.y() <= 0.0f && ve_tmp.y() <= 0.0f)
		if (isLowerVector(vs, v) && isLowerVector(ve, v))
		{
			//cerr << "lower !!" << endl;
			angle_p += gDegToRad(10.0f);
			Vector2f finalV_tmp = Vector2f(cos(angle_p), sin(angle_p));
			//finalV_tmp += shiftVector;
			//if (finalV_tmp.y() <= 0.0f)
			if (isLowerVector(finalV_tmp, v))
				angle_p -= gDegToRad(20.0f);
		}

		//cerr << "startPos = " << startPos.x() << "  " << startPos.y() << "  " << gRadToDeg(startAngle) << endl;
		//cerr << "endPos   = " << endPos.x() << "  " << endPos.y() << "  " << gRadToDeg(endAngle) << endl;
		//cerr << "v.angle = " << gRadToDeg(get_angle_vector(v)) << endl;
		//cerr << "p = " << p.x() << "  " << p.y() << "  " << gRadToDeg(angle_p) << endl;
		//cerr << "angle_p = " << gRadToDeg(angle_p) << endl;

		Path_Layer1 p1 = getPathPlanning_layer1(startPos, sAngle, p, angle_p);
		Path_Layer1 p2 = getPathPlanning_layer1(p, angle_p, endPos, eAngle);
		fp_res.append(p1);
		fp_res.append(p2);
	}

	setStateLayer2(startPos, startAngle, endPos, endAngle, fp_res);
	return fp_res;
}

void PathPlanning::getPathPlanning_layer3(const Vector2f startPos, const float startAngle, 
		const Vector2f endPos, const float endAngle, const deque<Obstacle> obstacles, 
		deque<Vector2fDir> &pathway)
{
	//cerr << "-------------------------------------------------------" << endl;
	//cerr << "startPos = " << startPos.x() << " " << startPos.y() << "  startAngle = " << gRadToDeg(startAngle) << endl;
	//cerr << "endPos = " << endPos.x() << " " << endPos.y() << "  endAngle = " << gRadToDeg(endAngle) << endl;

	Path_Layer2 basicPath = getPathPlanning_layer2(startPos, startAngle, endPos, endAngle);
	deque<Vector2f> pathway_pos;
	deque<float> pathway_angle;

	for (unsigned int i = 0; i < basicPath.mPaths.size(); i++)
	{
		for (unsigned int j = 0; j < obstacles.size(); j++)
		{
			float x1 = obstacles[j].mPoint.x();
			float y1 = obstacles[j].mPoint.y();
			float r  = obstacles[j].r;

			// Linear
			bool isInLinear = 0;
			if (basicPath.mPaths[i].mFlagLinear)
			{
				Vector2f q = getIntersectionPointLineAndPoint(basicPath.mPaths[i].mLinearFunc, Vector2f(x1, y1));
				bool isOnX = basicPath.mPaths[i].mLinearFunc.isOnX();
				LinearDomain ld = basicPath.mPaths[i].mLinearFunc.d;
				if ((v2f_distance(q, obstacles[j].mPoint) < r - 0.000001f) && 
						((!isOnX && q.x() >= ld.startD && q.x() <= ld.endD) || (isOnX && q.y() >= ld.startD && q.y() <= ld.endD)))
				{
					//cerr << "LINEAR ----> " << endl;
					float alpha = basicPath.mPaths[i].mLinearFunc.getAlphaValue() + M_PI_2;
					Vector2f p1 = Vector2f(x1 + (r * cos(alpha)), y1 + (r * sin(alpha)));
					Vector2f p2 = Vector2f(x1 - (r * cos(alpha)), y1 - (r * sin(alpha)));
					Vector2f p  = (v2f_distance(p1, q) < v2f_distance(p2, q)) ? p1 : p2;

					//cerr << "q = " << q << endl;
					//cerr << "x1 = " << x1 << "   y1 = " << y1 << endl;
					//cerr << "alpha = " << alpha << endl;
					//cerr << "p1 = " << p1 << endl;
					//cerr << "p2 = " << p2 << endl;
					//cerr << "p = " << p << endl;

					Vector2f v  = endPos - startPos;
					Vector2f v1 = Vector2f(cos(basicPath.mPaths[i].mLinearFunc.getAlphaValue()), sin(basicPath.mPaths[i].mLinearFunc.getAlphaValue()));
					Vector2f v2 = Vector2f(-v1.x(), -v1.y());
					float v1_angle = get_angle_between_vectors(v, v1);
					float v2_angle = get_angle_between_vectors(v, v2);

					//cerr << "v = " << v << endl;
					//cerr << "v1 = " << v1 << endl;
					//cerr << "v2 = " << v2 << endl;

					float angle_p = 0;
					//if (v_dir == v1_dir)
					if (v1_angle < v2_angle)
						angle_p = get_angle_vector(v1);
					//else if (v_dir == v2_dir)
					else// if (v2_angle < v1_angle)
						angle_p = get_angle_vector(v2);
					//else
					//{
					//	cerr << "BUG: " << __FUNCTION__ << "  " << __LINE__ << endl;
					//}
					//cerr << "get_angle_vector(v1) = " << get_angle_vector(v1) << endl;
					//cerr << "get_angle_vector(v2) = " << get_angle_vector(v2) << endl;
					//cerr << "angle_p = " << angle_p << endl;

					if (!((abs(p.x() - startPos.x()) <= 0.01f && abs(p.y() - startPos.y()) <= 0.01f) ||
							(abs(p.x() - endPos.x()) <= 0.01f && abs(p.y() - endPos.y()) <= 0.01f)))
					{
						pathway_pos.push_back(p);
						pathway_angle.push_back(angle_p);
						isInLinear = 1;
						pathway.push_back(Vector2fDir(p1, angle_p));
						pathway.push_back(Vector2fDir(p2, angle_p));
					}
				}
			}

			// Circular
			if (basicPath.mPaths[i].mFlagCircular && !isInLinear)
			{
				float alpha = basicPath.mPaths[i].mCircularFunc.alpha;
				float beta  = basicPath.mPaths[i].mCircularFunc.beta;
				//cerr << "r = " << basicPath.mPaths[i].mCircularFunc.r << endl;
				Vector2f o = Vector2f(alpha, beta);

				if ((v2f_distance(obstacles[j].mPoint, o) < basicPath.mPaths[i].mCircularFunc.r + r) &&
						(v2f_distance(obstacles[j].mPoint, o) > abs(basicPath.mPaths[i].mCircularFunc.r - r)) &&
						(v2f_distance(obstacles[j].mPoint, o) > r))
				{
					//cerr << "CIRCULAR ----> " << endl;
					float angle = get_angle_vector(o - obstacles[j].mPoint);
					//cerr << "angle = " << gRadToDeg(atan(m2)) << endl;

					Vector2f p1 = Vector2f(x1 + (r * cos(angle)), y1 + (r * sin(angle)));
					Vector2f p2 = Vector2f(x1 - (r * cos(angle)), y1 - (r * sin(angle)));
					Vector2f p  = (v2f_distance(p1, o) < v2f_distance(p2, o)) ? p1 : p2;
					//cerr << "p1 = " << p1.x() << " " << p1.y() << endl;
					//cerr << "p2 = " << p2.x() << " " << p2.y() << endl;
					//cerr << "p = " << p.x() << " " << p.y() << endl;

					Vector2f op = p - o;
					float sa = basicPath.mPaths[i].mCircularFunc.d.startAngle;
					Vector2f os = Vector2f(cos(sa), sin(sa));
					float ea = basicPath.mPaths[i].mCircularFunc.d.endAngle;
					Vector2f oe = Vector2f(cos(ea), sin(ea));
					if (abs(get_angle_between_vectors(os, oe) - get_angle_between_vectors(os, op) - get_angle_between_vectors(oe, op)) <= 0.01)
					{
						float angle2 = get_angle_vector(o - obstacles[j].mPoint) + M_PI_2;
						Vector2f v   = endPos - startPos;
						Vector2f v1 = Vector2f(cos(angle2), sin(angle2));
						Vector2f v2 = Vector2f(-v1.x(), -v1.y());

						float v1_angle = get_angle_between_vectors(v, v1);
						float v2_angle = get_angle_between_vectors(v, v2);

						float angle_p = 0;
						if (v1_angle < v2_angle)
							angle_p = get_angle_vector(v1);
						else
							angle_p = get_angle_vector(v2);

						if (!((abs(p.x() - startPos.x()) <= 0.01f && abs(p.y() - startPos.y()) <= 0.01f) ||
								(abs(p.x() - endPos.x()) <= 0.01f && abs(p.y() - endPos.y()) <= 0.01f)))
						{
							pathway_pos.push_back(p);
							pathway_angle.push_back(angle_p);
							pathway.push_back(Vector2fDir(p1, angle_p));
							pathway.push_back(Vector2fDir(p2, angle_p));
						}
					}
				}
			}
		}
	}

	unsigned int i = 0;
	while (i < pathway_pos.size()) 
	{
		bool flg = 0;
		for (unsigned int inObs = 0; inObs < obstacles.size(); inObs ++)
			if (v2f_distance(pathway_pos[i], obstacles[inObs].mPoint) < obstacles[inObs].r)
			{
				flg = 1;
				pathway_pos.erase(pathway_pos.begin() + i);
				pathway_angle.erase(pathway_angle.begin() + i);
				break;
			}
		if (!flg)
			i++;
	}

	if (pathway_pos.size())
	{
		pathway_pos.push_front(startPos);
		pathway_pos.push_back(endPos);
		pathway_angle.push_front(startAngle);
		pathway_angle.push_back(endAngle);

		//cerr << "**********************************" << endl;
		//for (unsigned int i = 0; i < pathway_pos.size(); i++)
		//{
			//cerr << pathway_pos[i].x() << "  " << pathway_pos[i].y() << endl;
			//cerr << gRadToDeg(pathway_angle[i]) << endl;
		//}
		//cerr << "**********************************" << endl;

		for (unsigned int i = 0; i < pathway_pos.size() - 1; i++)
		{
			//res.append(getPathPlanning_layer2(pathway_pos[i], pathway_angle[i], pathway_pos[i + 1], pathway_angle[i + 1]));
			//cerr << "STARTTTT = " << pathway_pos[i].x() << "  " << pathway_pos[i].y() << endl;
			//cerr << "ENDDDDDD = " << pathway_pos[i + 1].x() << "  " << pathway_pos[i + 1].y() << endl;
			if (!layer3_savedState(pathway_pos[i], pathway_pos[i + 1]))
				getPathPlanning_layer3(pathway_pos[i], pathway_angle[i], pathway_pos[i + 1], pathway_angle[i + 1], obstacles, pathway);
		}
	}

	//cerr << "pathway.size() = " << pathway_pos.size() << endl;
	return ;
}

const Path_Layer3 PathPlanning::getPathPlanning_layer4(const Vector2f startPos, const float startAngle, 
		const Vector2f endPos, const float endAngle, deque<Obstacle> obstacles)
{
	deque<Vector2fDir> pathway;
	getPathPlanning_layer3(startPos, startAngle, endPos, endAngle, obstacles, pathway);
	//while (pathway.size() > 2 * obstacles.size())
	//	pathway.pop_back();

	//sort
	if (pathway.size() > 3)
		for (unsigned int i = 0; i < pathway.size() - 3; i++)
			for (unsigned int j = 0; j < pathway.size() - 3; j++)
				if (startPos.x() < endPos.x())
				{
					float average1 = (pathway[j].mPoint.x() + pathway[j + 1].mPoint.x()) / 2;
					float average2 = (pathway[j + 2].mPoint.x() + pathway[j + 3].mPoint.x()) / 2;
					if (average1 > average2)
					{
						swap(pathway[j], pathway[j + 2]);
						swap(pathway[j + 1], pathway[j + 3]);
					}
				}
				else
				{
					float average1 = (pathway[j].mPoint.x() + pathway[j + 1].mPoint.x()) / 2;
					float average2 = (pathway[j + 2].mPoint.x() + pathway[j + 3].mPoint.x()) / 2;
					if (average1 < average2)
					{
						swap(pathway[j], pathway[j + 2]);
						swap(pathway[j + 1], pathway[j + 3]);
					}
				}
	pathway.push_front(Vector2fDir(startPos, startAngle));
	pathway.push_back(Vector2fDir(endPos, endAngle));

	//cerr << "pathway.size = " << pathway.size() << endl;
	//for (unsigned int i = 0; i < pathway.size(); i++)
		//cerr << pathway[i].mPoint.x() << "  " << pathway[i].mPoint.y() << endl;

	unsigned int n = pathway.size();
	unsigned int src = 0;
	unsigned int dst = pathway.size() - 1;
	float a[100][100];

	for (unsigned int i = 0; i < 100; i++)
		for (unsigned int j = 0; j < 100; j++)
			a[j][i] = 0.0f;

	//cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;
	// creating graph
	for (unsigned int k = 0; k < n; k++)
		for (unsigned int h = k + 1 + (k % 2); h < n; h++)
		{
			Path_Layer2 basicPath = getPathPlanning_layer2(pathway[k].mPoint, pathway[k].angle, 
					pathway[h].mPoint, pathway[h].angle);

			bool terminated = 0;
			/*float sx = gMin(pathway[k].mPoint.x(), pathway[h].mPoint.x());
			float ex = gMax(pathway[k].mPoint.x(), pathway[h].mPoint.x());
			float sy = gMin(pathway[k].mPoint.y(), pathway[h].mPoint.y());
			float ey = gMax(pathway[k].mPoint.y(), pathway[h].mPoint.y());*/
			for (unsigned int i = 0; i < basicPath.mPaths.size(); i++)
			{
				for (unsigned int j = 0; j < obstacles.size(); j++)
				/*if ((obstacles[j].mPoint.x() > sx - obstacles[j].r) &&
						(obstacles[j].mPoint.x() < ex + obstacles[j].r) &&
						(obstacles[j].mPoint.y() > sy - obstacles[j].r) &&
						(obstacles[j].mPoint.y() < ey + obstacles[j].r))*/
				{
					float x1 = obstacles[j].mPoint.x();
					float y1 = obstacles[j].mPoint.y();
					float r  = obstacles[j].r;

					// Linear
					if (basicPath.mPaths[i].mFlagLinear)
					{
						Vector2f q = getIntersectionPointLineAndPoint(basicPath.mPaths[i].mLinearFunc, Vector2f(x1, y1));
						bool isOnX = basicPath.mPaths[i].mLinearFunc.isOnX();
						LinearDomain ld = basicPath.mPaths[i].mLinearFunc.d;
						if ((v2f_distance(q, obstacles[j].mPoint) < r - 0.000001f) && 
								((!isOnX && q.x() >= ld.startD && q.x() <= ld.endD) || (isOnX && q.y() >= ld.startD && q.y() <= ld.endD)))
						{
							terminated = 1;
							break;
						}
					}

					// Circular
					if (basicPath.mPaths[i].mFlagCircular)
					{
						float alpha = basicPath.mPaths[i].mCircularFunc.alpha;
						float beta  = basicPath.mPaths[i].mCircularFunc.beta;
						//cerr << "r = " << basicPath.mPaths[i].mCircularFunc.r << endl;
						Vector2f o = Vector2f(alpha, beta);

						if ((v2f_distance(obstacles[j].mPoint, o) < basicPath.mPaths[i].mCircularFunc.r + r) &&
								(v2f_distance(obstacles[j].mPoint, o) > abs(basicPath.mPaths[i].mCircularFunc.r - r)) &&
								(v2f_distance(obstacles[j].mPoint, o) > r))
						{
							//cerr << "CIRCULAR ----> " << endl;
							float angle = get_angle_vector(o - obstacles[j].mPoint);

							Vector2f p1 = Vector2f(x1 + (r * cos(angle)), y1 + (r * sin(angle)));
							Vector2f p2 = Vector2f(x1 - (r * cos(angle)), y1 - (r * sin(angle)));
							//Vector2f p  = (v2f_distance(p1, o) < v2f_distance(p2, o)) ? p1 : p2;
							//cerr << "p1 = " << p1.x() << " " << p1.y() << endl;
							//cerr << "p2 = " << p2.x() << " " << p2.y() << endl;
							//cerr << "p = " << p.x() << " " << p.y() << endl;

							Vector2f p = p1;
							Vector2f op = p - o;
							float sa = basicPath.mPaths[i].mCircularFunc.d.startAngle;
							Vector2f os = Vector2f(cos(sa), sin(sa));
							float ea = basicPath.mPaths[i].mCircularFunc.d.endAngle;
							Vector2f oe = Vector2f(cos(ea), sin(ea));

							if (get_angle_between_vectors(os, oe) - get_angle_between_vectors(os, op) - get_angle_between_vectors(oe, op) >= -0.001f)
								if (get_angle_between_vectors(os, op) >= 0.000001f && get_angle_between_vectors(oe, op) >= 0.000001f)
								{
									terminated = 1;
									break;
								}

							p = p2;
							op = p - o;
							sa = basicPath.mPaths[i].mCircularFunc.d.startAngle;
							os = Vector2f(cos(sa), sin(sa));
							ea = basicPath.mPaths[i].mCircularFunc.d.endAngle;
							oe = Vector2f(cos(ea), sin(ea));
							if (get_angle_between_vectors(os, oe) - get_angle_between_vectors(os, op) - get_angle_between_vectors(oe, op) >= -0.001f)
								if (get_angle_between_vectors(os, op) >= 0.000001f && get_angle_between_vectors(oe, op) >= 0.000001f)
								{
									terminated = 1;
									break;
								}
						}
					}
				}
				if (terminated)
					break;
			}

			if (!terminated)
				a[h][k] = basicPath.get_distance_path();// + (1.0f / ((float) (h - k)));
		}

	/*for (unsigned int i = 0; i < n; i++)
	{
		for (unsigned int j = 0; j < n; j++)
			cerr << a[j][i] << " ";
		cerr << endl;
	}*/

	//cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;
	deque<unsigned int> path_point = dijkstra(n, src, dst, a);

	//cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;
	Path_Layer3 path;
	if (path_point.size() > 0)
		for (unsigned int i = 0; i < path_point.size() - 1; i++)
		{
			unsigned int sp = path_point[i];
			unsigned int ep = path_point[i + 1];
			path.append(getPathPlanning_layer2(pathway[sp].mPoint, pathway[sp].angle,pathway[ep].mPoint, pathway[ep].angle));
		}
	//cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;

	return path;
}

const Vector2f PathPlanning::getPointOutOfObstacle(Vector2f pos, deque<Obstacle> obstacles)
{
	for (unsigned int i = 0; i < obstacles.size(); i++)
		if (v2f_distance(pos, obstacles[i].mPoint) <= obstacles[i].r)
		{
			float angle = get_angle_vector(pos - obstacles[i].mPoint);
			Vector2f p1 = pos + (Vector2f(cos(angle), sin(angle)) * obstacles[i].r);
			Vector2f p2 = pos - (Vector2f(cos(angle), sin(angle)) * obstacles[i].r);
			Vector2f p  = (v2f_distance(p1, obstacles[i].mPoint) > v2f_distance(p2, obstacles[i].mPoint)) ? p1 : p2;
			return getPointOutOfObstacle(p, obstacles);	
		}
	return pos;
}

const Path_Layer3 PathPlanning::getPathPlanning_basic(const Vector2f startPos, const float startAngle, 
		const Vector2f endPos, const float endAngle, deque<Obstacle> obstacles)
{
	layer3_savedStates.clear();

	unsigned int i = 0;
	while (i < obstacles.size())
	{
		if (v2f_distance(endPos, obstacles[i].mPoint) <= obstacles[i].r || 
				v2f_distance(startPos, obstacles[i].mPoint) <= obstacles[i].r)
			obstacles.erase(obstacles.begin() + i);
		else
			i++;
	}

	/*for (unsigned int i = 0; i < obstacles.size(); i++)
		if (v2f_distance(startPos, obstacles[i].mPoint) <= obstacles[i].r)
		{
			Vector2f goalPoint = getPointOutOfObstacle(startPos, obstacles);
			deque<Obstacle> tmp;
			return getPathPlanning_layer4(startPos, startAngle, goalPoint, get_angle_vector(goalPoint - startPos), tmp);
		}*/

	//sort
	if (obstacles.size())
		for (unsigned int i = 0; i < obstacles.size() - 1; i++)
			for (unsigned int j = 0; j < obstacles.size() - 1; j++)
				if (startPos.x() < endPos.x())
				{
					if (obstacles[j].mPoint.x() > obstacles[j + 1].mPoint.x())
						swap(obstacles[j], obstacles[j + 1]);
				}
				else
				{
					if (obstacles[j].mPoint.x() < obstacles[j + 1].mPoint.x())
						swap(obstacles[j], obstacles[j + 1]);
				}

	return getPathPlanning_layer4(startPos, startAngle, endPos, endAngle, obstacles);
}

const Path_Layer3 PathPlanning::getPathPlanning(const Vector2f startPos, const Vector2f endPos, deque<Obstacle> obstacles)
{
	return getPathPlanning_basic(startPos, get_angle_vector(endPos - startPos), endPos, get_angle_vector(endPos - startPos), obstacles);

	//// BETA

	//sort
	if (obstacles.size())
		for (unsigned int i = 0; i < obstacles.size() - 1; i++)
			for (unsigned int j = 0; j < obstacles.size() - 1; j++)
				if (startPos.x() < endPos.x())
				{
					if (obstacles[j].mPoint.x() > obstacles[j + 1].mPoint.x())
						swap(obstacles[j], obstacles[j + 1]);
				}
				else
				{
					if (obstacles[j].mPoint.x() < obstacles[j + 1].mPoint.x())
						swap(obstacles[j], obstacles[j + 1]);
				}

	float bestStartAngle;
	float bestEndAngle;
	float minDis = 1 >> 30;
	int bestPathValue = 1 >> 30;

	cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;

	for (float sa = -180.0f; sa <= 180.0f; sa += 10.0f)
		for (float ea = -180.0f; ea <= 180.0f; ea += 10.0f)
		{
			/*pathway.clear();
			getPathPlanning_layer3(startPos, gDegToRad(sa), endPos, gDegToRad(ea), obstacles, pathway);
			if (pathway.size() < bestPathValue)
			{
				bestPathValue  = pathway.size();
				bestStartAngle = gDegToRad(sa);
				bestEndAngle   = gDegToRad(ea);
			}*/

			Path_Layer2 basicPath = getPathPlanning_layer2(startPos, gDegToRad(sa), endPos, gDegToRad(ea));
			int counter = 0;

			for (unsigned int i = 0; i < basicPath.mPaths.size(); i++)
			{
				for (unsigned int j = 0; j < obstacles.size(); j++)
				{
					float x1 = obstacles[j].mPoint.x();
					float y1 = obstacles[j].mPoint.y();
					float r  = obstacles[j].r;

					// Linear
					bool isInLinear = 0;
					if (basicPath.mPaths[i].mFlagLinear)
					{
						Vector2f q = getIntersectionPointLineAndPoint(basicPath.mPaths[i].mLinearFunc, Vector2f(x1, y1));
						bool isOnX = basicPath.mPaths[i].mLinearFunc.isOnX();
						LinearDomain ld = basicPath.mPaths[i].mLinearFunc.d;
						if ((v2f_distance(q, obstacles[j].mPoint) < r - 0.000001f) && 
								((!isOnX && q.x() >= ld.startD && q.x() <= ld.endD) || (isOnX && q.y() >= ld.startD && q.y() <= ld.endD)))
						{
							counter ++;
						}
					}

					// Circular
					if (basicPath.mPaths[i].mFlagCircular && !isInLinear)
					{
						float alpha = basicPath.mPaths[i].mCircularFunc.alpha;
						float beta  = basicPath.mPaths[i].mCircularFunc.beta;
						Vector2f o = Vector2f(alpha, beta);

						if ((v2f_distance(obstacles[j].mPoint, o) < basicPath.mPaths[i].mCircularFunc.r + r) &&
								(v2f_distance(obstacles[j].mPoint, o) > abs(basicPath.mPaths[i].mCircularFunc.r - r)) &&
								(v2f_distance(obstacles[j].mPoint, o) > r))
						{
							float angle = get_angle_vector(o - obstacles[j].mPoint);

							Vector2f p1 = Vector2f(x1 + (r * cos(angle)), y1 + (r * sin(angle)));
							Vector2f p2 = Vector2f(x1 - (r * cos(angle)), y1 - (r * sin(angle)));
							Vector2f p  = (v2f_distance(p1, o) < v2f_distance(p2, o)) ? p1 : p2;
							Vector2f op = p - o;
							float sa = basicPath.mPaths[i].mCircularFunc.d.startAngle;
							Vector2f os = Vector2f(cos(sa), sin(sa));
							float ea = basicPath.mPaths[i].mCircularFunc.d.endAngle;
							Vector2f oe = Vector2f(cos(ea), sin(ea));
							if (abs(get_angle_between_vectors(os, oe) - get_angle_between_vectors(os, op) - get_angle_between_vectors(oe, op)) <= 0.01)
							{
								counter ++;
							}
						}
					}
				}
			}

			if (counter <= bestPathValue && basicPath.get_distance_path() < minDis)
			{
				minDis = basicPath.get_distance_path();
				bestPathValue  = counter;
				bestStartAngle = gDegToRad(sa);
				bestEndAngle   = gDegToRad(ea);
			}
		}

	cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;

	Path_Layer3 res = getPathPlanning_basic(startPos, bestStartAngle, endPos, bestEndAngle, obstacles);

	cerr << ((float)clock() / (float)CLOCKS_PER_SEC) * 1000.0f << endl;

	return res;
}

const deque<Vector2f> PathPlanning::draw_path_layer1(const Path_Layer1 path, const float minDis, const float dis)
{
	deque<Vector2f> res;

	// simple
	/*if (path.mFlagLinear)
	{
		for (float x = path.mLinearFunc.d.startX; x <= path.mLinearFunc.d.endX; x += 0.001f)
		{
			Vector2f tmp = Vector2f(x, path.mLinearFunc.getYGivenX(x));
			res.push_back(tmp);
		}
	}

	if (path.mFlagCircular)
	{
		float sa = get_angle_positive(path.mCircularFunc.d.startAngle);
		float ea = get_angle_positive(path.mCircularFunc.d.endAngle);
		float averageAngle = (sa + ea) / 2.0f;
		Vector2f v = Vector2f(cos(averageAngle), sin(averageAngle));
		Vector2f v_inverse = Vector2f(-v.x(), -v.y());
		Vector2f vs = Vector2f(cos(sa), sin(sa));
		Vector2f averageVector;
		float offsetAngle;
		if (get_angle_between_vectors(v, vs) < get_angle_between_vectors(v_inverse, vs))
		{
			averageVector = v;
			offsetAngle = get_angle_between_vectors(v, vs);
		}
		else
		{
			averageVector = v_inverse;
			
			offsetAngle = get_angle_between_vectors(v_inverse, vs);
		}

		float lastAngle = get_angle_vector(averageVector) + (2.0f * M_PI);

		for (float a = lastAngle - offsetAngle; a <= lastAngle + offsetAngle; a += 0.01f)
		{
			float x = path.mCircularFunc.alpha + (path.mCircularFunc.r * cos(a));
			float y = path.mCircularFunc.beta  + (path.mCircularFunc.r * sin(a));
			Vector2f tmp = Vector2f(x, y);
			res.push_back(tmp);
		}
	}*/

	// set minDis
	float minDisLine = minDis;
	/*while (1)
	{
		float shift = abs(minDisLine * cos(path.mLinearFunc.m));
		float x = path.mLinearFunc.dDraw.startX;
		Vector2f tmp1 = Vector2f(x, path.mLinearFunc.getYGivenX(x));
		x += shift;
		Vector2f tmp2 = Vector2f(x, path.mLinearFunc.getYGivenX(x));

		if (v2f_distance(tmp1, tmp2) > dis)
			minDisLine /= 2.0f;
		else
			break;
	}*/

	if (path.isLinearFirst)
		if (path.mFlagLinear)
		{
			float shift = abs(minDisLine * cos(path.mLinearFunc.getSlope()));
			if (path.mLinearFunc.isOnX())
				shift = minDisLine;
			//cerr << "shift = " << shift << endl;
			if (path.mLinearFunc.dDraw.startD < path.mLinearFunc.dDraw.endD)
				for (float p = path.mLinearFunc.dDraw.startD; p <= path.mLinearFunc.dDraw.endD; p += shift)
				{
					Vector2f tmp = Vector2f(p, path.mLinearFunc.getYGivenX(p));
					if (path.mLinearFunc.isOnX())
						tmp = Vector2f(path.mLinearFunc.getXGivenY(p), p);
					res.push_back(tmp);
				}
			else
				for (float p = path.mLinearFunc.dDraw.startD; p >= path.mLinearFunc.dDraw.endD; p -= shift)
				{
					Vector2f tmp = Vector2f(p, path.mLinearFunc.getYGivenX(p));
					if (path.mLinearFunc.isOnX())
						tmp = Vector2f(path.mLinearFunc.getXGivenY(p), p);
					res.push_back(tmp);
				}
		}

	if (path.mFlagCircular)
	{
		float sa = get_angle_positive(path.mCircularFunc.d.startAngle);
		float ea = get_angle_positive(path.mCircularFunc.d.endAngle);
		float averageAngle = (sa + ea) / 2.0f;
		Vector2f v = Vector2f(cos(averageAngle), sin(averageAngle));
		Vector2f v_inverse = Vector2f(-v.x(), -v.y());
		Vector2f vs = Vector2f(cos(sa), sin(sa));
		Vector2f averageVector;
		float offsetAngle;
		if (get_angle_between_vectors(v, vs) < get_angle_between_vectors(v_inverse, vs))
		{
			averageVector = v;
			offsetAngle = get_angle_between_vectors(v, vs);
		}
		else
		{
			averageVector = v_inverse;
			
			offsetAngle = get_angle_between_vectors(v_inverse, vs);
		}

		float lastAngle = get_angle_vector(averageVector) + (2.0f * M_PI);

		float shift = abs(minDis / path.mCircularFunc.r);
		if (abs(get_angle_standard(lastAngle - offsetAngle) - path.mCircularFunc.d.startAngle) <= 0.01f ||
				abs(get_angle_standard(lastAngle - offsetAngle) - path.mCircularFunc.d.startAngle - (2.0f * M_PI)) <= 0.01f ||
				abs(get_angle_standard(lastAngle - offsetAngle) - path.mCircularFunc.d.startAngle + (2.0f * M_PI)) <= 0.01f)
		{
			for (float a = lastAngle - offsetAngle; a <= lastAngle + offsetAngle; a += shift)
			{
				float x = path.mCircularFunc.alpha + (path.mCircularFunc.r * cos(a));
				float y = path.mCircularFunc.beta  + (path.mCircularFunc.r * sin(a));
				Vector2f tmp = Vector2f(x, y);
				res.push_back(tmp);
			}
		}
		else if (abs(get_angle_standard(lastAngle + offsetAngle) - path.mCircularFunc.d.startAngle) <= 0.01f ||
				abs(get_angle_standard(lastAngle + offsetAngle) - path.mCircularFunc.d.startAngle - (2.0f * M_PI)) <= 0.02f ||
				abs(get_angle_standard(lastAngle + offsetAngle) - path.mCircularFunc.d.startAngle + (2.0f * M_PI)) <= 0.02f)
		{
			for (float a = lastAngle + offsetAngle; a >= lastAngle - offsetAngle; a -= shift)
			{
				float x = path.mCircularFunc.alpha + (path.mCircularFunc.r * cos(a));
				float y = path.mCircularFunc.beta  + (path.mCircularFunc.r * sin(a));
				Vector2f tmp = Vector2f(x, y);
				res.push_back(tmp);
			}
		}
	}

	if (!path.isLinearFirst)
		if (path.mFlagLinear)
		{
			float shift = abs(minDisLine * cos(path.mLinearFunc.getSlope()));
			if (path.mLinearFunc.isOnX())
				shift = minDisLine;
			if (path.mLinearFunc.dDraw.startD < path.mLinearFunc.dDraw.endD)
				for (float p = path.mLinearFunc.dDraw.startD; p <= path.mLinearFunc.dDraw.endD; p += shift)
				{
					Vector2f tmp = Vector2f(p, path.mLinearFunc.getYGivenX(p));
					if (path.mLinearFunc.isOnX())
						tmp = Vector2f(path.mLinearFunc.getXGivenY(p), p);
					res.push_back(tmp);
				}
			else
				for (float p = path.mLinearFunc.dDraw.startD; p >= path.mLinearFunc.dDraw.endD; p -= shift)
				{
					Vector2f tmp = Vector2f(p, path.mLinearFunc.getYGivenX(p));
					if (path.mLinearFunc.isOnX())
						tmp = Vector2f(path.mLinearFunc.getXGivenY(p), p);
					res.push_back(tmp);
				}
		}

	return res;
}

const deque<Vector2f> PathPlanning::drawPath(const Path_Layer3 path, const float minDis, const float dis)
{
	deque<Vector2f> res;

	for (unsigned int j = 0; j < path.mPaths.size(); j++)
	{
		Path_Layer2 fp = path.mPaths[j];
		for (unsigned int i = 0; i < fp.mPaths.size(); i++)
		{
			deque<Vector2f> path_draw = draw_path_layer1(fp.mPaths[i], minDis, dis);
			for (unsigned int i = 0; i < path_draw.size(); i++)
				res.push_back(path_draw[i]);
		}
	}

	return res;
}

bool PathPlanning::layer3_savedState(Vector2f v1, Vector2f v2)
{
	stringstream ss;
	ss << v1.x() << " " << v1.y() << " " << v2.x() << " " << v2.y() << endl;
	if (layer3_savedStates[ss.str()] == 0)
	{
		layer3_savedStates[ss.str()] = 1;
		return 0;
	}
	return 1;
}

bool PathPlanning::isExistLayer2(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle)
{
	stringstream ss;
	ss << startPos.x() << " " << startPos.y() << " " << startAngle << " " << endPos.x() << " " << endPos.y() << " " << endAngle << endl;
	return layer2_isExsist[ss.str()];
}

Path_Layer2 PathPlanning::existStateLayer2(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle)
{
	stringstream ss;
	ss << startPos.x() << " " << startPos.y() << " " << startAngle << " " << endPos.x() << " " << endPos.y() << " " << endAngle << endl;
	return layer2_state[ss.str()];
}

void PathPlanning::setStateLayer2(const Vector2f startPos, const float startAngle, const Vector2f endPos, const float endAngle, Path_Layer2 pl)
{
	stringstream ss;
	ss << startPos.x() << " " << startPos.y() << " " << startAngle << " " << endPos.x() << " " << endPos.y() << " " << endAngle << endl;
	layer2_isExsist[ss.str()] = 1;
	layer2_state[ss.str()] = pl;
}
