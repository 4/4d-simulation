#ifndef __LINEAR_DOMAIN_H_
#define __LINEAR_DOMAIN_H_

struct LinearDomain {
	float startD, endD;

	LinearDomain(): startD(0), endD(0) {}
	LinearDomain(const float d1, const float d2, const bool isForDraw);
};

#endif // __LINEAR_DOMAIN_H_
