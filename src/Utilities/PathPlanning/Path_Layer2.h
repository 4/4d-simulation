#ifndef __PATH_LAYER2_H_
#define __PATH_LAYER2_H_

#include <deque>
#include "Path_Layer1.h"

struct Path_Layer2 {
	std::deque<Path_Layer1> mPaths;

	Path_Layer2() {}

	void append(const Path_Layer1 p);
	void append(const Path_Layer2 p);

	const float get_distance_path() const;
};

#endif // __PATH_LAYER2_H_
