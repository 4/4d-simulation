#ifndef __CIRCULAR_DOMAIN_H_
#define __CIRCULAR_DOMAIN_H_

struct CircularDomain {
	float startAngle, endAngle;

	CircularDomain(): startAngle(0), endAngle(0) {}
	CircularDomain(const float a1, const float a2);
};

#endif // __CIRCULAR_DOMAIN_H_
