#ifndef __VECTOR2DIR_H_
#define __VECTOR2DIR_H_

#include <Vector2.h>

struct Vector2fDir {
	MIKE::Vector2f mPoint;
	float angle;

	Vector2fDir(const MIKE::Vector2f v, const float a): mPoint(v), angle(a) {}
};

#endif // __VECTOR2DIR_H_
