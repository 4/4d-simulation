#ifndef __PATH_LAYER3_H_
#define __PATH_LAYER3_H_

#include <deque>
#include "Path_Layer2.h"

struct Path_Layer3 {
	std::deque<Path_Layer2> mPaths;

	Path_Layer3() {}

	void append(const Path_Layer2 p);

	void append(const Path_Layer3 p);

	const float get_distance_path() const;
};

#endif // __PATH_LAYER3_H_
