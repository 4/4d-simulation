#ifndef __LINEAR_FUNC_H_
#define __LINEAR_FUNC_H_

#include <Vector2.h>
#include <Vector3.h>
#include "LinearDomain.h"

struct LinearFunc {
	float a, b, c;
	LinearDomain d;
	LinearDomain dDraw;

	LinearFunc(): a(0), b(0), c(0) {}
	LinearFunc(const float a1, const float b1, const float c1): a(a1), b(b1), c(b1) {}
	LinearFunc(const MIKE::Vector2f startPos, const float angle);
	LinearFunc(const MIKE::Vector3f startPos, const float angle);
	LinearFunc(const MIKE::Vector2f p1, const MIKE::Vector2f p2, const bool isInverse = 0); // on p1
	LinearFunc(const MIKE::Vector3f p1, const MIKE::Vector3f p2, const bool isInverse = 0); // on p1
	LinearFunc(const float a1, const float b1, const float c1, const LinearDomain d1, const LinearDomain dd1): 
			a(a1), b(b1), c(c1), d(d1), dDraw(dd1) {}

	const float getYGivenX(const float x) const;
	const float getXGivenY(const float y) const;

	const float get_distance_path() const;

	const bool isOnX() const;
	const float getAlphaValue() const;
	const float getSlope() const;
	const float getIntercept() const;
	const float getInterceptY() const;
	const float getInterceptX() const;
	const bool isInDomain(const MIKE::Vector2f p) const;
	const bool isInDomain(const MIKE::Vector3f p) const;
	const MIKE::Vector2f findNearestPointInDomain(const MIKE::Vector2f p) const;
	const MIKE::Vector3f findNearestPointInDomain(const MIKE::Vector3f p) const;
	const MIKE::Vector2f getStartPos() const;
	const MIKE::Vector2f getEndPos() const;

	const bool isOnLine(MIKE::Vector2f p) const;
};

#endif // __LINEAR_FUNC_H_
