#include "Path_Layer1.h"

#include "PPMath.h"
#include <Vector2.h>

using namespace MIKE;
using namespace pp_math;

Path_Layer1::Path_Layer1()
{
	isLinearFirst = 0;
	mFlagLinear = 0;
	mFlagCircular = 0;
}

void Path_Layer1::setLinearFunc(const LinearFunc lf)
{
	mFlagLinear = 1;
	mLinearFunc = lf;
}

void Path_Layer1::setCircularFunc(const CircularFunc cf)
{
	mFlagCircular = 1;
	mCircularFunc = cf;
}

const float Path_Layer1::get_distance_path() const
{
	float res = 0.0f;

	if (mFlagLinear)
		res += mLinearFunc.get_distance_path();

	if (mFlagCircular)
	{
		Vector2f v1 = Vector2f(cos(mCircularFunc.d.startAngle), sin(mCircularFunc.d.startAngle));
		Vector2f v2 = Vector2f(cos(mCircularFunc.d.endAngle), sin(mCircularFunc.d.endAngle));
		float theta = get_angle_between_vectors(v1, v2);
		res += mCircularFunc.r * theta;
	}

	return res;
}
