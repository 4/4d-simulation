#include "LinearFunc.h"

#include "PPMath.h"

using namespace pp_math;
using namespace MIKE;
using namespace std;

LinearFunc::LinearFunc(const Vector3f startPos, const float angle)
{
	Vector2f p(startPos.x(), startPos.y());
	float m = tan(angle);
	float n = -(m * p.x()) + p.y();

	a = -m;
	b = 1.0f;
	c = -n;

	if ((abs(get_angle_positive(get_angle_positive(angle) - get_angle_positive(M_PI_2))) < 0.001f) ||
			(abs(get_angle_positive(get_angle_positive(angle) - get_angle_positive(3.0f * M_PI_2))) < 0.001f))
	{
		a = 1.0f;
		b = 0.0f;
		c = -p.x();
	}
}

LinearFunc::LinearFunc(const Vector2f startPos, const float angle)
{
	float m = tan(angle);
	float n = -(m * startPos.x()) + startPos.y();

	a = -m;
	b = 1.0f;
	c = -n;

	if ((abs(get_angle_positive(get_angle_positive(angle) - get_angle_positive(M_PI_2))) < 0.001f) ||
			(abs(get_angle_positive(get_angle_positive(angle) - get_angle_positive(3.0f * M_PI_2))) < 0.001f))
	{
		a = 1.0f;
		b = 0.0f;
		c = -startPos.x();
	}
}

LinearFunc::LinearFunc(const Vector3f p1, const Vector3f p2, const bool isInverse)
{
	Vector2f p11(p1.x(), p1.y());
	Vector2f p22(p2.x(), p2.y());

	if (!isInverse)
	{
		float m = get_slope(p11, p22);
		float n = -(m * p11.x()) + p11.y();

		a = -m;
		b = 1.0f;
		c = -n;

		if (abs(p11.x() - p22.x()) < 0.000001f)
		{
			a = 1.0f;
			b = 0.0f;
			c = -p11.x();
		}
	}
	else
	{
		float m = get_slope_inverse(p11, p22);
		float n = -(m * p11.x()) + p11.y();

		a = -m;
		b = 1.0f;
		c = -n;

		if (abs(p11.y() - p22.y()) < 0.000001f)
		{
			a = 1.0f;
			b = 0.0f;
			c = -p11.x();
		}
	}

	d = LinearDomain(p11.x(), p22.x(), 0);
	if (abs(p11.x() - p22.x()) < 0.000001f)
		d = LinearDomain(p11.y(), p22.y(), 0);
}

LinearFunc::LinearFunc(const Vector2f p1, const Vector2f p2, const bool isInverse)
{
	if (!isInverse)
	{
		float m = get_slope(p1, p2);
		float n = -(m * p1.x()) + p1.y();

		a = -m;
		b = 1.0f;
		c = -n;

		if (abs(p1.x() - p2.x()) < 0.000001f)
		{
			a = 1.0f;
			b = 0.0f;
			c = -p1.x();
		}
	}
	else
	{
		float m = get_slope_inverse(p1, p2);
		float n = -(m * p1.x()) + p1.y();

		a = -m;
		b = 1.0f;
		c = -n;

		if (abs(p1.y() - p2.y()) < 0.000001f)
		{
			a = 1.0f;
			b = 0.0f;
			c = -p1.x();
		}
	}

	d = LinearDomain(p1.x(), p2.x(), 0);
	if (abs(p1.x() - p2.x()) < 0.000001f)
		d = LinearDomain(p1.y(), p2.y(), 0);
}

const float LinearFunc::getYGivenX(const float x) const
{
	return (-a / b) * x + (-c / b);
}

const float LinearFunc::getXGivenY(const float y) const
{
	return (-b / a) * y + (-c / a);
}

const float LinearFunc::get_distance_path() const
{
	Vector2f v1, v2;
	float m = -a / b;
	float n = -c / b;
	v1 = Vector2f(d.startD, m * d.startD + n);
	v2 = Vector2f(d.endD, m * d.endD + n);

	if (abs(b) < 0.000001f)
	{
		v1 = Vector2f(-c / a, d.startD);
		v2 = Vector2f(-c / a, d.endD);
	}

	return v2f_distance(v1, v2);
}

const bool LinearFunc::isOnX() const
{
	if (abs(b) < 0.000001f)
		return 1;
	return 0;
}

const float LinearFunc::getAlphaValue() const
{
	Vector2f s, e;
	if (!isOnX())
	{
		s = Vector2f(d.startD, getYGivenX(d.startD));
		e = Vector2f(d.endD, getYGivenX(d.endD));
		//cerr << "s = " << s << endl;
		//cerr << "e = " << e << endl;
	}
	else
	{
		s = Vector2f(getXGivenY(d.startD), d.startD);
		e = Vector2f(getXGivenY(d.endD), d.endD);
		//cerr << "s = " << s << endl;
		//cerr << "e = " << e << endl;
	}
	return get_angle_vector(e - s);
}

const float LinearFunc::getSlope() const
{
	return -a / b;
}

const float LinearFunc::getIntercept() const
{
	if (isOnX())
		return getInterceptX();
	return getInterceptY();
}

const float LinearFunc::getInterceptY() const
{
	return -c / b;
}

const float LinearFunc::getInterceptX() const
{
	return -c / a;
}

const bool LinearFunc::isInDomain(const Vector2f p) const
{
	if (isOnX())
	{
		if (p.y() >= d.startD && p.y() <= d.endD)
			return 1;
	}
	else
	{
		if (p.x() >= d.startD && p.x() <= d.endD)
			return 1;
	}
	return 0;
}

const bool LinearFunc::isInDomain(const Vector3f p) const
{
	return isInDomain(Vector2f(p.x(), p.y()));
}

const Vector2f LinearFunc::findNearestPointInDomain(const Vector2f p) const
{
	if (isInDomain(p))
		return p;

	if (isOnX())
	{
		if (p.y() < d.startD)
			return Vector2f(getXGivenY(d.startD), d.startD);
		return Vector2f(getXGivenY(d.endD), d.endD);
	}
	else
	{
		if (p.x() < d.startD)
			return Vector2f(d.startD, getYGivenX(d.startD));
		return Vector2f(d.endD, getYGivenX(d.endD));
	}
}

const Vector3f LinearFunc::findNearestPointInDomain(const Vector3f p) const
{
	return findNearestPointInDomain(Vector3f(p.x(), p.y(), 0));
}

const Vector2f LinearFunc::getStartPos() const
{
	if (isOnX())
		return Vector2f(getXGivenY(d.startD), d.startD);
	return Vector2f(d.startD, getYGivenX(d.startD));
}

const Vector2f LinearFunc::getEndPos() const
{
	if (isOnX())
		return Vector2f(getXGivenY(d.endD), d.endD);
	return Vector2f(d.endD, getYGivenX(d.endD));
}

const bool LinearFunc::isOnLine(Vector2f p) const
{
	/*if (isOnX())
		if (abs(getXGivenY(p.y()) - p.x()) < 0.01f)
			return 1;
	if (!isOnX())
		if (abs(getYGivenX(p.x()) - p.y()) < 0.01f)
			return 1;*/

	if (getDistanceFromLine(*this, p) < 0.01f)
		return 1;

	return 0;
}
