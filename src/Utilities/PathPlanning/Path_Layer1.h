#ifndef __PATH_LAYER1_H_
#define __PATH_LAYER1_H_

#include "LinearFunc.h"
#include "CircularFunc.h"

struct Path_Layer1 {
	LinearFunc mLinearFunc;
	CircularFunc mCircularFunc;
	bool mFlagLinear, mFlagCircular;
	bool isLinearFirst;

	Path_Layer1();

	void setLinearFunc(const LinearFunc lf);

	void setCircularFunc(const CircularFunc cf);

	const float get_distance_path() const;
};

#endif //__PATH_LAYER1_H_
