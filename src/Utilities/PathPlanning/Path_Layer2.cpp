#include "Path_Layer2.h"

void Path_Layer2::append(const Path_Layer1 p)
{
	mPaths.push_back(p);
}

void Path_Layer2::append(const Path_Layer2 p)
{
	for (unsigned int i = 0; i < p.mPaths.size(); i++)
		mPaths.push_back(p.mPaths[i]);
}

const float Path_Layer2::get_distance_path() const
{
	float res = 0.0f;

	for (unsigned int i = 0; i < mPaths.size(); i++)
		res += mPaths[i].get_distance_path();

	return res;
}
