#ifndef __PPGRAPH_H_
#define __PPGRAPH_H_

#include <deque>

namespace pp_graph
{
	inline std::deque<unsigned int> dijkstra(unsigned int n, unsigned int src, unsigned int dst, float a[100][100])
	{
		float cost[100];
		bool mark[100];
		int father[100];
		for (unsigned int i = 0; i < n; i++)
		{
			cost[i] = 999999;
			father[i] = -1;
			mark[i] = 0;
		}
		
		cost[src] = 0.0f;
		for (unsigned int i = 0; i < n - 1; i++)
		{
			float  min = 999999;
			unsigned int index = 0;
			for (unsigned int j = 0; j < n; j++)
				if (!mark[j] && cost[j] < min)
				{
					min = cost[j];
					index = j;
				}
			mark[index] = 1;
			for (unsigned int j = 0; j < n; j++)
				if (!mark[j] && a[j][index] > 0.0f)
					if (cost[index] + a[j][index] < cost[j])
					{
						cost[j] = cost[index] + a[j][index];
						father[j] = index;
					}
		}

		std::deque<unsigned int> res;
		while (dst != src)
		{
			res.push_front(dst);
			if (father[dst] == -1)
			{
				std::deque<unsigned int> res2;
				return res2;
			}
			dst = father[dst];
		}
		res.push_front(dst);
		return res;
	}
};

#endif // __P_P_GRAPH_H_
