#ifndef __PPMATH_H_
#define __PPMATH_H_

#include <cmath>
#include <BasicCMath.h>
#include <Vector2.h>
#include "LinearFunc.h"

#include <iostream>


namespace pp_math
{
	using namespace std;
	using namespace MIKE;

	inline const float get_angle_positive(const float angle)
	{
		float res = angle;
		while (res < 0.0f)
			res += 2.0f * M_PI;
		return res;
	}

	inline const float v2f_distance(const MIKE::Vector2f p1, const MIKE::Vector2f p2)
	{
		return sqrt(pow(p1.x() - p2.x(), 2.0f) + pow(p1.y() - p2.y(), 2.0f));
	}

	inline const float get_slope(const MIKE::Vector2f p1, const MIKE::Vector2f p2)
	{
		if (p1.x() - p2.x() == 0)
			return tan(M_PI_2);
		return (p1.y() - p2.y()) / (p1.x() - p2.x());
	}

	inline const float get_slope_inverse(const MIKE::Vector2f p1, const MIKE::Vector2f p2)
	{
		if (p1. y() - p2.y() == 0)
			return tan(M_PI_2);
		return (p1.x() - p2.x()) / (p2.y() - p1.y());
	}

	inline const float get_inner_product(const MIKE::Vector2f v1, const MIKE::Vector2f v2)
	{
		return (v1.x() * v2.x()) + (v1.y() * v2.y());
	}

	inline const float get_angle_vector(const MIKE::Vector2f v)
	{
		/*MIKE::Vector2f i = MIKE::Vector2f(1.0f, 0.0f);
		float m = 1.0f;
		if (v.y() < i.y())
			m *= -1.0f;
		return get_angle_between_vecotrs(v, i) * m;*/

		/*float res = atan(v.y() / v.x());
		if (v.x() == 0.0f)
		{
			res = M_PI_2;
			if (v.y() < 0.0f)
				res *= -1.0f;
		}
		return res;*/

		return atan2f(v.y(), v.x());
	}

	inline const float get_angle_between_vectors(const MIKE::Vector2f v1, const MIKE::Vector2f v2)
	{
		MIKE::Vector2f o = MIKE::Vector2f(0.0f, 0.0f);
		float res = (get_inner_product(v1, v2)) / (v2f_distance(v1, o) * v2f_distance(v2, o));
		if (res > 0.99999f)
			res = 0.99999f;
		if (res < -0.99999f)
			res = -0.99999f;
		return acos(res);

		/*float a1 = get_angle_positive(get_angle_vector(v1));
		float a2 = get_angle_positive(get_angle_vector(v2));
		float res = abs(a1 - a2);
		while (res > M_PI)
			res -= M_PI;
		return res;*/
	}

	inline const float get_angle_standard(const float angle)
	{
		float res = get_angle_positive(angle);
		while (res > M_PI)
			res -= 2.0 * M_PI;
		return res;
	}

	inline const bool isUpperVector(const MIKE::Vector2f vTarget, const MIKE::Vector2f v0)
	{
		float rotateAngle = -get_angle_vector(v0);
		MIKE::Vector2f rotatePos;
		rotatePos.x() = (vTarget.x() * cos(rotateAngle)) - (vTarget.y() * sin(rotateAngle));
		rotatePos.y() = (vTarget.x() * sin(rotateAngle)) + (vTarget.y() * cos(rotateAngle));
		if (rotatePos.y() >= -0.000001f)
			return 1;
		return 0;
	}

	inline const bool isLowerVector(const MIKE::Vector2f vTarget, const MIKE::Vector2f v0)
	{
		float rotateAngle = -get_angle_vector(v0);
		MIKE::Vector2f rotatePos;
		rotatePos.x() = (vTarget.x() * cos(rotateAngle)) - (vTarget.y() * sin(rotateAngle));
		rotatePos.y() = (vTarget.x() * sin(rotateAngle)) + (vTarget.y() * cos(rotateAngle));
		if (rotatePos.y() <= 0.000001f)
			return 1;
		return 0;
	}

	inline const MIKE::Vector2f getIntersectionPointTwoLines(const LinearFunc f1, const LinearFunc f2)
	{
		float x, y;
		x = (((f1.c * f2.b) / (f2.a * f1.b)) - (f2.c / f2.a)) / (1.0f - ((f1.a * f2.b) / (f2.a * f1.b)));
		y = ((-f1.a / f1.b) * x) - (f1.c / f1.b);

		if (std::abs(f1.a) < 0.000001f)
		{
			y = -f1.c / f1.b;
			x = ((-f2.b * y) + (-f2.c)) / f2.a;
		}
		if (std::abs(f2.a) < 0.000001f)
		{
			y = -f2.c / f2.b;
			x = ((-f1.b * y) + (-f1.c)) / f1.a;
		}

		if (std::abs(f1.b) < 0.000001f)
		{
			x = -f1.c / f1.a;
			y = ((-f2.a * x) + (-f2.c)) / f2.b;
		}
		if (std::abs(f2.b) < 0.000001f)
		{
			x = -f2.c / f2.a;
			y = ((-f1.a * x) + (-f1.c)) / f1.b;
		}

		return MIKE::Vector2f(x, y);
	}

	inline const MIKE::Vector2f getIntersectionPointLineAndPoint(const LinearFunc f, const MIKE::Vector2f p)
	{
		LinearFunc f_tmp = LinearFunc();
		f_tmp.a = -f.b;
		f_tmp.b = f.a;
		f_tmp.c = -((f_tmp.a * p.x()) + (f_tmp.b * p.y()));

		return getIntersectionPointTwoLines(f, f_tmp);
	}

	inline const float getDistanceFromLine(const LinearFunc f, const MIKE::Vector2f p)
	{
		return v2f_distance(getIntersectionPointLineAndPoint(f, p), p);
	}

	inline const float getDiffTwoLines(const LinearFunc f1, const LinearFunc f2)
	{
		float diff = 0;

		Vector2f v1 = Vector2f(cos(f1.getAlphaValue()), sin(f1.getAlphaValue()));
		Vector2f v2 = Vector2f(v1.x() * -1.0f, v1.y() * -1.0f);
		Vector2f v3 = Vector2f(cos(f2.getAlphaValue()), sin(f2.getAlphaValue()));
		diff += gRadToDeg(gMin(get_angle_between_vectors(v1, v3), get_angle_between_vectors(v2, v3)));

		Vector2f p1 = f1.getStartPos();
		Vector2f p2 = f1.getEndPos();

		Vector2f q1 = f2.getStartPos();
		Vector2f q2 = f2.getEndPos();

		diff += gMin(gMin(v2f_distance(p1, q2), v2f_distance(p2, q1)), gMin(v2f_distance(p1, q1), v2f_distance(p2, q2)));

		return diff;
	}
};

#endif // __P_P_MATH_H_
