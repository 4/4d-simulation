#ifndef __PATH_PLANNING_H_
#define __PATH_PLANNING_H_

#include "Path_Layer1.h"
#include "Path_Layer2.h"
#include "Path_Layer3.h"

#include "Obstacle.h"
#include <Vector2.h>
#include "Vector2Dir.h"
#include <deque>
#include <map>

class PathPlanning {
private:
	Path_Layer3 mPath;
	std::deque<MIKE::Vector2f> mPathPoints;
	std::deque<MIKE::Vector2f> mPathPointsRes;

private:
	std::map<std::string, bool> layer3_savedStates;
	bool layer3_savedState(MIKE::Vector2f, MIKE::Vector2f);

private:
	std::map<std::string, bool> layer2_isExsist;
	std::map<std::string, Path_Layer2> layer2_state;

	bool isExistLayer2(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle);
	Path_Layer2 existStateLayer2(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle);
	void setStateLayer2(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle, Path_Layer2 pl);

private:
	const Path_Layer1 getPathPlanning_layer1(const MIKE::Vector2f startPos, const float startAngle, MIKE::Vector2f endPos, const float endAngle);
	const Path_Layer2 getPathPlanning_layer2(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle);
	void getPathPlanning_layer3(const MIKE::Vector2f startPos, const float startAngle, 
		const MIKE::Vector2f endPos, const float endAngle, const std::deque<Obstacle> obstacles, 
		std::deque<Vector2fDir> &pathway);
	const Path_Layer3 getPathPlanning_layer4(const MIKE::Vector2f startPos, const float startAngle, 
		const MIKE::Vector2f endPos, const float endAngle, std::deque<Obstacle> obstacles);
	const Path_Layer3 getPathPlanning_basic(const MIKE::Vector2f startPos, const float startAngle, 
		const MIKE::Vector2f endPos, const float endAngle, std::deque<Obstacle> obstacles);
	const Path_Layer3 getPathPlanning(const MIKE::Vector2f startPos, const MIKE::Vector2f endPos, std::deque<Obstacle> obstacles);
	const MIKE::Vector2f getPointOutOfObstacle(MIKE::Vector2f pos, std::deque<Obstacle> obstacles);

private:
	const std::deque<MIKE::Vector2f> draw_path_layer1(const Path_Layer1 path, const float minDis, const float dis);
	const std::deque<MIKE::Vector2f> drawPath(const Path_Layer3 path, const float minDis, const float dis);

public:
	PathPlanning();
	PathPlanning(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle, 
			const std::deque<Obstacle> obstacles);
	PathPlanning(const MIKE::Vector2f startPos, const float startAngle, const MIKE::Vector2f endPos, const float endAngle);

	PathPlanning(const MIKE::Vector2f startPos, const MIKE::Vector2f endPos, const std::deque<Obstacle> obstacles);
	PathPlanning(const MIKE::Vector2f startPos, const MIKE::Vector2f endPos);

	const Path_Layer3 &getPath() const;
	const std::deque<MIKE::Vector2f> &getPathPoints(const float dis);
};

#endif // __PATH_PLANNING_H_
