#include "LinearDomain.h"

#include <BasicCMath.h>

using namespace MIKE;

LinearDomain::LinearDomain(const float d1, const float d2, const bool isForDraw)
{
	startD = d1;
	endD   = d2;

	if (!isForDraw)
	{
		startD = gMin(d1, d2);
		endD   = gMax(d1, d2);
	}
}
