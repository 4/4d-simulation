#ifndef __OBSTACLE_H_
#define __OBSTACLE_H_

#include <Vector2.h>

struct Obstacle {
	MIKE::Vector2f mPoint;
	float r;

	Obstacle(const MIKE::Vector2f p, const float r): mPoint(p), r(r) {}
};

#endif // __OBSTACLE_H_
