#ifndef __CIRCULAR_FUNC_H_
#define __CIRCULAR_FUNC_H_

#include "CircularDomain.h"

struct CircularFunc {
	float alpha, beta, r;
	CircularDomain d;

	CircularFunc(): alpha(0),beta(0), r(0)  {}
	CircularFunc(const float alpha1, const float beta1, const float r1, const CircularDomain d1): 
	alpha(alpha1), beta(beta1), r(r1), d(d1) {}
};

#endif // __CIRCULAR_FUNC_H_
