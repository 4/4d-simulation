// M.I.K.E. 3D Soccer Simulation team
// File: Singleton.h

#ifndef _MIKE_SINGLETON_H
#define _MIKE_SINGLETON_H

#include <cstddef>

namespace MIKE {

	// Singleton pattern for creating single-instance global classes
	template<class T> class Singleton {

		// Overload constructor and operator= to prevent direct instancing
		Singleton(const Singleton<T> &singleton);
		void operator= (const Singleton<T> &singleton);

	public:

		inline Singleton(); // Default constructor
		inline virtual ~Singleton(); // Destructor

		static inline T* Instance(); // Returns the pointer to the singleton

	}; // class Singleton

	template<class Type>
	inline Singleton<Type>::Singleton() {
	
	}

	template<class Type>
	inline Singleton<Type>::~Singleton() {

	}

	template<class Type>
	inline Type* Singleton<Type>::Instance() {
		static Type *singleton_ptr = new Type();
		return singleton_ptr;
	}

} // namespace MIKE

#endif // _MIKE_SINGLETON_H

