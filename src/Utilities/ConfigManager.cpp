// M.I.K.E. 3D Soccer Simulation team
// File: ConfigManager.cpp

#include <ConfigManager.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <boost/lexical_cast.hpp>
#include <Vector3.h>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>

using namespace std;
using namespace MIKE;

ConfigManager::ConfigManager() {

}

ConfigManager::~ConfigManager() {

}

const boost::property_tree::ptree& ConfigManager::getConfigTree() const {
	return ptree;
}

void ConfigManager::initialize(int argc, char *argv[]) {
	setByFiles();
	setByArguments(argc, argv);
}

void ConfigManager::setByFiles() {
	boost::filesystem::path path("Data/");
	for(boost::filesystem::directory_iterator iter(path); iter != boost::filesystem::directory_iterator(); iter ++) {
		string ext = iter->path().extension().string();
		if(ext == ".cfg" || ext == ".xml") {
			cout << "ConfigFile found: " << "[" << iter->path().string() << "]" << endl;

			boost::property_tree::ptree child;
			stringstream input;
			ifstream ifile(iter->path().string().c_str(), ios::in);
			input << string((std::istreambuf_iterator<char>(ifile)), std::istreambuf_iterator<char>());
			ifile.close();

			if (ext == ".cfg")
				boost::property_tree::read_ini(input, child);
			else if (ext == ".xml")
				boost::property_tree::read_xml(input, child);

			ptree.put_child(iter->path().stem().string(), child);
		}
	}
}

void ConfigManager::setByArguments(int argc, char *argv[]) {
	bool error = false;
	for(int i = 1; i < argc && !error; i ++)
		if(strcmp(argv[i], "--host") == 0) {
			if(++i == argc)
				error = true;
			else
				ptree.put("Server.Connection.Host", argv[i]);
		} else if(strcmp(argv[i], "--port") == 0) {
			if(++i == argc)
				error = true;
			else
				ptree.put("Server.Connection.Port", argv[i]);
		} else if(strcmp(argv[i], "--teamname") == 0) {
			if(++i == argc)
				error = true;
			else
				ptree.put("Agents.Global.Teamname", argv[i]);
		} else if(strcmp(argv[i], "--number") == 0) {
			if(++i == argc)
				error = true;
			else
				ptree.put("Agents.Global.Number", argv[i]);
		} else if(strcmp(argv[i], "--unum") == 0) {
			if(++i == argc)
				error = true;
			else
				ptree.put("Agents.Agent" + ptree.get<string>("Agents.Global.Number") + "." + "UniNum", argv[i]);
		} else if(strcmp(argv[i], "--beam") == 0) {
			Vector3f beam;
			
			if(++i == argc)
				error = true;
			else
				beam.x() = boost::lexical_cast<float>(argv[i]);
			
			if(++i == argc)
				error = true;
			else
				beam.y() = boost::lexical_cast<float>(argv[i]);

			if(++i == argc)
				error = true;
			else
				beam.z() = boost::lexical_cast<float>(argv[i]);

			ptree.put("Agents.Agent" + ptree.get<string>("Agents.Global.Number") + "." + "Beam", beam);
		} else if(strcmp(argv[i], "--help") == 0)
			error = true;
		else
			error = true;

	if(error) {
		cout << "Usage: " << argv[0] << " [Options]" << endl;
		cout << "Options:" << endl;
		cout << "	--help				: Show this text" << endl;
		cout << "	--host hostname			: Set hostname to connect to server" << endl;
		cout << "	--port portnumber		: Set destination port to connect to server" << endl;
		cout << "	--teamname teamname		: Set the team name" << endl;
		cout << "	--number number			: Set agent number" << endl;
		cout << "	--unum uninum			: Set agent uniform number" << endl;
		cout << "	--beam beampoint		: Set beam point" << endl;
		cout << endl;
	}
}

