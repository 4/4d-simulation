#ifndef __ANN_H_
#define __ANN_H_

#include <deque>
#include <string>

class ANN
{
private:
	unsigned int hiddenNum;
	std::deque<double> weights;

	std::deque<double> min_data;
	std::deque<double> max_data;

public:
	ANN(std::string filename);
	~ANN();

	std::deque<double> get(const std::deque<double> &_inputs) const;
};

#endif // __ANN_H_
