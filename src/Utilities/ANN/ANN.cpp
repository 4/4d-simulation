#include "ANN.h"

//#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

ANN::ANN(string filename)
{
	ifstream infilewb((filename + ".wb").c_str());

	infilewb >> hiddenNum;
	while (!infilewb.eof())
	{
		double tmp;
		infilewb >> tmp;
		weights.push_back(tmp);
	}
	//weights.pop_back();
	infilewb.close();


	ifstream infilemm((filename + ".mm").c_str());

	while (!infilemm.eof())
	{
		double min, max;
		infilemm >> min >> max;
		min_data.push_back(min);
		max_data.push_back(max);
	}
	//min_data.pop_back();
	//max_data.pop_back();
	infilemm.close();

	//cerr << hiddenNum << endl;
	//for (unsigned int i = 0; i < weights.size(); i++)
	//	cerr << weights[i] << endl;

	//for (unsigned int i = 0; i < min_data.size(); i++)
	//	cerr << min_data[i] << "  " << max_data[i] << endl;
}

ANN::~ANN()
{
}

deque<double> ANN::get(const deque<double> &_inputs) const
{
	/// normalize inputs
	deque<double> inputs;
	for (unsigned int i = 0; i < _inputs.size(); i++)
		inputs.push_back(2.0f * ((_inputs[i] - min_data[i]) / (max_data[i] - min_data[i])) - 1.0f);

	/// calculate outputs
	unsigned int outputWeightsIndex = (inputs.size() + 1) * hiddenNum;
	unsigned int outputNum = (weights.size() - outputWeightsIndex + 1) / (hiddenNum + 1);
	unsigned int outputBiasIndex = outputWeightsIndex + hiddenNum * outputNum;

	deque<double> result;
	for (unsigned int i = 0; i < outputNum; i++)
		result.push_back(0);

	unsigned int hiddenWeightsIndex = 0;
	unsigned int hiddenBiasIndex    = inputs.size() * hiddenNum;

	for (unsigned int i = 0; i < hiddenNum; i++)
	{
		double sum = 0;
		for (unsigned int j = 0; j < inputs.size(); j++)
			sum += inputs[j] * weights[hiddenWeightsIndex + j * hiddenNum];
		hiddenWeightsIndex ++;
		sum += weights[hiddenBiasIndex ++];
		sum = 2.0f / (1.0f + exp(-2.0 * sum)) - 1.0f;

		for (unsigned int i = 0; i < outputNum; i++)
			result[i] += sum * weights[outputWeightsIndex ++];
	}

	for (unsigned int i = 0; i < outputNum; i++)
		result[i] += weights[outputBiasIndex ++];

	/// normalize outputs
	for (unsigned int i = inputs.size(); i < min_data.size(); i++)
		result[i - inputs.size()] = ((result[i - inputs.size()] - (-1.0f)) / (1.0f - (-1.0f))) * (max_data[i] - min_data[i]) + min_data[i];

	return result;
}
