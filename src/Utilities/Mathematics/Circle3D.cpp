// M.I.K.E. 3D Soccer Simulation team
// File: Circle3D.cpp

#include "Circle3D.h"

using namespace MIKE;

Circle3D::Circle3D() {
	mc = Vector3f(0.0, 0.0, 0.0);
	mr = 0.0;
	ma = Vector3f(0.0, 0.0, 0.0);
	mb = Vector3f(0.0, 0.0, 0.0);
}

Circle3D::Circle3D(Vector3f& fC, float fR, Vector3f& fA, Vector3f& fB) {
	mc = fC;
	mr = fR;
	ma = fA;
	mb = fB;
}

Circle3D::~Circle3D() {
}
