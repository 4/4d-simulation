// M.I.K.E. 3D Soccer Simulation team
// File: Geometry.cpp

#include "Geometry.h"

using namespace MIKE;

int MIKE::gFact(int n) {
	if (n < 0)
		return 0;
	if (n == 0 || n == 1)
		return 1;
	return (n * gFact(n - 1));
}

int MIKE::gSelectMfromN(int n, int m) {
	return (gFact(n) / (gFact(n - m) * gFact(m)));
}
