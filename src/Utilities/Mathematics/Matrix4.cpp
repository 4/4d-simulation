// M.I.K.E. 3D Soccer Simulation team
// File: Matrix4.cpp

#include "Matrix4.h"
#include "Vector3.h"

using namespace MIKE;

Vector3f Matrix4::getEulerAngles() const {
	if (table[2][0] != +1 && table[2][0] != -1) {
		float Ry1 = -asin(table[2][0]);
		//float Ry2 = M_PI - Ry1;

		float Rx1 = atan2(table[2][1]/cos(Ry1), table[2][2]/cos(Ry1));
		//float Rx2 = atan2(table[2][1]/cos(Ry2), table[2][2]/cos(Ry2));

		float Rz1 = atan2(table[1][0]/cos(Ry1), table[0][0]/cos(Ry1));
		//float Rz2 = atan2(table[1][0]/cos(Ry2), table[0][0]/cos(Ry2));

		return Vector3f(Rx1, Ry1, Rz1);
		//return Vector3f(Rx2, Ry2, Rz2);
	}
	else {
		float Rx = 0.0f;
		float Ry = 0.0f;
		float Rz = 0.0f;
		if (table[2][0] == -1) {
			Ry = M_PI / 2.0f;
			Rx = Rz + atan2(table[0][1], table[0][2]);
			return Vector3f(Rx, Ry, Rz);
		}
		else { // table[2][0] == +1
			Ry = -M_PI / 2.0f;
			Rx = -Rz + atan2(-table[0][1], -table[0][2]);
			return Vector3f(Rx, Ry, Rz);
		}
	}
}
