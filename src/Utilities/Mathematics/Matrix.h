#ifndef __MATRIX_H_
#define __MATRIX_H_

#include <iostream>
#include <sstream>
#include <string>

template <class InType>
class Matrix
{
private:
	unsigned int row, column;
	InType **elements;
	bool elementsEmpty;

	void createElms(unsigned int _row, unsigned int _column);
	void deleteElms();

public:
	Matrix(unsigned int _row, unsigned int _column);
	template <class T>
	Matrix(unsigned int _row, unsigned int _column, T **in);
	template <class T>
	void operator= (const Matrix<T> &m);
	void operator= (const Matrix &m);
	template <class T>
	Matrix(const Matrix<T> &m);
	Matrix(const Matrix &m);
	void set(const InType in);
	~Matrix();

	template <class T>
	void append(const Matrix<T> &m);
	void append(const Matrix &m);

	const std::string toString() const;
	const unsigned int getRow() const;
	const unsigned int getColumn() const;
	InType **getElms() const;

	const Matrix operator+ (const Matrix &m) const;
	void operator+= (const Matrix &m);
	const Matrix operator+ (const InType &in) const;
	void operator+= (const InType &in);
	const Matrix operator- (const Matrix &m) const;
	void operator-= (const Matrix &m);
	const Matrix operator- (const InType &in) const;
	void operator-= (const InType &in);
	const Matrix operator* (const Matrix &m) const;
	void operator*= (const Matrix &m);
	const Matrix operator* (const InType &in) const;
	void operator*= (const InType &in);
	const Matrix operator/ (const InType &in) const;
	void operator/= (const InType &in);
	InType *operator[] (unsigned int r);
	const bool operator== (const Matrix &m) const;

	friend std::ostream &operator <<(std::ostream &out, const Matrix &m)
	{
		out << m.toString();
		return out;
	}

	const bool isSquare() const;
	void identity();
	void diagonal();
	const Matrix getDiagonal() const;
	void transpose();
	const Matrix getTranspose() const;
	void _minor(unsigned int elm_r, unsigned int elm_c);
	const Matrix get_minor(unsigned int elm_r, unsigned int elm_c) const;
	void cofactor();
	const Matrix getCofactor() const;
	void adjugate();
	const Matrix getAdjugate() const;
	void inverse();
	const Matrix getInverse() const;

	const long double determinant() const;
	const long double trace() const;
};

template <class InType>
void Matrix<InType>::createElms(unsigned int _row, unsigned int _column)
{
	row = _row;
	column = _column;
	elements = new InType*[row];
	for (unsigned int i = 0; i < row; i++)
		elements[i] = new InType[column];
	elementsEmpty = false;
}

template <class InType>
void Matrix<InType>::deleteElms()
{
	if (!elementsEmpty && elements)
	{
		for (unsigned int i = 0; i < row; i++)
			if (elements[i])
				delete[] elements[i];
		delete[] elements;
	}
	row = 0;
	column = 0;
	elementsEmpty = true;
}

template <class InType>
Matrix<InType>::Matrix(unsigned int _row, unsigned int _column)
{
	elementsEmpty = true;
	createElms(_row, _column);
}

template <class InType> template <class T>
Matrix<InType>::Matrix(unsigned int _row, unsigned int _column, T **in)
{
	elementsEmpty = true;
	createElms(_row, _column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] = in[i][j];
}

template <class InType> template <class T>
void Matrix<InType>::operator= (const Matrix<T> &m)
{
	deleteElms();
	createElms(m.getRow(), m.getColumn());

	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] = m.getElms()[i][j];
}

template <class InType>
void Matrix<InType>::operator= (const Matrix &m)
{
	deleteElms();
	createElms(m.getRow(), m.getColumn());

	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] = m.getElms()[i][j];
}

template <class InType> template <class T>
Matrix<InType>::Matrix(const Matrix<T> &m)
{
	elementsEmpty = true;
	(*this) = m;
}

template <class InType>
Matrix<InType>::Matrix(const Matrix &m)
{
	elementsEmpty = true;
	(*this) = m;
}

template <class InType>
void Matrix<InType>::set(const InType in)
{
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] = in;
}

template <class InType>
Matrix<InType>::~Matrix()
{
	deleteElms();
}

template <class InType> template <class T>
void Matrix<InType>::append(const Matrix<T> &m)
{
	if (m.getColumn() != column)
		return ;

	Matrix<InType> result(row + m.getRow(), column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result[i][j] = elements[i][j];
	for (unsigned int i = row; i < m.getRow(); i++)
		for (unsigned int j = 0; j < m.getColumn(); j++)
			result[i][j] = m.getElms()[i][j];
	*this = result;
}

template <class InType>
void Matrix<InType>::append(const Matrix &m)
{
	if (m.getColumn() != column)
		return ;

	Matrix<InType> result(row + m.getRow(), column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result[i][j] = elements[i][j];
	for (unsigned int i = row; i < m.getRow(); i++)
		for (unsigned int j = 0; j < m.getColumn(); j++)
			result[i][j] = m.getElms()[i][j];
	*this = result;
}

template <class InType>
const std::string Matrix<InType>::toString() const
{
	std::stringstream s;
	for (unsigned int i = 0; i < row; i++)
	{
		for (unsigned int j = 0; j < column; j++)
			s << elements[i][j] << " ";
		if (i < row - 1)
			s << std::endl;
	}
	return s.str();
}

template <class InType>
const unsigned int Matrix<InType>::getRow() const
{
	return row;
}

template <class InType>
const unsigned int Matrix<InType>::getColumn() const
{
	return column;
}

template <class InType>
InType **Matrix<InType>::getElms() const
{
	return elements;
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator+ (const Matrix<InType> &m) const
{
	if (row != m.row || column != m.column)
		return Matrix(0, 0);

	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] + m.elements[i][j];

	return result;
}

template <class InType>
void Matrix<InType>::operator+= (const Matrix<InType> &m)
{
	if (row != m.row || column != m.column)
		return ;

	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] += m.elements[i][j];
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator+ (const InType &in) const
{
	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] + in;

	return result;
}


template <class InType>
void Matrix<InType>::operator+= (const InType &in)
{
	(*this) = (*this) + in;
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator- (const Matrix<InType> &m) const
{
	if (row != m.row || column != m.column)
		return Matrix(0, 0);

	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] - m.elements[i][j];

	return result;
}

template <class InType>
void Matrix<InType>::operator-= (const Matrix<InType> &m)
{
	if (row != m.row || column != m.column)
		return ;

	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] -= m.elements[i][j];
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator- (const InType &in) const
{
	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] - in;

	return result;
}


template <class InType>
void Matrix<InType>::operator-= (const InType &in)
{
	(*this) = (*this) - in;
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator* (const Matrix<InType> &m) const
{
	if (column != m.row)
		return Matrix(0, 0);

	Matrix<InType> result(row, m.column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < m.column; j++)
		{
			InType sum = 0;
			for (unsigned int k = 0; k < column; k++)
				sum += elements[i][k] * m.elements[k][j];
			result.elements[i][j] = sum;
		}

	return result;
}

template <class InType>
void Matrix<InType>::operator*= (const Matrix<InType> &m)
{
	(*this) = (*this) * m;
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator* (const InType &in) const
{
	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] * in;

	return result;
}


template <class InType>
void Matrix<InType>::operator*= (const InType &in)
{
	(*this) = (*this) * in;
}

template <class InType>
const Matrix<InType> Matrix<InType>::operator/ (const InType &in) const
{
	Matrix<InType> result(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			result.elements[i][j] = elements[i][j] / in;

	return result;
}


template <class InType>
void Matrix<InType>::operator/= (const InType &in)
{
	(*this) = (*this) / in;
}

template <class InType>
InType *Matrix<InType>::operator[] (unsigned int r)
{
	return elements[r];
}

template <class InType>
const bool Matrix<InType>::operator== (const Matrix &m) const
{
	if (row != m.row || column != m.column)
		return false;

	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			if (elements[i][j] != m.elements[i][j])
				return false;

	return true;
}

template <class InType>
const bool Matrix<InType>::isSquare() const
{
	return row == column;
}

template <class InType>
void Matrix<InType>::identity()
{
	set(0);
	for (unsigned int i = 0; i < row; i++)
		elements[i][i] = 1;
}

template <class InType>
void Matrix<InType>::diagonal()
{
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			if (i != j)
				elements[i][j] = 0;
}

template <class InType>
const Matrix<InType> Matrix<InType>::getDiagonal() const
{
	Matrix res = *this;
	res.diagonal();
	return res;
}

template <class InType>
void Matrix<InType>::transpose()
{
	Matrix copy = *this;
	deleteElms();
	createElms(copy.column, copy.row);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			elements[i][j] = copy.elements[j][i];
}

template <class InType>
const Matrix<InType> Matrix<InType>::getTranspose() const
{
	Matrix res = *this;
	res.transpose();
	return res;
}

template <class InType>
void Matrix<InType>::_minor(unsigned int elm_r, unsigned int elm_c)
{
	Matrix copy = *this;
	deleteElms();
	createElms(copy.row - 1, copy.column - 1);

	unsigned int copy_i = 0;
	for (unsigned int i = 0; i < row; i++)
	{
		if (copy_i == elm_r)
			copy_i ++;

		unsigned int copy_j = 0;
		for (unsigned int j = 0; j < column; j++)
		{
			if (copy_j == elm_c)
				copy_j ++;
			elements[i][j] = copy.elements[copy_i][copy_j];
			copy_j ++;
		}
		copy_i ++;
	}
}

template <class InType>
const Matrix<InType> Matrix<InType>::get_minor(unsigned int elm_r, unsigned int elm_c) const
{
	Matrix res = *this;
	res._minor(elm_r, elm_c);
	return res;
}

template <class InType>
void Matrix<InType>::cofactor()
{
	if (!isSquare())
		return ;

	Matrix cofactor(row, column);
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			cofactor.elements[i][j] = (((i + j) % 2) ? -1.0f : 1.0f) * get_minor(i, j).determinant();
	*this = cofactor;
}

template <class InType>
const Matrix<InType> Matrix<InType>::getCofactor() const
{
	Matrix res = *this;
	res.cofactor();
	return res;
}

template <class InType>
void Matrix<InType>::adjugate()
{
	if (!isSquare())
		return ;

	*this = getCofactor().getTranspose();
}

template <class InType>
const Matrix<InType> Matrix<InType>::getAdjugate() const
{
	Matrix res = *this;
	res.adjugate();
	return res;
}

template <class InType>
void Matrix<InType>::inverse()
{
	if (!isSquare() || determinant() == 0.0f)
		return ;

	*this = getAdjugate() / determinant();
}

template <class InType>
const Matrix<InType> Matrix<InType>::getInverse() const
{
	Matrix res = *this;
	res.inverse();
	return res;
}

template <class InType>
const long double Matrix<InType>::determinant() const
{
	if (!isSquare())
		return 0;
	if (row == 1)
		return elements[0][0];

	long double sum = 0;
	for (unsigned int i = 0; i < row; i++)
		sum += ((i % 2) ? -1.0f : 1.0f) * elements[i][0] * get_minor(i, 0).determinant();

	return sum;
}

template <class InType>
const long double Matrix<InType>::trace() const
{
	long double sum = 0;
	for (unsigned int i = 0; i < row; i++)
		for (unsigned int j = 0; j < column; j++)
			if (i == j)
				sum += elements[i][j];
	return sum;
}

#endif // __MATRIX_H_
