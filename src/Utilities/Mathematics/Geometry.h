// M.I.K.E. 3D Soccer Simulation team
// File: Geometry.h

#ifndef _MIKE_GEOMETRY_H
#define _MIKE_GEOMETRY_H

#include <cmath>

#ifndef M_PI
#	define M_PI 3.1415926535897932384626433832795
#endif

#ifndef M_PI2 // M_PI / 2
#	define M_PI2 (M_PI / 2.0)
#endif

#define RTD (180.0 / float(M_PI))
#define DTR (float(M_PI) / 180.0)

namespace MIKE {

	template<class Type>
	inline Type gSin(Type angle) {
		return (Type) std::sin(angle);
	}

	template<class Type>
	inline Type gCos(Type angle) {
		return (Type) std::cos(angle);
	}

	template<class Type>
	inline Type gTan(Type angle) {
		return (Type) std::tan(angle);
	}

	template<class Type>
	inline Type gCot(Type angle) {
		return (Type) 1 / gTan(angle);
	}

	template<class Type>
	inline Type gSec(Type angle) {
		return (Type) 1 / gCos(angle);
	}

	template<class Type>
	inline Type gCse(Type angle) {
		return (Type) 1 / gSin(angle);
	}

	int gFact(int n);

	int gSelectMfromN(int n, int m);

} // namespace MIKE

#endif // _MIKE_GEOMETRY_H

