// M.I.K.E. 3D Soccer Simulation team
// File: Circle3D.h

#ifndef _MIKE_CIRCLE3D_H
#define _MIKE_CIRCLE3D_H

#include "Vector3.h"
#include "Geometry.h"

namespace MIKE {

	class Circle3D {
	public:
		Circle3D();
		Circle3D(Vector3f& c, float r, Vector3f& a, Vector3f& b);
		~Circle3D();

		inline Vector3f getPoint(float degree);


		//Setter function
		inline Vector3f& c(){ return mc; }
		inline float& r(){ return mr; }
		inline Vector3f& a(){ return ma; }
		inline Vector3f& b(){ return mb; }

		//Getter function
		inline Vector3f c() const { return mc; }
		inline float r() const { return mr; }
		inline Vector3f a() const { return ma; }
		inline Vector3f b() const { return mb; }

	private:
		Vector3f mc;
		float mr;
		Vector3f ma, mb;
	};

	inline Vector3f Circle3D::getPoint(float degree) // Degree must be in radian
	{
		return Vector3f(mc.x() + mr*gCos(degree)*ma.x() + mr*gSin(degree)*mb.x(),
						mc.y() + mr*gCos(degree)*ma.y() + mr*gSin(degree)*mb.y(),
						mc.z() + mr*gCos(degree)*ma.z() + mr*gSin(degree)*mb.z());
	}

} // namespace MIKE

#endif // _MIKE_CIRCLE3D_H
