// M.I.K.E. 3D Soccer Simulation team
// File: Sphere.h

#ifndef _MIKE_SPHERE_H
#define _MIKE_SPHERE_H

#include "Circle3D.h"
#include "Vector3.h"

namespace MIKE {

	class Sphere
	{
	public:
		Sphere();
		Sphere(const Vector3f& c, float r);
		~Sphere();

		int getCommonPoints(Sphere& s, Circle3D& ci, bool leftRotate = true); // -2 = Sphere -1 = No common Point, 0 = Tangent, 1 = Circle

		// Setter function
		inline Vector3f& c(){ return mc; }
		inline float& r(){ return mr; }

		// Getter function
		inline Vector3f c() const { return mc; }
		inline float r() const { return mr; }

	private:
		Vector3f mc;
	    float mr;
	};

} // namespace MIKE

#endif // _MIKE_SPHERE_H
