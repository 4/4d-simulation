// M.I.K.E. 3D Soccer Simulation team
// File: BezierCurve.cpp

#include "BezierCurve.h"
#include "Geometry.h"

using namespace MIKE;
using namespace std;

BezierCurve::BezierCurve()
{
}

BezierCurve::~BezierCurve()
{
	controlPoints.clear();
}

void BezierCurve::addPoint(float x, float y, float z)
{
	Vector3f p(x, y, z);
	controlPoints.push_back(p);
}

void BezierCurve::addPoint(Vector3f p)
{
	controlPoints.push_back(p);
}

Vector3f BezierCurve::getPointGivenTime(float time)
{
	Vector3f sum(0, 0, 0);

	for (int i = 0; i < (int)controlPoints.size(); i++)
	{
		int n = controlPoints.size();
		int m = i+1;
	    float zarib = gSelectMfromN(n, m) * pow(time, m) * pow(1 - time, n-m);
		/*sum = Vector3f(sum.x() + controlPoints[i].x() * zarib,
					   sum.y() + controlPoints[i].y() * zarib,
					   sum.z() + controlPoints[i].z() * zarib);*/
		sum.x() += controlPoints[i].x() * zarib;
		sum.y() += controlPoints[i].y() * zarib;
		sum.z() += controlPoints[i].z() * zarib;
	}
	return sum;
}

BezierCurve BezierCurve::operator= (const BezierCurve& bc)
{
	controlPoints.clear();
	for (int i = 0; i < (int)bc.controlPoints.size(); i++)
		controlPoints.push_back(bc.controlPoints[i]);
	return (*this);
}
