// M.I.K.E. 3D Soccer Simulation team
// File: Sphere.cpp

#include "Sphere.h"

using namespace MIKE;

Sphere::Sphere() {
	mc = Vector3f(0.0, 0.0, 0.0);
	mr = 0.0;
}

Sphere::Sphere(const Vector3f& fC, float fR) {
	mc = fC;
	mr = fR;
}

Sphere::~Sphere() {
}

int Sphere::getCommonPoints(Sphere& s, Circle3D& ci, bool leftRotate) {
	Vector3f ciC(0.0, 0.0, 0.0);
	//float ciR = 0.0;
	Vector3f ciA(0.0, 0.0, 0.0);
	Vector3f ciB(0.0, 0.0, 0.0);

	Vector3f sC = s.c();
	float sR = s.r();

	float dc = sqrt(pow(this->mc.x() - sC.x(), 2) + pow(this->mc.y() - sC.y(), 2) + pow(this->mc.z() - sC.z(), 2));
	float dr = this->mr + sR;

	if (dr < dc) // Motekharej
		return -1; // No Common Point
	if (dc < 0.0001) // Ham markaz
		{
			if (this->mr == sR)
				return -2; // Sphere
			else
				return -1; // No Common Point
		}
	if (dc - fabs(this->mr - sR) < -0.0001) // Motedakhel
		return -1; // No Common Point

	float x1 = 0.0;
	//float x2 = 0.0;
	float x1SubX2 = 0.0; // x1 + x2
	float x1MinusX2 = 0.0; // x1 - x2
	float t = pow(this->mr, 2) - pow(sR, 2);

	if (dc < fmaxf(this->mr, sR))
		{
			x1MinusX2 = dc;
			x1SubX2 = t / x1MinusX2;
			x1 = (x1SubX2 + x1MinusX2) / 2.0;
		}
	else
		{
			x1SubX2 = dc;
			x1MinusX2 = t / x1SubX2;
			x1 = (x1SubX2 + x1MinusX2) / 2.0;
		}

	float x = this->mr - x1;
	ci.r() = sqrt(pow(this->mr, 2) - pow(this->mr - x, 2));
	//ciR = ci.r();

	Vector3f v(sC.x() - this->mc.x(), sC.y() - this->mc.y(), sC.z() - this->mc.z());
	v = v.normalize();
	v *= (this->mr - x);
	ciC = Vector3f(v.x() + this->c().x(), v.y() + this->c().y(), v.z() + this->c().z());
	ci.c() = ciC;

	if (dr - dc < 0.00001) // Tangent
		return 0;

	ciA.x() = 1.0;
	ciA.y() = 0.0;
	ciA.z() = 0.0;
	if (fabs(v.y()) < 0.0001)
		ciA.z() = -(v.x() / v.z());
	else
		ciA.y() = -(v.x() / v.y());

	/*ciA.x() = fabs(v.x());
	ciA.y() = v.y();
	ciA.z() = -(ciA.x()*v.x() + ciA.y()*v.y()) / v.z();*/

	ciA = ciA.normalize();
	ci.a() = ciA;

	ciB = ciA * v;
	ciB = ciB.normalize();
	if (!leftRotate) // right rotate
		ciB *= (-1.0);
	ci.b() = ciB;

	//std::cout << "a = " << ciA.getX() << "	" << ciA.getY() << "	" << ciA.getZ() << "	" << std::endl;
	//std::cout << "b = " << ciB.getX() << "	" << ciB.getY() << "	" << ciB.getZ() << "	" << std::endl;
	return 1; // Circle
}
