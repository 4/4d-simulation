#include "BasicCMath.h"

float MIKE::normalizeAngle(float angle)
{
	while (angle < -M_PI) angle += 2 * M_PI;
	while (angle > M_PI) angle -= 2 * M_PI;
	return angle;
}

float MIKE::absoluteAngle(float angle)
{
	while (angle < 0) angle += 2 * 180;
	while (angle >= 180) angle -= 2 * 180;
	return angle;
}

float MIKE::average(float a1,float a2, float a3, float a4)
{
	return ((a1+a2+a3+a4)/4);
}
