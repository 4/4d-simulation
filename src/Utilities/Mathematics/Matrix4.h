// M.I.K.E. 3D Soccer Simulation team
// File: Matrix4.h

#ifndef _MIKE_MATRIX4_H
#define _MIKE_MATRIX4_H

#include <Types.h>
#include <Vector3.h>
#include <iostream>
#include <Geometry.h>
#include <array>

namespace MIKE {

    // Matrix4 class provides a 4x4 matrix
    class Matrix4 {
	
		float table[4][4];

    public:

		inline Matrix4(const Matrix4 &matrix); // Copy constructor

		// Construct matrix from 16 float values, Same as Matrix4::set
		inline Matrix4(float m00, float m01, float m02, float m03,
					  float m10, float m11, float m12, float m13,
					  float m20, float m21, float m22, float m23,
					  float m30, float m31, float m32, float m33);

		inline Matrix4(); // Default constructor
		inline ~Matrix4(); // Destructor

		// Sets up matrix from 16 float values
		inline void set(float m00, float m01, float m02, float m03,
						float m10, float m11, float m12, float m13,
						float m20, float m21, float m22, float m23,
						float m30, float m31, float m32, float m33);

		inline std::array<float, 16> getCArray() const; // Returns matrix's one dimension array

		inline void zero(); // Sets up a zero matrix
		inline void identity(); // Sets up the identity matrix

		inline void translate(const Vector3f &vector); // Create a translate matrix

		inline void rotateX(float theta); // Create a rotation x matrix by given theta in degree
		inline void rotateY(float theta); // Create a rotation y matrix by given theta in degree
		inline void rotateZ(float theta); // Create a rotation z matrix by given theta in degree

		inline void scale(const Vector3f &vector); // Create a scale matrix

		inline const float* operator[] (int index) const;
		inline float* operator[] (int index);

		inline const float operator() (const int w, const int h) const;
		inline float& operator() (const int w, const int h);

		inline void operator= (const Matrix4 &matrix);

		inline Matrix4 operator* (const Matrix4 &matrix) const;
		inline void operator*= (const Matrix4 &matrix);

		inline Matrix4 operator* (const float n) const;
		inline void operator*= (const float n);

		inline friend std::ostream& operator<< (std::ostream &output, const Matrix4 &matrix);

		inline Vector3f pos() const; // Returns pos vector of matrix
		Vector3f getEulerAngles() const; // Returns a vector of calculated euler angles

    }; // class Matrix4

    inline Matrix4::Matrix4(const Matrix4 &matrix) {
		for(Uint16 i = 0; i < 4; i ++)
			for(Uint16 j = 0; j < 4; j ++)
				table[i][j] = matrix[i][j];
    }

    inline Matrix4::Matrix4(float m00, float m01, float m02, float m03,
						  float m10, float m11, float m12, float m13,
						  float m20, float m21, float m22, float m23,
						  float m30, float m31, float m32, float m33) {

		set(m00, m01, m02, m03,
			m10, m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33);
    }

    inline Matrix4::Matrix4() {
		// Set up a zero matrix
		zero();
    }

    inline Matrix4::~Matrix4() {

    }

    inline void Matrix4::set(float m00, float m01, float m02, float m03,
							float m10, float m11, float m12, float m13,
							float m20, float m21, float m22, float m23,
							float m30, float m31, float m32, float m33) {

		table[0][0] = m00; table[0][1] = m01; table[0][2] = m02; table[0][3] = m03;
		table[1][0] = m10; table[1][1] = m11; table[1][2] = m12; table[1][3] = m13;
		table[2][0] = m20; table[2][1] = m21; table[2][2] = m22; table[2][3] = m23;
		table[3][0] = m30; table[3][1] = m31; table[3][2] = m32; table[3][3] = m33;
	}

	inline std::array<float, 16> Matrix4::getCArray() const {
		std::array<float, 16> result;
	
		for(int i = 0; i < 4; i ++)
			for(int j = 0; j < 4; j ++)
				result[i*4 + j] = table[i][j];

		return result;
	}

	inline void Matrix4::zero() {
		table[0][0] = 0; table[0][1] = 0; table[0][2] = 0; table[0][3] = 0;
		table[1][0] = 0; table[1][1] = 0; table[1][2] = 0; table[1][3] = 0;
		table[2][0] = 0; table[2][1] = 0; table[2][2] = 0; table[2][3] = 0;
		table[3][0] = 0; table[3][1] = 0; table[3][2] = 0; table[3][3] = 0;
	}

	inline void Matrix4::identity() {
		zero(); // Create zero matrix
		table[0][0] = 1;
		table[1][1] = 1;
		table[2][2] = 1;
		table[3][3] = 1;
	}

	inline void Matrix4::translate(const Vector3f &vector) {
		identity();
	
		table[3][0] = vector.x();	table[3][1] = vector.y();	table[3][2] = vector.z();
	}

	inline void Matrix4::rotateX(float theta) {
		identity();

		table[1][1] = gCos(theta);	table[1][2] = -1 * gSin(theta);
		table[2][1] = gSin(theta);	table[2][2] = gCos(theta);
	}

	inline void Matrix4::rotateY(float theta) {
		identity();

		table[0][0] = gCos(theta);	table[0][2] = gSin(theta);
		table[2][0] = -1 * gSin(theta); table[2][2] = gCos(theta);
	}

	inline void Matrix4::rotateZ(float theta) {
		identity();

		table[0][0] = gCos(theta);	table[0][1] = -1 * gSin(theta);
		table[1][0] = gSin(theta);	table[1][1] = gCos(theta);
	}

	inline void Matrix4::scale(const Vector3f &vector) {
		identity();

		table[0][0] = vector.x();
		table[1][1] = vector.y();
		table[2][2] = vector.z();
	}

	inline const float* Matrix4::operator[] (int index) const {
		return table[index];
	}

	inline float* Matrix4::operator[] (int index) {
		return table[index];
	}

	inline const float Matrix4::operator() (const int w, const int h) const {
		return table[w][h];
	}

	inline float& Matrix4::operator() (const int w, const int h) {
		return table[w][h];
	}

	inline void Matrix4::operator= (const Matrix4 &matrix) {
		for(Uint16 i = 0; i < 4; i ++)
			for(Uint16 j = 0; j < 4; j ++)
				table[i][j] = matrix[i][j];
	}

	inline Matrix4 Matrix4::operator* (const Matrix4 &matrix) const {
		Matrix4 result;

		for(Uint16 i = 0; i < 4; i ++)
			for(Uint16 j = 0; j < 4; j ++)
				for(Uint16 q = 0; q < 4; q ++)
					result[i][j] += table[i][q] * matrix[q][j];

		return result;
	}

	inline void Matrix4::operator*= (const Matrix4 &matrix) {
		(*this) = (*this) * matrix;
	}

	inline Matrix4 Matrix4::operator* (const float n) const {
		Matrix4 result;

		for(Uint16 i = 0; i < 4; i ++)
			for(Uint16 j = 0; j < 4; j ++)
				result[i][j] = table[i][j] * n;

		return result;
	}

	inline void Matrix4::operator*= (const float n) {
		(*this) = (*this) * n;
	}

	inline Vector3f Matrix4::pos() const {
		return Vector3f(table[3][0], table[3][1], table[3][2]);
	}

	inline std::ostream& operator<< (std::ostream &output, const Matrix4 &matrix) {
		for(Uint16 i = 0; i < 4; i ++) {
			for(Uint16 j = 0; j < 4; j ++)
				output << matrix[i][j] << " ";
			output << std::endl;
		}
		return output;
	}

} // namespace MIKE

#endif // _MIKE_MATRIX4_H

