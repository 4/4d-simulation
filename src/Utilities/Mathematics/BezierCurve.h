// M.I.K.E. 3D Soccer Simulation team
// File: BezierCurve.h

#ifndef _MIKE_BEZIER_CURVE_H
#define _MIKE_BEZIER_CURVE_H

#include "Vector3.h"
#include <deque>
#include <cmath>

namespace MIKE {

	class BezierCurve
	{
		std::deque<Vector3f> controlPoints;

	public:
		BezierCurve();
		~BezierCurve();

		void addPoint(float x, float y, float z);
		void addPoint(Vector3f p);

		Vector3f getPointGivenTime(float time); // time must be between 0, 1

		BezierCurve operator= (const BezierCurve& bc);
	};

}

#endif // _MIKE_BEZIER_CURVE_H
