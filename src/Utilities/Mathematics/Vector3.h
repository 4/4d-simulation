// M.I.K.E. 3D Soccer Simulation team
// File: Vector3.h

#ifndef _MIKE_VECTOR3_H
#define _MIKE_VECTOR3_H

#include <cmath>
#include <iostream>
#include <Geometry.h>
#include <Types.h>

namespace MIKE {

	// Vector3 provides a vector or a point in the 3d space
	template<typename T>
	class Vector3 {

		T mx, my, mz;

	public:

		inline Vector3();// Default constructor
		inline Vector3(const Vector3<T> &vector); // Copy constructor
		inline Vector3(T mx, T my, T mz, CoordSystem system = CARTESIAN); // Constructor
		inline Vector3(const Vector3<T> &vector, CoordSystem system); // Construct from a vector with different coord system
		inline ~Vector3(); // Destructor

		// Setter functions
		inline T& x();
		inline T& y();
		inline T& z();

		// Getter functions
		inline T x() const;
		inline T y() const;
		inline T z() const;

		inline T length() const; // Returns vector length
		inline Vector3<T> normalize() const; // Returns the normalized vector

		inline void operator= (const Vector3<T> &vector);

		inline Vector3<T> operator+ (const Vector3<T> &vector) const;
		inline void operator+= (const Vector3<T> &vector);

		inline Vector3<T> operator- (const Vector3<T> &vector) const;
		inline void operator-= (const Vector3<T> &vector);

		inline Vector3<T> operator* (const float &n) const;
		inline void operator*= (const float &n);

		inline Vector3<T> operator* (const double &n) const;
		inline void operator*= (const double &n);

		inline Vector3<T> operator* (const Vector3<T> &vector) const; // Cross product of two vector
		inline void operator*= (const Vector3<T> &vector);	// Cross product of two vector

		inline Vector3<T> operator/ (const float &n) const;
		inline void operator/= (const float &n);

		inline Vector3<T> operator/ (const double &n) const;
		inline void operator/= (const double &n);

		inline T dot(const Vector3<T> &vector) const; // Dot product of two vectors
		static inline T dot(const Vector3<T> &v1, const Vector3<T> &v2); // Dot product of two vectors
		
		static T angleBetween(const Vector3<T> &v1, const Vector3<T> &v2);
		static T angleBetweenAbout(const Vector3<T> &v1, const Vector3<T> &v2, const Vector3<T> &v3, bool isLeftPositive = true); // v1 & v2 about v3

		static Vector3<T> rotateAboutAAxis(const Vector3<T> &vector, const Vector3<T> &point, T degree); // A = Arbitrary, Rotate Point about Vector, Degree in Radian
		static Vector3<T> rotateAboutOAxis(const Vector3<T> &vector, const Vector3<T> &degrees); // O = Original, Degrees in Radian
		static Vector3<T> rotateLine(const Vector3<T> &vector, Vector3<T> &point1, Vector3<T> &point2, T degree); // Rotate points and return "point2 - point1", Degree in Radian

		template<class Type> inline friend std::ostream& operator<< (std::ostream &output, const Vector3<Type> &vector);
		template<class Type> inline friend std::istream& operator>> (std::istream &input, Vector3<Type> &vector);

	}; // class Vector3

	typedef Vector3<int> Vector3i;
	typedef Vector3<float> Vector3f;
	typedef Vector3<double> Vector3d;

	template<typename Type>
	inline Vector3<Type>::Vector3() : mx(0), my(0), mz(0) {

	}

	template<typename Type>
	inline Vector3<Type>::Vector3(const Vector3<Type> &vector) {
		mx = vector.mx;
		my = vector.my;
		mz = vector.mz;
	}

	template<typename Type>
	inline Vector3<Type>::Vector3(Type mx, Type my, Type mz, CoordSystem system) {
		switch (system) {
			case CARTESIAN:
				this->mx = mx;
				this->my = my;
				this->mz = mz;
				break;
			case POLAR:
				{
				    float r = mx * cos(mz);
					this->mx = r * cos(my);
					this->my = r * sin(my);
					this->mz = mx * sin(mz);
					break;
				}
			case DPOLAR:
				{	
				    float r = mx * gCos(mz);
					this->mx = r * gCos(my);
					this->my = r * gSin(my);
					this->mz = mx * gSin(mz);
					break;
				}
			}
	}

	template<typename Type>
	inline Vector3<Type>::Vector3(const Vector3<Type> &vector, CoordSystem system) {
		Type mx = vector.mx, my = vector.my, mz = vector.mz;

		switch (system) {
			case CARTESIAN:
				this->mx = mx;
				this->my = my;
				this->mz = mz;
				break;
			case POLAR:
				{
				    float r = mx * cos(mz);
					this->mx = r * cos(my);
					this->my = r * sin(my);
					this->mz = mx * sin(mz);
					break;
				}
			case DPOLAR:
				{	
				    float r = mx * gCos(mz);
					this->mx = r * gCos(my);
					this->my = r * gSin(my);
					this->mz = mx * gSin(mz);
					break;
				}
			}

	}

	template<typename Type>
	inline Vector3<Type>::~Vector3() {

	}

	template<typename Type>
	inline Type& Vector3<Type>::x() {
		return mx;
	}

	template<typename Type>
	inline Type& Vector3<Type>::y() {
		return my;
	}

	template<typename Type>
	inline Type& Vector3<Type>::z() {
		return mz;
	}

	template<typename Type>
	inline Type Vector3<Type>::x() const {
		return mx;
	}

	template<typename Type>
	inline Type Vector3<Type>::y() const {
		return my;
	}

	template<typename Type>
	inline Type Vector3<Type>::z() const {
		return mz;
	}

	template<typename Type>
	inline Type Vector3<Type>::length() const {
		return sqrt(mx*mx + my*my + mz*mz);
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::normalize() const {
		if(!length()) // In case of zero vector
			return Vector3<Type>();

		return Vector3<Type>(mx/length(), my/length(), mz/length());
	}

	template<typename Type>
	inline void Vector3<Type>::operator= (const Vector3<Type> &vector) {
		mx = vector.x();
		my = vector.y();
		mz = vector.z();
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator+ (const Vector3<Type> &vector) const {
		return Vector3<Type>(mx + vector.x(), my + vector.y(), mz + vector.z());
	}

	template<typename Type>
	inline void Vector3<Type>::operator+= (const Vector3<Type> &vector) {
		mx += vector.x();
		my += vector.y();
		mz += vector.z();
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator- (const Vector3<Type> &vector) const {
		return Vector3<Type>(mx - vector.x(), my - vector.y(), mz - vector.z());
	}

	template<typename Type>
	inline void Vector3<Type>::operator-= (const Vector3<Type> &vector) {
		mx -= vector.x();
		my -= vector.y();
		mz -= vector.z();
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator* (const float &n) const {
		return Vector3<Type>(mx*n, my*n, mz*n);
	}

	template<typename Type>
	inline void Vector3<Type>::operator*= (const float &n) {
		mx *= n;
		my *= n;
		mz *= n;
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator* (const double &n) const {
		return Vector3<Type>(mx*n, my*n, mz*n);
	}

	template<typename Type>
	inline void Vector3<Type>::operator*= (const double &n) {
		mx *= n;
		my *= n;
		mz *= n;
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator* (const Vector3<Type> &vector) const {
		return Vector3<Type>((my * vector.z()) - (mz * vector.y()),
							 (mz * vector.x()) - (mx * vector.z()),
							 (mx * vector.y()) - (my * vector.x()));
	}

	template<typename Type>
	inline void Vector3<Type>::operator*= (const Vector3<Type> &vector) {
		(*this) = (*this) * vector;
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator/ (const float &n) const {
		return Vector3<Type>(mx/n, my/n, mz/n);
	}

	template<typename Type>
	inline void Vector3<Type>::operator/= (const float &n) {
		mx /= n;
		my /= n;
		mz /= n;
	}

	template<typename Type>
	inline Vector3<Type> Vector3<Type>::operator/ (const double &n) const {
		return Vector3<Type>(mx/n, my/n, mz/n);
	}

	template<typename Type>
	inline void Vector3<Type>::operator/= (const double &n) {
		mx /= n;
		my /= n;
		mz /= n;
	}

	template<typename Type>
	inline Type Vector3<Type>::dot(const Vector3<Type> &vector) const {
		return mx*vector.x() + my*vector.y() + mz*vector.z();
	}

	template<typename Type>
	inline Type Vector3<Type>::dot(const Vector3<Type> &v1, const Vector3<Type> &v2) {
		return v1.x()*v2.x() + v1.y()*v2.y() + v1.z()*v2.z();
	}

	template<typename Type>
    Type Vector3<Type>::angleBetween(const Vector3<Type> &v1, const Vector3<Type> &v2) {
	    Type eps = 0.000001f;
		if (v1.length() < eps || v2.length() < eps)
			return 0.0;
	    Type theta = Vector3<Type>::dot(v1, v2) / (v1.length() * v2.length());
		if (theta < -1.0f)
			theta = -1.0f;
		if (theta > 1.0f)
			theta = 1.0f;
		return acos(theta);
	}
	
	template<typename Type>
    Type Vector3<Type>::angleBetweenAbout(const Vector3<Type> &v1, const Vector3<Type> &v2, const Vector3<Type> &v3,
											bool isLeftPositive) {
		/*Type t1 = (v3.x()*v1.x() + v3.y()*v1.y() + v3.z()*v1.z()) / (pow(v3.x(), 2) + pow(v3.y(), 2) + pow(v3.z(), 2));
		Vector3<Type> v1p(v1.x()-v3.x()*t1,
						  v1.y()-v3.y()*t1,
						  v1.z()-v3.z()*t1);

		Type t2 = (v3.x()*v2.x() + v3.y()*v2.y() + v3.z()*v2.z()) / (pow(v3.x(), 2) + pow(v3.y(), 2) + pow(v3.z(), 2));
		Vector3<Type> v2p(v2.x()-v3.x()*t2,
						  v2.y()-v3.y()*t2,
						  v2.z()-v3.z()*t2);

		Type degree = Vector3<Type>::angleBetween(v1p, v2p);
		Vector3<Type> Plate1N = v3 * v1;*/

		Vector3<Type> Plate1N = v3 * v1;
		Vector3<Type> Plate2N = v3 * v2;
		Type degree = Vector3<Type>::angleBetween(Plate1N, Plate2N);
		
		if (angleBetween(Plate1N, v2) < (M_PI / 2.0f) && isLeftPositive)
			return fabs(degree);
		else
			return -fabs(degree);
	}

	template<typename Type>
	Vector3<Type> Vector3<Type>::rotateAboutAAxis(const Vector3<Type> &vector, const Vector3<Type> &point, Type degree) {
		Vector3<Type> vec = vector.normalize();
	    Type u = vec.x();
	    Type v = vec.y();
	    Type w = vec.z();
	    Type x = point.x();
	    Type y = point.y();
	    Type z = point.z();
		Vector3<Type> fP;
		fP.x() = u*(u*x + v*y + w*z)*(1 - cos(degree)) + x*cos(degree) + (-w*y + v*z)*sin(degree);
		fP.y() = v*(u*x + v*y + w*z)*(1 - cos(degree)) + y*cos(degree) + (+w*x - u*z)*sin(degree);
		fP.z() = w*(u*x + v*y + w*z)*(1 - cos(degree)) + z*cos(degree) + (-v*x + u*y)*sin(degree);

		return fP;
	}

	template<typename Type>
    Vector3<Type> Vector3<Type>::rotateAboutOAxis(const Vector3<Type> &vector, const Vector3<Type> &degrees) {
		Vector3<Type> v = vector;
		Vector3<Type> xAxis(1, 0, 0);
		Vector3<Type> yAxis(0, 1, 0);
		Vector3<Type> zAxis(0, 0, 1);
		v = rotateAboutAAxis(xAxis, v, degrees.x());
		v = rotateAboutAAxis(yAxis, v, degrees.y());
		v = rotateAboutAAxis(zAxis, v, degrees.z());
		return v;
	}

	template<typename Type>
	Vector3<Type> Vector3<Type>::rotateLine(const Vector3<Type> &v, Vector3<Type> &p1, Vector3<Type> &p2, Type degree) {
		p1 = rotateAboutAAxis(v, p1, degree);
		p2 = rotateAboutAAxis(v, p2, degree);
		return Vector3<Type>(p2 - p1);
	}

	template<typename Type>
	inline std::ostream& operator<< (std::ostream &output, const Vector3<Type> &vector) {
		output << vector.x() << " " << vector.y() << " " << vector.z();
		return output;
	}
	
	template<typename Type>
	inline std::istream& operator>> (std::istream &input, Vector3<Type> &vector) {
		input >> std::skipws >> vector.x() >> vector.y() >> vector.z();
		return input;
	}

} // namespace MIKE

#endif // _MIKE_VECTOR3_H

