// M.I.K.E. 3D Soccer Simulation team
// File: Vector2.h

#ifndef _MIKE_VECTOR2_H
#define _MIKE_VECTOR2_H

#include <cmath>
#include <iostream>

namespace MIKE {

	// Vector2 provides a vector or a point in the 2d space
	template<typename T>
	class Vector2 {

		T mx, my;

	public:

		inline Vector2();// Default constructor
		inline Vector2(const Vector2<T> &vector);
		inline Vector2(T mx, T my); // Constructor
		inline ~Vector2(); // Destructor

		// Setter functions
		inline T& x();
		inline T& y();

		// Getter functions
		inline T x() const;
		inline T y() const;

		inline float length() const; // Returns vector length
		inline Vector2<T> normalize() const; // Returns the normalized vector

		inline void operator= (const Vector2<T> &vector);

		inline Vector2<T> operator+ (const Vector2<T> &vector) const;
		inline void operator+= (const Vector2<T> &vector);

		inline Vector2<T> operator- (const Vector2<T> &vector) const;
		inline void operator-= (const Vector2<T> &vector);

		inline Vector2<T> operator* (const float &n) const;
		inline void operator*= (const float &n);

		inline Vector2<T> operator/ (const float &n) const;
		inline void operator/= (const float &n);

		inline float dot(const Vector2<T> &vector) const; // Dot product of two vectors

		template<class Type> inline friend std::ostream& operator<< (std::ostream &output, const Vector2<Type> &vector);
		template<class Type> inline friend std::istream& operator>> (std::istream &input, Vector2<Type> &vector);

	}; // class Vector2

	typedef Vector2<int> Vector2i;
	typedef Vector2<float> Vector2f;

	template<typename Type>
	inline Vector2<Type>::Vector2() : mx(0), my(0) {

	}

	template<typename Type>
	inline Vector2<Type>::Vector2(const Vector2<Type> &vector) {
		mx = vector.mx;
		my = vector.my;
	}

	template<typename Type>
	inline Vector2<Type>::Vector2(Type mx, Type my) : mx(mx), my(my) {

	}

	template<typename Type>
	inline Vector2<Type>::~Vector2() {

	}

	template<typename Type>
	inline Type& Vector2<Type>::x() {
		return mx;
	}

	template<typename Type>
	inline Type& Vector2<Type>::y() {
		return my;
	}

	template<typename Type>
	inline Type Vector2<Type>::x() const {
		return mx;
	}

	template<typename Type>
	inline Type Vector2<Type>::y() const {
		return my;
	}

	template<typename Type>
	inline float Vector2<Type>::length() const {
		return sqrt(mx*mx + my*my);
	}

	template<typename Type>
	inline Vector2<Type> Vector2<Type>::normalize() const {
		if(!length()) // In case of zero vector
			return Vector2<Type>();

		return Vector2<Type>(mx/length(), my/length());
	}

	template<typename Type>
	inline void Vector2<Type>::operator= (const Vector2<Type> &vector) {
		mx = vector.x();
		my = vector.y();
	}

	template<typename Type>
	inline Vector2<Type> Vector2<Type>::operator+ (const Vector2<Type> &vector) const {
		return Vector2<Type>(mx + vector.x(), my + vector.y());
	}

	template<typename Type>
	inline void Vector2<Type>::operator+= (const Vector2<Type> &vector) {
		mx += vector.x();
		my += vector.y();
	}

	template<typename Type>
	inline Vector2<Type> Vector2<Type>::operator- (const Vector2<Type> &vector) const {
		return Vector2<Type>(mx - vector.x(), my - vector.y());
	}

	template<typename Type>
	inline void Vector2<Type>::operator-= (const Vector2<Type> &vector) {
		mx -= vector.x();
		my -= vector.y();
	}

	template<typename Type>
	inline Vector2<Type> Vector2<Type>::operator* (const float &n) const {
		return Vector2<Type>(mx*n, my*n);
	}

	template<typename Type>
	inline void Vector2<Type>::operator*= (const float &n) {
		mx *= n;
		my *= n;
	}

	template<typename Type>
	inline Vector2<Type> Vector2<Type>::operator/ (const float &n) const {
		return Vector2<Type>(mx/n, my/n);
	}

	template<typename Type>
	inline void Vector2<Type>::operator/= (const float &n) {
		mx /= n;
		my /= n;
	}

	template<typename Type>
	inline float Vector2<Type>::dot(const Vector2<Type> &vector) const {
		return mx*vector.x() + my*vector.y();
	}

	template<typename Type>
	inline std::ostream& operator<< (std::ostream &output, const Vector2<Type> &vector) {
		output << vector.x() << " " << vector.y();
		return output;
	}

	template<typename Type>
	inline std::istream& operator>> (std::istream &input, Vector2<Type> &vector) {
		input >> std::skipws >> vector.x() >> vector.y();
		return input;
	}

} // namespace MIKE

#endif // _MIKE_VECTOR2_H

