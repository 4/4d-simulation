// M.I.K.E. 3D Soccer Simulation team
// File: ConfigManager.h

#ifndef _MIKE_CONFIGMANAGER_H
#define _MIKE_CONFIGMANAGER_H

#include <Singleton.h>
#include <boost/property_tree/ptree.hpp>

namespace MIKE {

	class ConfigManager : public Singleton<ConfigManager> {

		boost::property_tree::ptree ptree;

	public:

		ConfigManager(); // Default constructor
		~ConfigManager(); // Destructor

		void initialize(int argc, char *argv[]); // Initialize config files

		template<class Type>
		inline const Type get(const std::string &name) const;

		const boost::property_tree::ptree& getConfigTree() const;

	private:

		void setByFiles();
		void setByArguments(int argc, char *argv[]);

	}; // class ConfigManager

	template<class Type>
	inline const Type ConfigManager::get(const std::string &name) const {
		return ptree.get<Type>(name);
	}

} // namespace MIKE

#endif // _MIKE_CONFIGMANAGER_H

