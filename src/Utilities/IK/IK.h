// M.I.K.E. 3D Soccer Simulation team
// File: IK.h

#ifndef _MIKE_IK_H
#define _MIKE_IK_H

#include <Types.h>
#include <Body.h>
#include <Vector3.h>
#include <Circle3D.h>
#include <Sphere.h>
#include <cmath>

namespace MIKE {
	class IK {
		bool mLeft;
		bool toeMode;
		bool getShankPos(Vector3f& shankPos, float zRot, const Vector3f& anklePos); // zRot in Radian
		bool simpleLIK(const Vector3f& anklePos, Vector3f& ljV); //ljV.x() = lj3, ljV.y() = lj4
	    void getAnkleJointsVector(const Vector3f& hipJointsAngle, float shankJointAngle, Vector3f& lj5V, Vector3f& lj6V);

		bool getElbowPos(Vector3f& elbowPos, float zRot, const Vector3f& fingerPos); // zRot in Radian
		bool simpleAIK(const Vector3f& fingerPos, float zRot, Vector3f& ajV);
	public:
		IK();
		~IK();

		int LIK(Body& body, const Vector3f& point, const Vector3f rotation, bool left, bool legParallel = true, IKMod ikm = IK_Ankle); // Leg IK, Fill body, rotation in Radian, if legParallel = true => leg is parallel with Torso xoy plate; and if false => leg is parallel with ground, return => -2 = mode isn't exist, -1 = Not possible, 0 = Too big rotation, 1 = Every things ok B-)

		int AIK(Body& body, const Vector3f& point, float zRot, bool left); // Arm IK, rotation in Radian, -1 = Not possible, 0 = Too big rotation, 1 = Every things ok B-)

	}; // end class IK
} // end namespace MIKE

#endif // _MIKE_IK_H
