// M.I.K.E. 3D Soccer Simulation team
// File: IK.cpp

#include "IK.h"
#include <Geometry.h>

#include <iostream>
using namespace std;

using namespace MIKE;

IK::IK() {
	mLeft = true;
	toeMode = false;
}

IK::~IK() {
}

bool IK::getShankPos(Vector3f& shankPos, float zRot, const Vector3f& anklePos) {
	float hip2ShankLength = 0.120104121;
	float shank2AnkleLength = 0.100;

	Vector3f hipPos = Vector3f(0.0, 0.0, 0.0);
	Sphere hipS(hipPos, hip2ShankLength);

	Sphere ankleS(anklePos, shank2AnkleLength);

	Circle3D ci;
	int state = hipS.getCommonPoints(ankleS, ci, mLeft);
	if (state == -2 || state == -1)
		return false;
	else if (state == 0)
		shankPos = ci.c();
	else if (state == 1)
		shankPos = ci.getPoint(zRot);
	return true;
}

bool IK::simpleLIK(const Vector3f& anklePos, Vector3f& ljV) {
	Vector3f shankPos;
	bool state = IK::getShankPos(shankPos, 0.0, anklePos);

	if (state == -2 || state == -1)
		return false;
	//else if (state == 0 || state == 1)
	Vector3f hip2Shank(0.005, 0.0, -0.120); // TODO : add from wm->getBody()
	Vector3f shank2Ankle(0.0, 0.0, -0.100); // TODO : add from wm->getBody()

	Vector3f lj3V(0, 1, 0);
	ljV.x() = -Vector3f::angleBetweenAbout(hip2Shank, shankPos, lj3V, true);

	Vector3f tmpAnklePos = anklePos;
	tmpAnklePos -= shankPos;

	Vector3f lj4V(0, 1, 0);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj3V, shank2Ankle, -ljV.x());
	ljV.y() = Vector3f::angleBetweenAbout(shank2Ankle, tmpAnklePos, lj4V, false);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj4V, shank2Ankle, -ljV.y());

	return true;
}

void IK::getAnkleJointsVector(const Vector3f& hipJointsAngle, float shankJointAngle, Vector3f& lj5V, Vector3f& lj6V) {
	// Set Relative Vectors
	Vector3f hip2Shank = Vector3f(0.005, 0.0, -0.120);
	Vector3f shank2Ankle = Vector3f(0.0, 0.0, -0.100);

	// Prepare joints' Vectors
	Vector3f lj1V(0, 1, (mLeft ? -1 : 1)); 	// lj1 Vector
	Vector3f lj2V(1, 0, 0); 				// lj2 Vector
	Vector3f lj3V(0, 1, 0); 				// lj3 Vector
	Vector3f lj4V(0, 1, 0); 				// lj4 Vector

	// Set joints' Angles
	float lj1A = hipJointsAngle.x(); // lj1 Angle
	float lj2A = hipJointsAngle.y(); // lj2 Angle
	float lj3A = -hipJointsAngle.z(); // lj3 Angle
	float lj4A = -shankJointAngle; // lj4 Angle

	// Set lj2V
	Vector3f lj2P1(-1, 0, 0);
	Vector3f lj2P2(1, 0, 0);
	lj2V = Vector3f::rotateLine(lj1V, lj2P1, lj2P2, lj1A);

	// Set lj3V
	Vector3f lj3P1(0, -1, 0);
	Vector3f lj3P2(0, 1, 0);
	lj3V = Vector3f::rotateLine(lj1V, lj3P1, lj3P2, lj1A);
	lj3V = Vector3f::rotateLine(lj2V, lj3P1, lj3P2, lj2A);

	// Set lj4V
	Vector3f lj4P1 = hip2Shank + Vector3f(0, -1, 0);
	Vector3f lj4P2 = hip2Shank + Vector3f(0, 1, 0);
	lj4V = Vector3f::rotateLine(lj1V, lj4P1, lj4P2, lj1A);
	lj4V = Vector3f::rotateLine(lj2V, lj4P1, lj4P2, lj2A);
	lj4V = Vector3f::rotateLine(lj3V, lj4P1, lj4P2, lj3A);

	// Rotate hip2Shank about joint 1, 2, 3
	hip2Shank = Vector3f::rotateAboutAAxis(lj1V, hip2Shank, lj1A);
	hip2Shank = Vector3f::rotateAboutAAxis(lj2V, hip2Shank, lj2A);
	hip2Shank = Vector3f::rotateAboutAAxis(lj3V, hip2Shank, lj3A);

	// Rotate shank2Ankle about joint 1, 2, 3
	shank2Ankle = Vector3f::rotateAboutAAxis(lj1V, shank2Ankle, lj1A);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj2V, shank2Ankle, lj2A);
	shank2Ankle = Vector3f::rotateAboutAAxis(lj3V, shank2Ankle, lj3A);

	// Set lj5V
	Vector3f lj5P1 = shank2Ankle + lj4P1 - hip2Shank;
	Vector3f lj5P2 = shank2Ankle + lj4P2 - hip2Shank;
    lj5V = Vector3f::rotateLine(lj4V, lj5P1, lj5P2, lj4A);

	// Rotate shank2Ankle about joint 4
	shank2Ankle = Vector3f::rotateAboutAAxis(lj4V, shank2Ankle, lj4A);

	// Set lj6V
    lj6V = shank2Ankle * lj5V;
	lj6V = lj6V.normalize();
}

int IK::LIK(Body& body, const Vector3f& point, const Vector3f rotation, bool left, bool legParallel, IKMod ikm) {
	mLeft = left;

	if (ikm == IK_Toe) {
		Body tmpBody;
		toeMode = true;
		Vector3f firstPoint = (point.normalize() * 0.33);
		int status = IK::LIK(tmpBody, firstPoint, rotation, left, legParallel, IK_Ankle);
		if (status < 0)
			return status;
		tmpBody.LFK(left);
		Vector3f toe2ankle = tmpBody.getTorso()->findJoint(left ? "llj5" : "rlj5")->getRelativePosition() - 
								tmpBody.getTorso()->findJoint(left ? "llj6" : "rlj6")->getRelativePosition();
		Vector3f newPoint = point + toe2ankle;
		toeMode = false;
		status = IK::LIK(body, newPoint, rotation, left, legParallel, IK_Ankle);
		return status;
	}
	else if (ikm == IK_Ankle) {
		Vector3f hipPos = (mLeft ? body.getTorsoToLeftHip() : body.getTorsoToRightHip());
		Vector3f anklePos;
		anklePos = point - hipPos;
		Vector3f rot = rotation;
		rot.z() *= -1.0;

		float hip2AnkleLength = anklePos.length();
		Vector3f shankPos = body.getHipToShank();
		Vector3f tmpAnklePos(0.0, 0.0, -hip2AnkleLength);
		Vector3f ljV;
		bool state = simpleLIK(tmpAnklePos, ljV);
		if (!toeMode && !state)
			return -1;

		Vector3f hipJointsAngle;
		hipJointsAngle.x() = rot.z(); // lj1

		Vector3f lj1V(0, 1, (mLeft ? -1 : 1));
		tmpAnklePos = Vector3f::rotateAboutAAxis(lj1V, tmpAnklePos, hipJointsAngle.x());
		shankPos = Vector3f::rotateAboutAAxis(lj1V, shankPos, hipJointsAngle.x());

		Vector3f lj2P1(-1, 0, 0);
		Vector3f lj2P2(1, 0, 0);
		Vector3f lj2V;
		lj2V = Vector3f::rotateLine(lj1V, lj2P1, lj2P2, hipJointsAngle.x());
		hipJointsAngle.y() = Vector3f::angleBetweenAbout(tmpAnklePos, anklePos, lj2V, true); // lj2
		tmpAnklePos = Vector3f::rotateAboutAAxis(lj2V, tmpAnklePos, hipJointsAngle.y());
		shankPos = Vector3f::rotateAboutAAxis(lj2V, shankPos, hipJointsAngle.y());

		Vector3f lj3P1(0, -1, 0);
		Vector3f lj3P2(0, 1, 0);
		Vector3f lj3V;
		lj3V = Vector3f::rotateLine(lj1V, lj3P1, lj3P2, hipJointsAngle.x());
		lj3V = Vector3f::rotateLine(lj2V, lj3P1, lj3P2, hipJointsAngle.y());

		hipJointsAngle.z() = -Vector3f::angleBetweenAbout(tmpAnklePos, anklePos, lj3V, true) + ljV.x(); // lj3
		shankPos = Vector3f::rotateAboutAAxis(lj3V, shankPos, -hipJointsAngle.z());

		float shankJointAngle = ljV.y(); // lj4

		Vector3f shankToAnkle = anklePos - shankPos;
		Vector3f ankleJointsAngle;
		Vector3f footParallel;
		if (legParallel) {
			footParallel = Vector3f(0.0, 0.0, -1.0);
		}
		else {
			// TODO : add foot parallel with ground after having singleton wm :|
			footParallel = Vector3f(0.0, 0.0, -1.0);
			Vector3f degrees = Vector3f(0.0, 0.0, 0.0);//WorldModel::instance()->getBody()->getTorsoRotationsVector();
			degrees.z() = 0.0f;
			footParallel = Vector3f::rotateAboutOAxis(footParallel, degrees);
		}

		Vector3f lj5V;
		Vector3f lj6V;
		getAnkleJointsVector(hipJointsAngle, shankJointAngle, lj5V, lj6V);
		ankleJointsAngle.x() = -Vector3f::angleBetweenAbout(shankToAnkle, footParallel, lj5V, true) + rot.x(); // lj5
		shankToAnkle = Vector3f::rotateAboutAAxis(lj5V, shankToAnkle, -ankleJointsAngle.x());
		lj6V = Vector3f::rotateAboutAAxis(lj5V, lj6V, -ankleJointsAngle.x());

		ankleJointsAngle.y() = Vector3f::angleBetweenAbout(shankToAnkle, footParallel, lj6V, true) + rot.y(); // lj6

		body.findJoint(mLeft ? "llj1" : "rlj1")->setJointValue(RTD * hipJointsAngle.x());
		body.findJoint(mLeft ? "llj2" : "rlj2")->setJointValue(RTD * hipJointsAngle.y());
		body.findJoint(mLeft ? "llj3" : "rlj3")->setJointValue(RTD * hipJointsAngle.z());
		body.findJoint(mLeft ? "llj4" : "rlj4")->setJointValue(RTD * shankJointAngle);
		body.findJoint(mLeft ? "llj5" : "rlj5")->setJointValue(RTD * ankleJointsAngle.x());
		body.findJoint(mLeft ? "llj6" : "rlj6")->setJointValue(RTD * ankleJointsAngle.y());

		int startID = (mLeft ? JID_LLEG_1 : JID_RLEG_1);
		int endID = (mLeft ? JID_LLEG_6 : JID_RLEG_6);
		for (int i = startID; i <= endID; i++) {
			float max = body.findJoint(i)->getMaxJointValue();
			float min = body.findJoint(i)->getMinJointValue();
			if (body.findJoint(i)->getJointValue() > max || body.findJoint(i)->getJointValue() < min)
				return 0;
		}

		return 1;
	}
	else {
		return -2;
	}
}

bool IK::getElbowPos(Vector3f& elbowPos, float zRot, const Vector3f& fingerPos) {
	const float shoulder2ElbowLength = 0.090448881;
	const float finger2ElbowLength = 0.105;

	Vector3f shoulderPos = Vector3f(0.0, 0.0, 0.0);
	Sphere shoulderS(shoulderPos, shoulder2ElbowLength);

	Sphere fingerS(fingerPos, finger2ElbowLength);

	Circle3D ci;
	int state = shoulderS.getCommonPoints(fingerS, ci, mLeft);
	if (state == -2 || state == -1)
		return false;
	else if (state == 0)
		elbowPos = ci.c();
	else if (state == 1)
		elbowPos = ci.getPoint(M_PI - zRot);
	return true;
}

bool IK::simpleAIK(const Vector3f& fingerPos, float zRot, Vector3f& ajV) {
	Vector3f elbowPos;
	int state = IK::getElbowPos(elbowPos, zRot, fingerPos);
	zRot = fabs(M_PI2 - zRot) * (mLeft ? -1.0 : 1.0);

	if (state == -2 || state == -1)
		return false;
	//else if (state == 0 || state == 1)
	// TODO take form wm->getBody()
	const Vector3f shoulderPos = mLeft ? Vector3f(0.0, 0.098, 0.075) : Vector3f(0.0, -0.098, 0.075);
	const Vector3f shoulder2Elbow(0.09, 0.0, 0.009);
	const Vector3f elbow2Finger(0.105, 0.0, 0.0);
	const Vector3f torso2Finger = mLeft ? Vector3f(0.195, 0.108, 0.084) : Vector3f(0.195, -0.108, 0.084);
	// End of TODO
	Vector3f fFingerPos = fingerPos;// + shoulderPos; // Final Finger Pos
	Vector3f tmpS2E = shoulder2Elbow;
	Vector3f tmpT2F = torso2Finger;
	Vector3f tmpE2F = elbow2Finger;

	Vector3f aj1V(0, 1, 0);
	ajV.x() = -Vector3f::angleBetweenAbout(tmpS2E, elbowPos, aj1V, true);
	tmpS2E = Vector3f::rotateAboutAAxis(aj1V, tmpS2E, -ajV.x());

	Vector3f aj2P1(0, 0, -1);
	Vector3f aj2P2(0, 0, +1);
	Vector3f aj2V;
	aj2V = Vector3f::rotateLine(aj1V, aj2P1, aj2P2, -ajV.x());
	ajV.y() = Vector3f::angleBetweenAbout(tmpS2E, elbowPos, aj2V, true);
	tmpS2E = Vector3f::rotateAboutAAxis(aj2V, tmpS2E, ajV.y());

	Vector3f aj3P1(-1, 0, 0);
	Vector3f aj3P2(+1, 0, 0);
	Vector3f aj3V;
	aj3V = Vector3f::rotateLine(aj1V, aj3P1, aj3P2, -ajV.x());
	aj3V = Vector3f::rotateLine(aj2V, aj3P1, aj3P2, ajV.y());
	float aj3A = zRot;
	//tmpS2E = Vector3f::rotateAboutAAxis(aj3V, tmpS2E, aj3A);

	Vector3f aj4P1 = shoulder2Elbow + Vector3f(0, 0, -1);
	Vector3f aj4P2 = shoulder2Elbow + Vector3f(0, 0, +1);
	Vector3f aj4V;
	aj4V = Vector3f::rotateLine(aj1V, aj4P1, aj4P2, -ajV.x());
	aj4V = Vector3f::rotateLine(aj2V, aj4P1, aj4P2, ajV.y());
	aj4V = Vector3f::rotateLine(aj3V, aj4P1, aj4P2, aj3A);

	tmpE2F = Vector3f::rotateAboutAAxis(aj1V, tmpE2F, -ajV.x());
	tmpE2F = Vector3f::rotateAboutAAxis(aj2V, tmpE2F, ajV.y());
	tmpE2F = Vector3f::rotateAboutAAxis(aj3V, tmpE2F, aj3A);

	fFingerPos -= elbowPos;
	ajV.z() = Vector3f::angleBetweenAbout(tmpE2F, fFingerPos, aj4V, true);
	tmpE2F = Vector3f::rotateAboutAAxis(aj4V, tmpE2F, ajV.z());

	return true;
}

int IK::AIK(Body& body, const Vector3f& point, float zRot, bool left) {
	mLeft = left;
	Vector3f shoulderPos = (mLeft ? body.getTorsoToLeftShoulder() : body.getTorsoToRightShoulder());
	Vector3f fingerPos = point - shoulderPos;

	Vector3f ajV;
	Vector3f tmpFingerPos(0, 0, -fingerPos.length());
	bool state = simpleAIK(tmpFingerPos, zRot, ajV);
	if (!state)
		return -1;

	Vector3f aj1V(0, 1, 0);
	Vector3f shoulderJointsAngle;
	shoulderJointsAngle.x() = -Vector3f::angleBetweenAbout(tmpFingerPos, fingerPos, aj1V, false) + ajV.x();
	tmpFingerPos = Vector3f::rotateAboutAAxis(aj1V, tmpFingerPos, -shoulderJointsAngle.x());

	Vector3f aj2P1(0, 0, -1);
	Vector3f aj2P2(0, 0, +1);
	Vector3f aj2V = Vector3f::rotateLine(aj1V, aj2P1, aj2P2, -shoulderJointsAngle.x());
	shoulderJointsAngle.y() = Vector3f::angleBetweenAbout(tmpFingerPos, fingerPos, aj2V, true) + ajV.y();
	//tmpFingerPos = Vector3f::rotateAboutAAxis(aj2V, tmpFingerPos, shoulderJointsAngle.y());

	Vector3f elbowJointsAngle;
	elbowJointsAngle.x() = fabs(M_PI2 - zRot) * (mLeft ? -1.0 : 1.0);
	elbowJointsAngle.y() = ajV.z();

	body.findJoint(mLeft ? "laj1" : "raj1")->setJointValue(RTD * shoulderJointsAngle.x());
	body.findJoint(mLeft ? "laj2" : "raj2")->setJointValue(RTD * shoulderJointsAngle.y());
	body.findJoint(mLeft ? "laj3" : "raj3")->setJointValue(RTD * elbowJointsAngle.x());
	body.findJoint(mLeft ? "laj4" : "raj4")->setJointValue(RTD * elbowJointsAngle.y());

	

	int startID = (mLeft ? JID_LARM_1 : JID_RARM_1);
	int endID = (mLeft ? JID_LARM_4 : JID_RARM_4);
	/*cout << endl << endl;
	for (int i = startID; i <= endID; i++)
	cout << FindJointName[i] << " = " << body.findJoint(i)->getJointValue() << endl;*/
	for (int i = startID; i <= endID; i++) {
		float max = body.findJoint(i)->getMaxJointValue();
		float min = body.findJoint(i)->getMinJointValue();
		if (body.findJoint(i)->getJointValue() > max || body.findJoint(i)->getJointValue() < min)
			return 0;
	}

	return 1;
}
