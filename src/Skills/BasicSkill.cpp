// M.I.K.E. 3D Soccer Simulation Team
// File: BasicSkill.cpp

#include <BasicSkill.h>
#include <BodyCommand.h>

using namespace std;
using namespace MIKE;

BasicSkill::BasicSkill(const string &name) : name(name) {

}

BasicSkill::~BasicSkill() {

}

string BasicSkill::getName() const {
	return name;
}

shared_ptr<BodyCommand> BasicSkill::getBodyCommand() const {
	return mBodyCommand;
}

