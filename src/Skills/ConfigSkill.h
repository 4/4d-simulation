// M.I.K.E. 3D Soccer Simulation Team
// File: ConfigSkill.h

#ifndef _MIKE_CONFIGSKILL_H
#define _MIKE_CONFIGSKILL_H

#include <vector>

#include <ConfigStep.h>
#include <BasicSkill.h>

namespace MIKE
{
	class ConfigSkill : public BasicSkill
	{
		float mPrepareStartTime;
		float mExecuteStartTime;
		float mStopStartTime;

		int mPrepareStepNumber;
		int mExecuteStepNumber;
		int mStopStepNumber;

		bool mIsRepetetive;
		std::vector<ConfigStep> mPrepareSteps;
		std::vector<ConfigStep> mExecuteSteps;
		std::vector<ConfigStep> mStopSteps;

		bool mPrepareDone;
		bool mStopDone;

		std::shared_ptr<BodyCommand> mPreviusStepBody;

	public:
		ConfigSkill(const std::string&);
		~ConfigSkill();

		void reset();
		void prepare();
		void execute();
		void stop();

		bool isPrepareDone() const;
		bool isStopDone() const;
	}; // class ConfigSkill
} // namespace MIKE

#endif //_MIKE_CONFIGSTEP_H
