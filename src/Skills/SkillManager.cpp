// M.I.K.E. 3D Soccer Simulation Team
// File: SkillManager.cpp

#include <SkillManager.h>
#include <BodyCommand.h>
#include <BasicSkill.h>

using namespace std;
using namespace MIKE;

SkillManager::SkillManager() {

}

SkillManager::~SkillManager() {

}

shared_ptr<BodyCommand> SkillManager::getBodyCommand() const {
	return mBodyCommand;
}

void SkillManager::update()
{
	if (!mSkillQueue.front()->isPrepareDone())
		mSkillQueue.front()->prepare();
	else
	{
		if (mSkillQueue.size() == 1)
			mSkillQueue.front()->execute();
		else
			mSkillQueue.front()->stop();
		if (mSkillQueue.front()->isStopDone()){
			mSkillQueue.pop();
			mSkillQueue.front()->reset();
		}
	}
	mBodyCommand = mSkillQueue.front()->getBodyCommand();
}
