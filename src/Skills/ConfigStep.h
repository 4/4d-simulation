// M.I.K.E. 3D Soccer Simulation Team
// File: ConfigStep.h

#ifndef _MIKE_CONFIGSTEP_H
#define _MIKE_CONFIGSTEP_H

#include <Types.h>
#include <BodyCommand.h>
#include <boost/property_tree/ptree.hpp>

namespace MIKE
{
	class ConfigStep
	{
		double mTime;
		BodyCommand* target;
	public:
		ConfigStep(const boost::property_tree::ptree&);
		~ConfigStep();

		double getTime() const;
		const BodyCommand* getTarget() const;
	}; // class ConfigStep
} // namespace MIKE

#endif //_MIKE_CONFIGSTEP_H
