// M.I.K.E. 3D Soccer Simulation Team
// File: BasicSkill.h

#ifndef _MIKE_BASICSKILL_H
#define _MIKE_BASICSKILL_H

#include <memory>
#include <string>

namespace MIKE {

	class BodyCommand;

	// BasicSkill abstract class. Any skill must be inherited from this class
	class BasicSkill {
	protected:
		std::shared_ptr<BodyCommand> mBodyCommand;

		std::string name;

	public:

		BasicSkill(const std::string &name); // Default constructor
		virtual ~BasicSkill(); // Destructor

		virtual void reset() = 0; // Reset the process

		virtual void prepare() = 0; // Prepare to execute the skill
		virtual void execute() = 0; // Execute the skill
		virtual void stop() = 0; // Stop executing and get back to start situation

		virtual bool isPrepareDone() const = 0; // Returns true if the prepare proccess is done
		virtual bool isStopDone() const = 0; // Returns true if the stop process is done

		std::string getName() const; // Returns the name of the skill
		std::shared_ptr<BodyCommand> getBodyCommand() const; // Returns the command for current cycle

	}; // class BasicSkill

} // namespace MIKE

#endif // _MIKE_BASICSKILL_H

