// M.I.K.E. 3D Soccer Simulation Team
// File: ConfigSkill.cpp

#include <ConfigSkill.h>
#include <WorldModel.h>
#include <ConfigManager.h>
#include <Types.h>

using namespace std;
using namespace boost::property_tree;
using namespace MIKE;
ConfigSkill::ConfigSkill(const string& name) : BasicSkill(name)
{
	mPrepareStartTime = -1;
	mExecuteStartTime = -1;
	mStopStartTime = -1;
	
	mPrepareStepNumber = 0;
	mExecuteStepNumber = 0;
	mStopStepNumber = 0;

	mPrepareDone = false;
	mStopDone = false;

	ptree pt = ConfigManager::Instance()->getConfigTree().get_child(name);

	mIsRepetetive = pt.get<bool>("repeat");

	ptree prepareTree = pt.get_child("prepare");
	for(ptree::const_iterator it = prepareTree.begin() ; it != prepareTree.end() ; it++)
		mPrepareSteps.push_back(ConfigStep(it->second));

	ptree executeTree = pt.get_child("execute");
	for(ptree::const_iterator it = executeTree.begin() ; it != executeTree.end() ; it++)
		mExecuteSteps.push_back(ConfigStep(it->second));

	ptree stopTree = pt.get_child("stop");
	for(ptree::const_iterator it = stopTree.begin() ; it != stopTree.end() ; it++)
		mStopSteps.push_back(ConfigStep(it->second));
	
	mBodyCommand = shared_ptr<BodyCommand>(new BodyCommand());
	mPreviusStepBody = shared_ptr<BodyCommand>(new BodyCommand());
}

ConfigSkill::~ConfigSkill()
{

}

void ConfigSkill::reset(){
	mPrepareStartTime = -1;
	mExecuteStartTime = -1;
	mStopStartTime = -1;
	
	mPrepareStepNumber = 0;
	mExecuteStepNumber = 0;
	mStopStepNumber = 0;

	mPrepareDone = false;
	mStopDone = false;
}

void ConfigSkill::prepare(){
	float time = WorldModel::Instance()->getWorldTime();
	if(abs(mPrepareStartTime + 1) < 1e-1){
		mPrepareStartTime = time;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	else if(abs(time - mPrepareStartTime - mPrepareSteps[mPrepareStepNumber].getTime()) < 1e-1){
		mPrepareStartTime = time;
		mPrepareStepNumber++;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	if(mPrepareStepNumber >= (int)mPrepareSteps.size()){
		mPrepareDone = true;
		return;
	}
	time = time - mPrepareStartTime;
	time /= mPrepareSteps[mPrepareStepNumber].getTime();
	for(int i = 3 ; i < JOINT_COUNT ; i++){
		mBodyCommand->operator[](static_cast<JointID>(i)) = (mPreviusStepBody->operator[](static_cast<JointID>(i)) + (mPrepareSteps[mPrepareStepNumber].getTarget()->operator[](static_cast<JointID>(i)) - mPreviusStepBody->operator[](static_cast<JointID>(i))) * time - WorldModel::Instance()->getBody()->findJoint(i)->getJointValue()) / 3.14;
	}
}

void ConfigSkill::execute(){
	float time = WorldModel::Instance()->getWorldTime();
	if(abs(mExecuteStartTime + 1) < 1e-1){
		mExecuteStartTime = time;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	else if(abs(time - mExecuteStartTime - mExecuteSteps[mExecuteStepNumber].getTime()) < 1e-1){
		mExecuteStartTime = time;
		mExecuteStepNumber++;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	if(mExecuteStepNumber >= (int)mExecuteSteps.size()){
		if(mIsRepetetive)
			mExecuteStepNumber = 0;
		else{
			for(int i = 3 ; i < JOINT_COUNT ; i++)
				mBodyCommand->operator[](static_cast<JointID>(i)) = 0;
			return;
		}
	}
	time = time - mExecuteStartTime;
	time /= mExecuteSteps[mExecuteStepNumber].getTime();
	for(int i = 3 ; i < JOINT_COUNT ; i++){
		mBodyCommand->operator[](static_cast<JointID>(i)) = (mPreviusStepBody->operator[](static_cast<JointID>(i)) + (mExecuteSteps[mExecuteStepNumber].getTarget()->operator[](static_cast<JointID>(i)) - mPreviusStepBody->operator[](static_cast<JointID>(i))) * time - WorldModel::Instance()->getBody()->findJoint(i)->getJointValue()) / 3.14;
	}
}

void ConfigSkill::stop(){
	float time = WorldModel::Instance()->getWorldTime();
	if(abs(mStopStartTime + 1) < 1e-1){
		mStopStartTime = time;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	else if(abs(time - mStopStartTime - mStopSteps[mStopStepNumber].getTime()) < 1e-1){
		mStopStartTime = time;
		mStopStepNumber++;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mPreviusStepBody->operator[](static_cast<JointID>(i)) = WorldModel::Instance()->getBody()->findJoint(i)->getJointValue();
	}
	if(mStopStepNumber >= (int)mStopSteps.size()){
		mStopDone = true;
		for(int i = 3 ; i < JOINT_COUNT ; i++)
			mBodyCommand->operator[](static_cast<JointID>(i)) = 0;
		return;
	}
	time = time - mStopStartTime;
	time /= mStopSteps[mStopStepNumber].getTime();
	for(int i = 3 ; i < JOINT_COUNT ; i++){
		mBodyCommand->operator[](static_cast<JointID>(i)) = (mPreviusStepBody->operator[](static_cast<JointID>(i)) + (mStopSteps[mStopStepNumber].getTarget()->operator[](static_cast<JointID>(i)) - mPreviusStepBody->operator[](static_cast<JointID>(i))) * time - WorldModel::Instance()->getBody()->findJoint(i)->getJointValue()) / 3.14;
	}

}

bool ConfigSkill::isPrepareDone() const{
	return mPrepareDone;
}

bool ConfigSkill::isStopDone() const{
	return mStopDone;
}
