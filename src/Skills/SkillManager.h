// M.I.K.E. 3D Soccer Simulation Team
// File: SkillManager.h

#ifndef _MIKE_SKILLMANAGER_H
#define _MIKE_SKILLMANAGER_H

#include <Singleton.h>
#include <memory>
#include <string>
#include <queue>
#include <map>
#include <functional>

namespace MIKE {

	class BodyCommand;
	class BasicSkill;

	// TODO: Test skill manager!!
	
	// This class manages and switches between the available skills
	class SkillManager : public Singleton<SkillManager> {

		std::shared_ptr<BodyCommand> mBodyCommand;
		std::queue<BasicSkill*> mSkillQueue;

		std::map<std::string, BasicSkill*> mSkills;

	public:

		SkillManager(); // Default constructor
		~SkillManager(); // Destructor

		std::shared_ptr<BodyCommand> getBodyCommand() const; // Returns the current skill's command

		// Manage skill by its type and name and set function
		template<class Skill>
		void manage(const std::string &name, std::function<void(Skill*)> set = std::function<void(Skill*)>()) {
			if (mSkills.count(name) == 0)
				mSkills[name] = new Skill(name);

			// TODO: BasicSwitch
			
			if (set)
				set(static_cast<Skill*>(mSkills[name]));

			if (mSkillQueue.size() == 0 || mSkillQueue.front() != mSkills[name])
				mSkillQueue.push(mSkills[name]);

			// TODO: Reset skill
		}

		void update();

	}; // class SkillManager

} // namespace MIKE

#endif // _MIKE_SKILLMANAGER_H

