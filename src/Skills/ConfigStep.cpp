// M.I.K.E. 3D Soccer Simulation Team
// File: ConfigStep.cpp

#include <ConfigStep.h>
#include <string>

using namespace std;
using namespace MIKE;
using namespace boost::property_tree;

ConfigStep::ConfigStep(const ptree& pt)
{
	target = new BodyCommand();
	mTime = pt.get<float>("time");
	for(ptree::const_iterator it = pt.begin() ; it != pt.end() ; it++){
		if(it->first != "time")
			target->operator[](static_cast<JointID>(FindJointID.at(it->first))) = it->second.get_value<float>();
	}
}

ConfigStep::~ConfigStep()
{
}

double ConfigStep::getTime() const
{
	return mTime;
}

const BodyCommand* ConfigStep::getTarget() const
{
	return target;
}
