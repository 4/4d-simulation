#!/bin/bash
#
# M.I.K.E. 3D Soccer Simulation team kill script
#

AGENT_BINARY=mike

killall -9 $AGENT_BINARY &> /dev/null
sleep 1

