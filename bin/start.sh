#!/bin/bash
#
# M.I.K.E. 3D Soccer Simulation team start script
#

AGENT_BINARY=mike
NUM_PLAYERS=11

for ((i=1; i<=$NUM_PLAYERS; i++)); do
	echo "Running agent No. $i"
	./$AGENT_BINARY --host $1 --number $i &> /dev/null &
	sleep 2
done

